#pragma once

#include "Dependencies\glm\vec3.hpp"

#include <utility>

class IBoundingVolume;

class Ray
{
public:
	Ray();
	Ray(const glm::vec3 & origin, const glm::vec3 & direction);
	~Ray();
	// returns positive distance to intersection point or -1.0f if no intersection occured
	std::pair<bool, float> Intersection(const IBoundingVolume & bounding_volume);
	glm::vec3 PointAlongRay(const float distance);

	glm::vec3 origin_;
	glm::vec3 direction_;
};

