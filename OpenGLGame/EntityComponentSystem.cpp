#include "EntityComponentSystem.h"

#include "Camera.h"
#include "ComponentContainer.h"
#include "Frustum.h"
#include "IBoundingVolume.h"
#include "Model.h"
#include "Locator.h"
#include "SDLOpenGLWindow.h"

#include "Dependencies\glm\gtc\matrix_transform.hpp"
#include "Dependencies\glm\gtx\component_wise.hpp"

#include <type_traits>
#include <vector>

EntityComponentSystem::EntityComponentSystem() : id_counter_(1)
{
	// assign bit positions and create component containers
	size_t bit_position_counter = 1;
	LambdaForEach(
		[&](auto c)
		{
			static_assert(std::is_base_of<Component<decltype(c)>, decltype(c)>::value, "Type not derived from Component template with CRTP");
			decltype(c)::bit_position_ = bit_position_counter++;
			component_containers_[decltype(c)::bit_position_ - 1] = std::make_unique<ComponentContainer<decltype(c)>>();
			std::cout << typeid(decltype(c)).name() << " gets the bit_position " << decltype(c)::bit_position_ << "\n";
		},
		ComponentList());
}

EntityComponentSystem::~EntityComponentSystem()
{
}

void EntityComponentSystem::UpdateGameLoop(const float dt)
{
	UpdatePhysics(dt);
	RemoveDeadEntities();
}

void EntityComponentSystem::UpdateFrame()
{
	Update3DRendering();
	Update2DRendering();
}

void EntityComponentSystem::RemoveDeadEntities()
{
	// removal of unordered_map while iterating through it can lead to problems
	// workaround: gather dead entities inside vector to iterate through them while deleting from unordered_map

	std::vector<std::pair<const size_t, Bitmask>> dead_id_bitmask_pairs;
	for (auto & id_bitmask : id_bitmask_table_)
	{
		if (!id_bitmask.second.test(0))
			dead_id_bitmask_pairs.push_back(id_bitmask);
	}

	for (auto & dead_id_bitmask_pair : dead_id_bitmask_pairs)
	{
		LambdaForEach(
			[&](auto c)
			{
				if (TestForComponents<decltype(c)>(dead_id_bitmask_pair.second))
					GetComponentContainer<decltype(c)>().Remove(dead_id_bitmask_pair.first);
			},
			ComponentList());

		// done with components, remove id and bitmask from table
		id_bitmask_table_.erase(dead_id_bitmask_pair.first);
	}
}

void EntityComponentSystem::ClearEntities()
{
	LambdaForEach([&](auto c){ GetComponentContainer<decltype(c)>().Clear(); }, ComponentList());
	id_bitmask_table_.clear();
	id_counter_ = 1;
}

void EntityComponentSystem::KillEntity(const size_t id)
{
	// position 0 in bitset is alive/dead bit
	id_bitmask_table_[id].reset(0);
}

void EntityComponentSystem::UpdateCameraEntity(const size_t id)
{
	GetComponent<Position>(id) = Locator<Camera>::service()->position_;
	GetComponent<Rotation>(id) = Locator<Camera>::service()->GetRotationQuaternion();
}

size_t EntityComponentSystem::UniqueID()
{
	return id_counter_++;
}

void EntityComponentSystem::UpdatePhysics(const float dt)
{
	// update rot with angular velocity
	for (auto & id_index : GetComponentContainer<AngularVelocity>().id_index_table_)
	{
		// how many normalizations necessary ??? 0-3 possible
		GetComponent<Rotation>(id_index.first) =
			glm::normalize(glm::quat(GetComponentContainer<AngularVelocity>().GetByIndex(id_index.second).angular_velocity_ * dt) *
				GetComponent<Rotation>(id_index.first).rotation_);
	}
		
	// update pos with velocity
	for (auto & id_index : GetComponentContainer<Velocity>().id_index_table_)
		GetComponent<Position>(id_index.first).position_ +=	GetComponentContainer<Velocity>().GetByIndex(id_index.second).velocity_ * dt;	

	// update bounding volumes

	// vector of ids, Bounding_pointer pairs
	std::vector<std::pair<size_t, IBoundingVolume*>> id_bounding_volume_pairs(
		GetComponentContainer<BoundingSphere>().Size() +
		GetComponentContainer<OrientedBoundingBox>().Size());
	size_t id_bounding_volume_index = 0;

	for (auto & id_index : GetComponentContainer<BoundingSphere>().id_index_table_)
	{
		BoundingSphere & bounding_sphere = GetComponentContainer<BoundingSphere>().GetByIndex(id_index.second);
		
		// update BoundingSphere
		Bitmask bitmask = id_bitmask_table_[id_index.first];

		bounding_sphere.position_ = GetComponent<Position>(id_index.first);
		if (TestForComponents<Scaling>(bitmask))
			bounding_sphere.radius_ = bounding_sphere.initial_radius_ * glm::compMax(GetComponent<Scaling>(id_index.first).scaling_);
			
		// add id pointer pair to vector
		id_bounding_volume_pairs[id_bounding_volume_index] = std::make_pair(id_index.first, &bounding_sphere);

		id_bounding_volume_index++;
	}

	for (auto & id_index : GetComponentContainer<OrientedBoundingBox>().id_index_table_)
	{
		OrientedBoundingBox & oriented_bounding_box = GetComponentContainer<OrientedBoundingBox>().GetByIndex(id_index.second);

		// update OrientedBoundingBox
		Bitmask bitmask = id_bitmask_table_[id_index.first];

		oriented_bounding_box.position_ = GetComponent<Position>(id_index.first);
		if (TestForComponents<Rotation>(bitmask))
			oriented_bounding_box.rotation_ = GetComponent<Rotation>(id_index.first);
		if (TestForComponents<Scaling>(bitmask))
			oriented_bounding_box.extent_ = oriented_bounding_box.initial_extent_ * GetComponent<Scaling>(id_index.first).scaling_;

		// add id pointer pair to vector
		id_bounding_volume_pairs[id_bounding_volume_index] = std::make_pair(id_index.first, &oriented_bounding_box);

		id_bounding_volume_index++;
	}


	// Collision Detection


	Ray picking_ray = Locator<Camera>::service()->GetPickingRay();
	float picking_ray_min_distance = FLT_MAX;
	size_t picking_ray_min_distance_id = 0;

	// iterate through generated vector
	size_t id_bounding_volume_pairs_size = id_bounding_volume_pairs.size();
	for (size_t i = 0; i < id_bounding_volume_pairs_size; i++)
	{
		size_t id = id_bounding_volume_pairs[i].first;
		Bitmask bitmask = id_bitmask_table_[id];
		IBoundingVolume * bounding_volume = id_bounding_volume_pairs[i].second;

		// Ray picking
		std::pair<bool, float> ray_picking_result = picking_ray.Intersection(*bounding_volume);
		if (ray_picking_result.first)
		{
			if (ray_picking_result.second < picking_ray_min_distance)
			{
				picking_ray_min_distance = ray_picking_result.second;
				picking_ray_min_distance_id = id;
			}
		}

		for (size_t j = i+1; j < id_bounding_volume_pairs_size; j++)
		{
			size_t target_id = id_bounding_volume_pairs[j].first;
			Bitmask target_bitmask = id_bitmask_table_[target_id];
			IBoundingVolume * target_bounding_volume = id_bounding_volume_pairs[j].second;

			if (bounding_volume->Collision(*target_bounding_volume))
			{
				// gather collision events, resolve later
				// TODO...

				// Entities with Projectile Component kill the entities they collide with and themselves
				if (TestForComponents<Projectile>(bitmask) || TestForComponents<Projectile>(target_bitmask))
				{
					KillEntity(target_id);
					KillEntity(id);
				}
			}
		}
	}
	// save results in camera
	Locator<Camera>::service()->picking_ray_min_distance_ = picking_ray_min_distance;
	Locator<Camera>::service()->picking_ray_min_distance_id_ = picking_ray_min_distance_id;
	
}

void EntityComponentSystem::Update3DRendering()
{
	glm::mat4 model_matrix;
	glm::mat4 projection_matrix = Locator<SDLOpenGLWindow>::service()->ProjectionMatrix();
	glm::mat4 view_matrix = Locator<Camera>::service()->view_matrix_;
	Frustum frustum(projection_matrix * view_matrix);
	
	unsigned counter = 0;
	for (auto & id_index : GetComponentContainer<Rendering>().id_index_table_)
	{
		Bitmask bitmask = id_bitmask_table_[id_index.first];
		
		// Check if entities with bounding volumes are inside the frustum
		if (TestForComponents<BoundingSphere>(bitmask) && !frustum.Contained(GetComponent<BoundingSphere>(id_index.first)))
			continue;
		if (TestForComponents<OrientedBoundingBox>(bitmask) && !frustum.Contained(GetComponent<OrientedBoundingBox>(id_index.first)))
			continue;


		// allow for rendering of entities which are missing the 'Rotation' or 'Scaling' components by replacing them with default values
		model_matrix = ModelMatrix(
			GetComponent<Position>(id_index.first),
			TestForComponents<Rotation>(bitmask) ? GetComponent<Rotation>(id_index.first) : Rotation(),
			TestForComponents<Scaling>(bitmask) ? GetComponent<Scaling>(id_index.first) : Scaling());

		GetComponentContainer<Rendering>().GetByIndex(id_index.second).model_ptr_->Draw(projection_matrix, view_matrix, model_matrix);
		counter++;

	}

	//std::cout << " number of rendered objects: " << counter << "\n";

	// Render Bounding Volumes in Wireframe mode and without backface culling
	Locator<SDLOpenGLWindow>::service()->ToggleWireFrameMode();
	Locator<SDLOpenGLWindow>::service()->ToggleBackFaceCulling();
	for (auto & id_index : GetComponentContainer<BoundingSphere>().id_index_table_)
		GetComponentContainer<BoundingSphere>().GetByIndex(id_index.second).Draw(projection_matrix, view_matrix);
	for (auto & id_index : GetComponentContainer<OrientedBoundingBox>().id_index_table_)
		GetComponentContainer<OrientedBoundingBox>().GetByIndex(id_index.second).Draw(projection_matrix, view_matrix);
	Locator<SDLOpenGLWindow>::service()->ToggleWireFrameMode();
	Locator<SDLOpenGLWindow>::service()->ToggleBackFaceCulling();
}

void EntityComponentSystem::Update2DRendering()
{
	// Render UI

	// change proj matrix to be orthogornal ???

	glm::mat4 model_matrix;
	glm::mat4 projection_matrix = Locator<SDLOpenGLWindow>::service()->ProjectionMatrix();
	glm::mat4 view_matrix = Locator<Camera>::service()->view_matrix_;

	for (auto & id_index : GetComponentContainer<RenderingUI>().id_index_table_)
	{
		Bitmask bitmask = id_bitmask_table_[id_index.first];
		RenderingUI & rendering_ui = GetComponentContainer<RenderingUI>().GetByIndex(id_index.second);

		if (rendering_ui.show_)
		{
			// allow for rendering of entities which are missing the 'Rotation' or 'Scaling' components by replacing them with default values
			model_matrix = ModelMatrix(
				GetComponent<Position>(id_index.first).position_,
				TestForComponents<Rotation>(bitmask) ? GetComponent<Rotation>(id_index.first) : Rotation(),
				TestForComponents<Scaling>(bitmask) ? GetComponent<Scaling>(id_index.first) : Scaling());

			rendering_ui.model_ptr_->Draw(projection_matrix, view_matrix, model_matrix);
		}
	}
}

glm::mat4x4 EntityComponentSystem::ModelMatrix(const glm::vec3 & position, const glm::quat & quaternion, const glm::vec3 & scaling)
{
	// translation * rotation * scale
	auto test = glm::toMat4(quaternion);
	return glm::translate(glm::mat4(1.0f), position) * glm::toMat4(quaternion) * glm::scale(glm::mat4(1.0f), scaling);
}

bool EntityComponentSystem::ComponentDependenciesSatisfied(const Bitmask & bitmask)
{
	// implication 'a->b' equivalent to 'not(a) or b'
	auto Impl = [](const bool a, const bool b) { return !a || b; };

	bool has_bounding_volume = TestForComponents<BoundingSphere>(bitmask) || TestForComponents<OrientedBoundingBox>(bitmask);

	// (!ABV || !BBV) && ... for each pair
	bool only_one_bounding_volume = (!TestForComponents<BoundingSphere>(bitmask) || !TestForComponents<OrientedBoundingBox>(bitmask));

	return Impl(TestForComponents<AngularVelocity>(bitmask), TestForComponents<Rotation>(bitmask)) &&
		Impl(TestForComponents<Velocity>(bitmask), TestForComponents<Position>(bitmask)) &&
		Impl(TestForComponents<Rendering>(bitmask), TestForComponents<Position>(bitmask)) &&
		Impl(TestForComponents<BoundingSphere>(bitmask), TestForComponents<Position>(bitmask)) &&
		Impl(TestForComponents<RenderingUI>(bitmask), TestForComponents<Position>(bitmask)) &&
		Impl(TestForComponents<Projectile>(bitmask), has_bounding_volume) &&
		Impl(has_bounding_volume, TestForComponents<Position>(bitmask)) &&
		only_one_bounding_volume;
}

