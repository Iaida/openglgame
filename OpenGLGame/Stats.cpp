#include "Stats.h"

#include <fstream>
#include <sstream>

Stats::Stats(std::string file_path) 
{
	std::vector<std::vector<std::string>> string_vector = CsvToStringVector(file_path);
	attributes_vector_ = StringVectorToAttributeVector(string_vector);
}


std::vector<std::vector<std::string>> Stats::CsvToStringVector(std::string file_path)
{
	std::vector<std::vector<std::string>> result;
	std::ifstream file_stream(file_path);
	std::string line;

	// skip first line with descriptions of columns
	std::getline(file_stream, line);

	// save current line till end of file
	while (std::getline(file_stream, line))
	{
		std::vector<std::string> line_result;
		std::string field;
		std::istringstream string_stream(line);

		// save current field till end of line
		while (std::getline(string_stream, field, ','))
		{
			line_result.push_back(field);
		}
		// done with current line
		result.push_back(line_result);
	}
	return result;
}

std::vector<attributes> Stats::StringVectorToAttributeVector(std::vector<std::vector<std::string>> &string_vector)
{
	std::vector<attributes> result;
	attributes a;
	// iterate over rows
	for (size_t c = 0; c < string_vector.size(); c++)
	{
		// go over attributes
		a.name = string_vector[c][0];
		a.strength = std::stoi(string_vector[c][1]);
		a.agility = std::stoi(string_vector[c][2]);
		a.intelligence = std::stoi(string_vector[c][3]);
		result.push_back(a);
	}
	return result;
}