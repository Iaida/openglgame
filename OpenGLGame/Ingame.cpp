#include "Ingame.h"
#include "Options.h"
#include "SDLInputEvents.h"
#include "Locator.h"
#include "EntityComponentSystem.h"
#include "Camera.h"
#include "GameInfo.h"
#include "Options.h"
#include "ModelContainer.h"
#include "BoundingSphere.h"

#include "Dependencies\glm\geometric.hpp"


Ingame::Ingame()
{
}


Ingame::~Ingame()
{
}

void Ingame::ProcessKeyboard()
{
	if (Locator<SDLInputEvents>::service()->KeyPressed(SDL_SCANCODE_ESCAPE))
	{
		Locator<GameInfo>::service()->set_game_state<Paused>();
		if (SDL_SetRelativeMouseMode(SDL_FALSE) < 0)
			std::cout << "SDL_SetRelativeMouseMode failed \n";
	}

	

	if (!Locator<Options>::service()->gamepad())
	{
		// WASD movement in 8 directions
		switch (Locator<SDLInputEvents>::service()->WASDCardinalDirection())
		{
		case NONE:
			// stop
			Locator<Camera>::service()->velocity_ = glm::vec3(0.0f, 0.0f, 0.0f);
			break;
		case NORTH:
			// move along direction vector
			Locator<Camera>::service()->velocity_ = Locator<Camera>::service()->direction_;
			break;
		case NORTHEAST:
			// move along direction vector, strafe right
			Locator<Camera>::service()->velocity_ = glm::normalize(Locator<Camera>::service()->direction_ - Locator<Camera>::service()->right_);
			break;
		case EAST:
			// strafing right
			Locator<Camera>::service()->velocity_ = -Locator<Camera>::service()->right_;
			break;
		case SOUTHEAST:
			// move along negative direction vector, strafe right
			Locator<Camera>::service()->velocity_ = glm::normalize(-Locator<Camera>::service()->direction_ - Locator<Camera>::service()->right_);
			break;
		case SOUTH:
			// move along negative direction vector
			Locator<Camera>::service()->velocity_ = -Locator<Camera>::service()->direction_;
			break;
		case SOUTHWEST:
			// move along negative direction vector, strafe left
			Locator<Camera>::service()->velocity_ = glm::normalize(-Locator<Camera>::service()->direction_ + Locator<Camera>::service()->right_);
			break;
		case WEST:
			// strafing left
			Locator<Camera>::service()->velocity_ = Locator<Camera>::service()->right_;
			break;
		case NORTHWEST:
			// move along direction vector, strafe left
			Locator<Camera>::service()->velocity_ = glm::normalize(Locator<Camera>::service()->direction_ + Locator<Camera>::service()->right_);
			break;
		}

		if (Locator<SDLInputEvents>::service()->KeyPressed(SDL_SCANCODE_KP_MINUS))
		{
			// remove ang vel component from entity with id 1
			//Locator<EntityComponentSystem>::service()->RemoveComponent<AngularVelocity>(1);
			static size_t id_counter = 1;
			Locator<EntityComponentSystem>::service()->KillEntity(id_counter++);

		}
		if (Locator<SDLInputEvents>::service()->KeyPressed(SDL_SCANCODE_KP_PLUS))
		{
			// add new ang vel component for entity with id 1
			//Locator<EntityComponentSystem>::service()->AddReplaceComponent(1, AngularVelocity(0.0f, 40.0f, 0.0f));


		}
	}
}

void Ingame::ProcessController()
{
	// LEFT STICK
	switch (Locator<SDLInputEvents>::service()->LeftStickCardinalDirection())
	{
	case NONE:
		// set velocity to zero
		Locator<Camera>::service()->velocity_ = glm::vec3(0.0f, 0.0f, 0.0f);
		break;
	case NORTH:
		// move along direction vector
		Locator<Camera>::service()->velocity_ = Locator<Camera>::service()->direction_;
		break;
	case NORTHEAST:
		// move along direction vector, strafe right
		Locator<Camera>::service()->velocity_ = glm::normalize(Locator<Camera>::service()->direction_ - Locator<Camera>::service()->right_);
		break;
	case EAST:
		// strafing right
		Locator<Camera>::service()->velocity_ = -Locator<Camera>::service()->right_;
		break;
	case SOUTHEAST:
		// move along negative direction vector, strafe right
		Locator<Camera>::service()->velocity_ = glm::normalize(-Locator<Camera>::service()->direction_ - Locator<Camera>::service()->right_);
		break;
	case SOUTH:
		// move along negative direction vector
		Locator<Camera>::service()->velocity_ = -Locator<Camera>::service()->direction_;
		break;
	case SOUTHWEST:
		// move along negative direction vector, strafe left
		Locator<Camera>::service()->velocity_ = glm::normalize(-Locator<Camera>::service()->direction_ + Locator<Camera>::service()->right_);
		break;
	case WEST:
		// strafing left
		Locator<Camera>::service()->velocity_ = Locator<Camera>::service()->right_;
		break;
	case NORTHWEST:
		// move along direction vector, strafe left
		Locator<Camera>::service()->velocity_ = glm::normalize(Locator<Camera>::service()->direction_ + Locator<Camera>::service()->right_);
		break;
	}

	// RIGHT STICK
	if (Locator<SDLInputEvents>::service()->RightStickInDeadzone())
	{
		Locator<Camera>::service()->y_rot_vel_ = 0.0f;
		Locator<Camera>::service()->xz_rot_vel_ = 0.0f;
	}
	else
	{
		Locator<Camera>::service()->y_rot_vel_ = -Locator<SDLInputEvents>::service()->RightStickXValue();
		Locator<Camera>::service()->xz_rot_vel_ = -Locator<SDLInputEvents>::service()->RightStickYValue();
	}

	if (Locator<SDLInputEvents>::service()->ButtonPressed(SDL_CONTROLLER_BUTTON_START))
	{
		Locator<GameInfo>::service()->set_game_state<Paused>();
		if (SDL_SetRelativeMouseMode(SDL_FALSE) < 0)
			std::cout << "SDL_SetRelativeMouseMode failed \n";
	}

	
}

void Ingame::ProcessMouse()
{
	if (!Locator<Options>::service()->gamepad())
	{
		Locator<Camera>::service()->y_rot_ += -static_cast<float>(Locator<SDLInputEvents>::service()->mouse_x_rel_) * Locator<SDLInputEvents>::service()->mouse_sensitivity_;
		Locator<Camera>::service()->xz_rot_ += -static_cast<float>(Locator<SDLInputEvents>::service()->mouse_y_rel_) * Locator<SDLInputEvents>::service()->mouse_sensitivity_;
	}

	if (Locator<SDLInputEvents>::service()->MouseButtonPressed(SDL_BUTTON_RIGHT))
	{
		Locator<Camera>::service()->zoom_ = (Locator<Camera>::service()->zoom_ == 1.0f) ? 0.25f : 1.0f;
		// toggle sniping ui elements / overlay
		if (Locator<Camera>::service()->zoom_overlay_id_ != 0)
		{
			// bit flip for toggling bool
			Locator<EntityComponentSystem>::service()->GetComponent<RenderingUI>(Locator<Camera>::service()->zoom_overlay_id_).show_ ^= 1;
		}

	}

	if (Locator<SDLInputEvents>::service()->MouseButtonPressed(SDL_BUTTON_LEFT))
	{
		if (Locator<Camera>::service()->zoom_overlay_id_ != 0)
		{
			Locator<EntityComponentSystem>::service()->CreateEntity(
				Position(Locator<Camera>::service()->position_),
				Scaling(0.25f),
				Rotation(Locator<Camera>::service()->GetRotationQuaternion()),
				Velocity(Locator<Camera>::service()->direction_ * 400.0f),
				Rendering(Locator<ModelContainer>::service()->Get("cube_textured")),
				//OrientedBoundingBox(Locator<ModelContainer>::service()->Get("oriented_bounding_box"), Locator<ModelContainer>::service()->Get("cube_textured")),
				BoundingSphere(Locator<ModelContainer>::service()->Get("bounding_sphere"), Locator<ModelContainer>::service()->Get("cube_textured")),
				Projectile());
		}
	}
}
