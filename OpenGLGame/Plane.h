#pragma once
#include "Dependencies\glm\vec3.hpp"
#include "Dependencies\glm\vec4.hpp"

// Based on: 'Real-Time Collision Detection' by Christer Ericson 

class IBoundingVolume;

class Plane
{
public:
	Plane();
	// normal should already be normalized
	Plane(const glm::vec3 & normal, const float distance);
	// normal should already be normalized, 4th component is distance
	Plane(const glm::vec4 & normal_distance);
	// 3 noncollinear points (ordered ccw)
	Plane(const glm::vec3 & a, const glm::vec3 & b, const glm::vec3 & c);
	~Plane();

	float SignedPointDistance(const glm::vec3 & point) const;
	

	glm::vec3 normal_; 
	float distance_; // from origin, distance_ = dot(normal_,x) for a given point x on the plane
};

