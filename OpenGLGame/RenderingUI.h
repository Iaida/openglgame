#pragma once
#include "Component.h"

class Model;

class RenderingUI : public Component<RenderingUI>
{
public:
	RenderingUI() {};
	RenderingUI(Model * model_ptr) : model_ptr_(model_ptr), show_(false) {};
	
	Model * model_ptr_;
	bool show_;
};

