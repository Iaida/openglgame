#pragma once
#include "Model.h"
class OrientedBoundingBoxModel :
	public Model
{
public:
	OrientedBoundingBoxModel(Program * program);
	~OrientedBoundingBoxModel();

	virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::mat4& model_matrix)	override final;

private:
	int number_vertices_;
	int number_faces_;
};

