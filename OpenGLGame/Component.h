#pragma once

template <typename T>
class Component
{
public:
	static size_t bit_position_;
};

template <typename T>
size_t Component<T>::bit_position_(0);