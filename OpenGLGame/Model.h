#pragma once

#include "Program.h"

#include "Dependencies\glm\mat4x4.hpp"

#include "Dependencies\glm\vec3.hpp"

#include <vector>

class Model 
{
public:
	Model() {}
	virtual ~Model();
	
	virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::mat4& model_matrix) = 0;

	void CalculateMinMax(const std::vector<glm::vec3> & vertex_positons);
	
	glm::vec3 min_;
	glm::vec3 max_;
	float radius_;

protected:
	unsigned vao_;
	std::vector<unsigned> vbos_;
	Program*  program_;
};

