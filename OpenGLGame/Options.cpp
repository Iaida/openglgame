#define _USE_MATH_DEFINES

#include "Options.h"

#include <cmath>
#include <fstream>
#include <vector>
#include <sstream>
#include <iostream>

#include "Locator.h"
#include "SDLOpenGLWindow.h"

Options::Options(const std::string & file_path, const std::string & default_file_path)
{
	file_path_ = file_path;
	default_file_path_ = default_file_path;
	gamepad_ = false; // activate or deactivate gamepad usage

	Load();
}

void Options::SaveChanges()
{
	std::string temp_file_path = "Config temp.cfg";

	std::ifstream filein(file_path_); 
	std::ofstream fileout(temp_file_path); 
	if (!filein || !fileout)
	{
		std::cout << "error opening files! \n";
		return;
	}

	std::string line;
	std::string section;

	// save current line till end of file is reached
	while (std::getline(filein, line))
	{
		std::istringstream string_stream(line);
		std::string key; // always first element in line
		std::getline(string_stream, key, '=');

		// empty line or comment
		if (key.size() == 0 || key[0] == '#')
		{
			fileout << line + "\n";
			continue;
		}
			
		// new section, syntax: [Section]
		if (key[0] == '[')
		{
			section = key;
			fileout << line + "\n";
			continue;
		}

		// normal line in a section, syntax: key=value
		std::string value; // value
		std::getline(string_stream, value);

		if (section == "[General]")
		{
			if (key == "Title")
				set_title(value);
			else
				std::cout << "key not recognized in section [General] \n";
		}

		else if (section == "[Video]")
		{
			
			if (key == "VerticalSync" && (stoi(value) != 0) != vertical_sync())
				value = std::to_string(vertical_sync());
			/*else if (key == "Fullscreen" && (stoi(value) != 0) != fullscreen())
				value = std::to_string(fullscreen());*/
			/*else if (key == "FovHorizontal" && stof(value) != fov_horizontal_deg())
				value = std::to_string(fov_horizontal_deg());*/

			/*if (key == "Height" && stoi(value) != height())
				value = height();
			else if (key == "Width")
				set_width(stoi(value));
			else if (key == "FovHorizontal" && stof(value) != fov_horizontal_deg())
				value = fov_horizontal_deg();
			else if (key == "FarDistance")
				set_far_distance(stof(value));
			else if (key == "NearDistance")
				set_near_distance(stof(value));
			else if (key == "VerticalSync")
				set_vertical_sync(stoi(value) != 0);*/
			/*else
				std::cout << "key not recognized in section [Video] \n";*/
		}
		else if (section == "[Audio]")
		{
			/*if (key == "MasterVolume")
				set_master_volume(stof(value));
			else if (key == "EffectsVolume")
				set_effects_volume(stof(value));
			else if (key == "VoiceVolume")
				set_voice_volume(stof(value));
			else if (key == "AmbienceVolume")
				set_ambience_volume(stof(value));
			else if (key == "MusicVolume")
				set_music_volume(stof(value));
			else
				std::cout << "key not recognized in section [Audio] \n";*/
		}

		else
		{
			std::cout << "section not recognized \n";
		}
		// done with current line

		fileout << key + "=" + value + "\n";

	}

	// done with entire file
	


	// delete original file
	if (std::remove(file_path_.c_str()) != 0)
	{
		std::cout << "error removing file "<< file_path_ << "\n";
	}
	// rename temp file to original file name
	if (std::rename(temp_file_path.c_str(), file_path_.c_str()) != 0)
	{
		std::cout << "error renaming file " << temp_file_path << " to " << file_path_ << "\n";
	}
}



void Options::set_title(const std::string & title)
{
	title_ = title;
}

void Options::set_gamepad(const bool gamepad)
{
	gamepad_ = gamepad;
}

void Options::set_width(const int width)
{
	width_ = width;
}

void Options::set_height(const int height)
{
	(height == 0) ? height_ = 1 : height_ = height;
}

void Options::set_resolution(const int width, const int height)
{
	// change width and height
	set_width(width);
	set_height(height);

	Locator<SDLOpenGLWindow>::service()->SetResolution(width_, height_);
}


// https://en.wikipedia.org/wiki/Field_of_view_in_video_games

void Options::set_fov_horizontal_deg(const float fov_horizontal_deg)
{
	fov_vertical_deg_ = RadToDeg(2.0f * atan(tan(DegToRad(fov_horizontal_deg) / 2.0f) / aspect_ratio()));
}

void Options::set_fov_vertical_deg(const float fov_vertical_deg)
{
	fov_vertical_deg_ = fov_vertical_deg;
}

void Options::set_near_distance(const float near_distance)
{
	near_distance_ = near_distance;
}

void Options::set_far_distance(const float far_distance)
{
	far_distance_ = far_distance;
}

void Options::set_vertical_sync(const bool vertical_sync)
{
	vertical_sync_ = vertical_sync;
	if (Locator<SDLOpenGLWindow>::service())
		Locator<SDLOpenGLWindow>::service()->SetVSync(vertical_sync_);
	
	
}

void Options::set_fullscreen(const bool fullscreen)
{
	fullscreen_ = fullscreen;
	if (Locator<SDLOpenGLWindow>::service())
		Locator<SDLOpenGLWindow>::service()->SetFullscreen(fullscreen);
}

void Options::set_master_volume(const float master_volume)
{
	if (master_volume > 1.0f)
		master_volume_ = 1.0f;
	else if (master_volume < 0.0f)
		master_volume_ = 0.0f;
	else
		master_volume_ = master_volume;
}

void Options::set_effects_volume(const float effects_volume)
{
	if (effects_volume > 1.0f)
		effects_volume_ = 1.0f;
	else if (effects_volume < 0.0f)
		effects_volume_ = 0.0f;
	else
		effects_volume_ = effects_volume;
}

void Options::set_voice_volume(const float voice_volume)
{
	if (voice_volume > 1.0f)
		voice_volume_ = 1.0f;
	else if (voice_volume < 0.0f)
		voice_volume_ = 0.0f;
	else
		voice_volume_ = voice_volume;
}

void Options::set_ambience_volume(const float ambience_volume)
{
	if (ambience_volume > 1.0f)
		ambience_volume_ = 1.0f;
	else if (ambience_volume < 0.0f)
		ambience_volume_ = 0.0f;
	else
		ambience_volume_ = ambience_volume;
}

void Options::set_music_volume(const float music_volume)
{
	if (music_volume > 1.0f)
		music_volume_ = 1.0f;
	else if (music_volume < 0.0f)
		music_volume_ = 0.0f;
	else
		music_volume_ = music_volume;
}


std::string Options::title() const
{
	return title_;
}

bool Options::gamepad() const
{
	return gamepad_;
}

int Options::width() const
{
	return width_;
}

int Options::height() const
{
	return height_;
}

float Options::aspect_ratio() const
{
	return (float)width_ / (float)height_;
}


// https://en.wikipedia.org/wiki/Field_of_view_in_video_games

float Options::fov_horizontal_deg() const
{
	return RadToDeg(2.0f * atan(tan(DegToRad(fov_vertical_deg_) / 2.0f) * aspect_ratio()));
}

float Options::fov_vertical_deg() const
{
	return fov_vertical_deg_;
}

float Options::near_distance() const
{
	return near_distance_;
}

float Options::far_distance() const
{
	return far_distance_;
}

bool Options::vertical_sync() const
{
	return vertical_sync_;
}

bool Options::fullscreen() const
{
	return fullscreen_;
}

float Options::master_volume() const
{
	return master_volume_;
}

float Options::effects_volume() const
{
	return effects_volume_;
}

float Options::voice_volume() const
{
	return voice_volume_;
}

float Options::ambience_volume() const
{
	return ambience_volume_;
}

float Options::music_volume() const
{
	return music_volume_;
}


// http://www.mathinary.com/degrees_radians.jsp

float Options::DegToRad(const float deg) const
{
	return (deg * (float)M_PI) / 180.0f;
}

float Options::RadToDeg(const float rad) const
{
	return (rad * 180.0f) / (float)M_PI;

}

void Options::Load()
{
	std::ifstream file_stream(file_path_);
	if (!file_stream)
	{
		std::cout << file_path_ << " could not be opened, using default Config \n";

		file_stream.open(default_file_path_);
		if (!file_stream)
		{
			std::cout << default_file_path_ << " could not be opened \n";
			return;
		}

		// http://stackoverflow.com/questions/10195343/copy-a-file-in-a-sane-safe-and-efficient-way
		// copy default file to replace Config.cfg
		std::ifstream  src(default_file_path_, std::ios::binary);
		std::ofstream  dst(file_path_, std::ios::binary);
		dst << src.rdbuf();

	}

	std::string line;
	std::string section;

	// save current line till end of file is reached
	while (std::getline(file_stream, line))
	{
		std::istringstream string_stream(line);
		std::string key; // always first element in line
		std::getline(string_stream, key, '=');

		// empty line or comment
		if (key.size() == 0 || key[0] == '#')
			continue;

		// new section, syntax: [Section]
		if (key[0] == '[')
		{
			section = key;
			continue;
		}

		// normal line in a section, syntax: key=value
		std::string value; // value
		std::getline(string_stream, value);

		if (section == "[General]")
		{
			if (key == "Title")
				set_title(value);
			else
				std::cout << "key not recognized in section [General] \n";
		}
		else if (section == "[Video]")
		{
			if (key == "Height")
				set_height(stoi(value));
			else if (key == "Width")
				set_width(stoi(value));
			else if (key == "FovHorizontal")
				set_fov_horizontal_deg(stof(value));
			else if (key == "FarDistance")
				set_far_distance(stof(value));
			else if (key == "NearDistance")
				set_near_distance(stof(value));
			else if (key == "VerticalSync")
				set_vertical_sync(stoi(value) != 0);
			else if (key == "Fullscreen")
				set_fullscreen(stoi(value) != 0);
			else
				std::cout << "key not recognized in section [Video] \n";
		}
		else if (section == "[Audio]")
		{
			if (key == "MasterVolume")
				set_master_volume(stof(value));
			else if (key == "EffectsVolume")
				set_effects_volume(stof(value));
			else if (key == "VoiceVolume")
				set_voice_volume(stof(value));
			else if (key == "AmbienceVolume")
				set_ambience_volume(stof(value));
			else if (key == "MusicVolume")
				set_music_volume(stof(value));
			else
				std::cout << "key not recognized in section [Audio] \n";
		}
		else
		{
			std::cout << "section not recognized \n";
		}
		// done with current line
		int b = 0;
	}
	// done with entire file
}








