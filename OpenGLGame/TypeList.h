#pragma once

// based on: http://stackoverflow.com/questions/11184223/c-function-variadic-templates-with-no-arguments/11184314#11184314

template <typename... Ts>
class TypeList
{
public:
	using type = TypeList;
	static constexpr size_t size() noexcept { return sizeof...(Ts); }
};

// Use 'auto t' as lambda parameter and 'decltype(t)' inside the body to get the respective type
template<class Lambda, typename T1, typename ... Ts>
void LambdaForEach(Lambda lambda, const TypeList<T1, Ts...> &)
{
	lambda(T1());
	LambdaForEach(lambda, TypeList<Ts...>());
};

template<class Lambda>
void LambdaForEach(Lambda lambda, const TypeList<> &) { }