#include "BoundingSphere.h"

#include "Model.h"
#include "Ray.h"
#include "OrientedBoundingBox.h"
#include "Plane.h"


#include "Dependencies\glm\geometric.hpp"
#include "Dependencies\glm\gtc\matrix_transform.hpp"

BoundingSphere::BoundingSphere()
{
}

// initialize position so it won't be seen when initiated, only after first update
// if target_model_ptr->radius_ was not computed/set, choose aabb max as furthest point of mesh
BoundingSphere::BoundingSphere(Model * model_ptr, Model * target_model_ptr) :
	model_ptr_(model_ptr), position_(FLT_MAX), 
	initial_radius_((target_model_ptr->radius_ == float()) ? glm::length(target_model_ptr->max_) : target_model_ptr->radius_),
	radius_(initial_radius_)
{
}

BoundingSphere::~BoundingSphere()
{
}

bool BoundingSphere::Collision(const IBoundingVolume & bounding_volume) const
{
	return bounding_volume.CollisionBoundingSphere(*this);
}

bool BoundingSphere::CollisionBoundingSphere(const BoundingSphere & bounding_sphere) const
{
	// Sphere-Sphere Collision
	float distance = glm::length(position_ - bounding_sphere.position_);
	// scaling xyz components should be equal for each sphere
	float sum_radii = radius_ + bounding_sphere.radius_;

	return distance < sum_radii;
}

bool BoundingSphere::CollisionOrientedBoundingBox(const OrientedBoundingBox & oriented_bounding_box) const
{
	// Sphere-OBB -> OBB-Sphere
	return oriented_bounding_box.CollisionBoundingSphere(*this);
}


void BoundingSphere::Draw(const glm::mat4 & projection_matrix, const glm::mat4 & view_matrix)
{
	// initialize diagonal with scaling factor
	glm::mat4 model_matrix(radius_);
	// set translation column
	model_matrix[3] = glm::vec4(position_, 1.0f);
	model_ptr_->Draw(projection_matrix, view_matrix, model_matrix);
}

std::pair<bool, float> BoundingSphere::RayIntersection(const Ray & ray) const
{
	// https://en.wikipedia.org/wiki/Line%E2%80%93sphere_intersection

	glm::vec3 sphere_center_to_ray_origin = ray.origin_ - position_;

	auto squared = [](float value) { return value * value; };

	float temp_dot = glm::dot(ray.direction_, sphere_center_to_ray_origin);

	float under_root = squared(temp_dot) -
		squared(glm::length(sphere_center_to_ray_origin)) +
		squared(radius_);

	if (under_root < 0.0f)
	{
		// no solution
		return std::make_pair(false, -1.0f);
	}
	else
	{
		// 1 or 2 solutions, choose nearest
		float distance_along_ray = -temp_dot - sqrt(under_root);

		// discard negative results in opposite direction of ray
		return distance_along_ray < 0.0f ? 
			std::make_pair(false, -1.0f) : 
			std::make_pair(true, distance_along_ray);
	}
}

bool BoundingSphere::NegativeHalfSpace(const Plane & plane) const
{
	return plane.SignedPointDistance(position_) <= radius_;
}


