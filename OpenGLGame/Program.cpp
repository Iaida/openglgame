#include "Program.h"

#include "Dependencies/glew/glew.h"

#include <fstream>
#include <iostream>
#include <vector>


Program::Program(const std::string & vertex_shader_file_path, const std::string & fragment_shader_file_path)
{
	program_ = glCreateProgram();

	// attach shaders
	GLuint vertex_shader = CreateShader(GL_VERTEX_SHADER, ReadShader(vertex_shader_file_path), vertex_shader_file_path);
	GLuint fragment_shader = CreateShader(GL_FRAGMENT_SHADER, ReadShader(fragment_shader_file_path), fragment_shader_file_path);
	glAttachShader(program_, vertex_shader);
	glAttachShader(program_, fragment_shader);

	// link 
	glLinkProgram(program_);
	GLint link_result = 0;
	glGetProgramiv(program_, GL_LINK_STATUS, &link_result);
	if (!link_result)
	{
		int info_log_length = 0;
		glGetProgramiv(program_, GL_INFO_LOG_LENGTH, &info_log_length);
		std::vector<char> program_log(info_log_length);
		glGetProgramInfoLog(program_, info_log_length, NULL, &program_log[0]);
		std::cout << "Shader Loader : LINK ERROR \n" << &program_log[0] << "\n";
	}
}

Program::Program()
{
}


Program::~Program()
{
	glDeleteProgram(program_);
}

std::string Program::ReadShader(const std::string & file_path)
{
	std::string shader_code;
	std::ifstream file(file_path, std::ios::in);

	if (!file.good())
	{
		std::cout << "Can't read file " << file_path.c_str() << "\n";
	}

	file.seekg(0, std::ios::end);
	shader_code.resize((unsigned int)file.tellg());
	file.seekg(0, std::ios::beg);
	file.read(&shader_code[0], shader_code.size());

	return shader_code;
}

GLuint Program::CreateShader(const GLenum shader_type, const std::string & shader_code, const std::string & shader_name)
{
	GLuint shader = glCreateShader(shader_type);
	const char *shader_code_ptr = shader_code.c_str();
	const int shader_code_size = (const int)shader_code.size();

	glShaderSource(shader, 1, &shader_code_ptr, &shader_code_size);
	glCompileShader(shader);
	GLint compile_result = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compile_result);

	//check for errors
	if (!compile_result)
	{
		int info_log_length = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_log_length);
		std::vector<char> shader_log(info_log_length);
		glGetShaderInfoLog(shader, info_log_length, NULL, &shader_log[0]);
		std::cout << "ERROR compiling shader: " << shader_name.c_str() << "\n" << &shader_log[0] << "\n";
		return 0;
	}
	return shader;
}
