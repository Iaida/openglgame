#include "Model.h"

#include "Dependencies/glew/glew.h"

Model::~Model()
{
	glDeleteVertexArrays(1, &vao_);
	glDeleteBuffers((GLsizei)vbos_.size(), &vbos_[0]);
}

void Model::CalculateMinMax(const std::vector<glm::vec3>& vertex_positons)
{
	glm::vec3 min(FLT_MAX, FLT_MAX, FLT_MAX);
	glm::vec3 max(FLT_MIN, FLT_MIN, FLT_MIN);

	for (auto & vertex_position : vertex_positons)
	{
		if (vertex_position.x < min.x)
			min.x = vertex_position.x;
		if (vertex_position.x > max.x)
			max.x = vertex_position.x;

		if (vertex_position.y < min.y)
			min.y = vertex_position.y;
		if (vertex_position.y > max.y)
			max.y = vertex_position.y;

		if (vertex_position.z < min.z)
			min.z = vertex_position.z;
		if (vertex_position.z > max.z)
			max.z = vertex_position.z;
	}
	min_ = min;
	max_ = max;
}
