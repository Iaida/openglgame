#pragma once
#include "Component.h"

class Model;

class Rendering : public Component<Rendering>
{
public:
	Rendering() {};
	Rendering(Model * model_ptr) : model_ptr_(model_ptr) {};
	
	Model * model_ptr_;
};

