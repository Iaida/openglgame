#pragma once
#include "Component.h"
#include "Dependencies\glm\vec3.hpp"


class Scaling : public Component<Scaling>
{
public:
	Scaling() : scaling_(glm::vec3(1.0f)) {}
	Scaling(const float x, const float y, const float z) : scaling_(glm::vec3(x, y, z)) {}
	Scaling(const float s) : scaling_(glm::vec3(s, s, s)) {}
	Scaling(const glm::vec3 & v) : scaling_(v) {}
	operator glm::vec3() { return scaling_; }
	Scaling& operator= (const glm::vec3& v) { scaling_ = v; return *this; }

	glm::vec3 scaling_;
};

