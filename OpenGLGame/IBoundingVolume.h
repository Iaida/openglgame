#pragma once

#include "Dependencies\glm\mat4x4.hpp"

#include <utility>

class BoundingSphere;
class OrientedBoundingBox;
class Ray;
class Plane;

class IBoundingVolume
{
public:
	virtual ~IBoundingVolume() {}

	virtual bool Collision(const IBoundingVolume & bounding_volume) const = 0;
	virtual bool CollisionBoundingSphere(const BoundingSphere & bounding_sphere) const = 0;
	virtual bool CollisionOrientedBoundingBox(const OrientedBoundingBox & oriented_bounding_box) const = 0;
	// returns positive distance to intersection point or -1.0f if no intersection occured
	virtual std::pair<bool, float> RayIntersection(const Ray & ray) const = 0;
	// returns true if at least part of the bounding volume exists in the plane's negative halfspace
	virtual bool NegativeHalfSpace(const Plane & plane) const = 0;

	virtual void Draw(const glm::mat4 & projection_matrix, const glm::mat4 & view_matrix) = 0;
	


	
};