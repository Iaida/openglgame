#pragma once

#include "AngularVelocity.h"
#include "BoundingSphere.h"
#include "ComponentContainer.h"
#include "OrientedBoundingBox.h"
#include "Position.h"
#include "Projectile.h"
#include "Rendering.h"
#include "RenderingUI.h"
#include "Rotation.h"
#include "Scaling.h"
#include "TypeList.h"
#include "Velocity.h"

#include "Dependencies\glm\mat4x4.hpp"

#include <unordered_map>
#include <bitset>
#include <memory>
#include <iostream>
#include <array>


class EntityComponentSystem
{
public:
	// loosely sorted by rate of occurance
	using ComponentList = TypeList<Position, Rendering, Scaling, Rotation, Velocity, AngularVelocity, BoundingSphere, OrientedBoundingBox, Projectile, RenderingUI>;
	using Bitmask = std::bitset<ComponentList::size() + 1>; // first bit reserved for 'alive'  or 'dead'

	EntityComponentSystem();
	~EntityComponentSystem();

	// updating systems
	void UpdateGameLoop(const float dt); // physics/ai/logic
	void UpdateFrame(); // render 3d(world with access to scale pos rot) or 2d(hud/menus quads with (min,max))
	
	// managing entities
	template<typename C1, typename... Cs>
	size_t CreateEntity(const C1 & first_component, const Cs & ... components);
	void RemoveDeadEntities();
	void ClearEntities();
	void KillEntity(const size_t id);

	// managing single components
	template<typename C>
	void AddReplaceComponent(const size_t id, const C & component);
	template<typename C>
	void RemoveComponent(const size_t id);

	void UpdateCameraEntity(const size_t id);

	template<typename C>
	C & GetComponent(const size_t id);

private:
	size_t id_counter_; // initiated with 1, use 0 for special cases
	size_t UniqueID(); // starts with 1 on first call

	std::unordered_map<size_t, Bitmask> id_bitmask_table_;
	std::array<std::unique_ptr<IComponentContainer>, ComponentList::size()> component_containers_;
	
	template<typename C>
	ComponentContainer<C> & GetComponentContainer();
	
	template<typename C1, typename... Cs> 
	void CreateEntityHelper(const size_t id, Bitmask & bitmask, const C1 & first_component, const Cs & ... components);
	void CreateEntityHelper(const size_t id, Bitmask & bitmask) {} // termination version

	// Systems
	void UpdatePhysics(const float dt);
	void Update3DRendering();
	void Update2DRendering();
	glm::mat4x4 ModelMatrix(const glm::vec3 & position, const glm::quat & quaternion, const glm::vec3 & scaling);
	
	template<typename C1, typename C2, typename... Cs>
	bool TestForComponents(const Bitmask & bitmask); // >=2 template parameters
	template<typename C1>
	bool TestForComponents(const Bitmask & bitmask); // 1 template parameter

	bool ComponentDependenciesSatisfied(const Bitmask & bitmask);

};


template<typename C1, typename ...Cs>
inline size_t EntityComponentSystem::CreateEntity(const C1 & first_component, const Cs & ...components)
{
	size_t unique_id = UniqueID();
	Bitmask bitmask;
	bitmask.set(0); // set alive bit at position 0 to 1

	// unique_id constant, bitmask changed via reference for each component creation
	CreateEntityHelper(unique_id, bitmask, first_component, components...);

	if (ComponentDependenciesSatisfied(bitmask))
	{
		id_bitmask_table_.emplace(unique_id, bitmask);
		return unique_id;
	}
	else
	{
		std::cout << "ComponentDependenciesSatisfied returned false \n";
		return 0;
	}
	
}

template<typename C>
inline void EntityComponentSystem::AddReplaceComponent(const size_t id, const C & component)
{
	Bitmask & bitmask = id_bitmask_table_[id];

	// check if bit is already set 
	if (TestForComponents<C>(bitmask))
	{
		// replace component
		GetComponentContainer<C>().GetByID(id) = component;
	}
	else
	{
		// create new component
		// call create for component's respective container object 
		GetComponentContainer<C>().Create(id, component);
		// set component's bit in bitmask
		bitmask.set(C::bit_position_);
	}
}

template<typename C>
inline void EntityComponentSystem::RemoveComponent(const size_t id)
{
	Bitmask & bitmask = id_bitmask_table_[id];

	// check if bit is set and calling function was justified
	if (TestForComponents<C>(bitmask))
	{
		// remove component from respective container
		GetComponentContainer<C>().Remove(id);
		// set respective bit to 0
		bitmask.reset(C::bit_position_);
	}
	else
	{
		std::cout << "Component could not be removed -> " << typeid(C).name() << " with bit position " << C::bit_position_ << " does not exist for id " << id << "\n";
	}
}

template<typename C>
inline C & EntityComponentSystem::GetComponent(const size_t id)
{
	return GetComponentContainer<C>().GetByID(id);
}


template<typename C>
inline ComponentContainer<C> & EntityComponentSystem::GetComponentContainer()
{
	return *static_cast<ComponentContainer<C> *>(component_containers_[C::bit_position_ - 1].get());
}

template<typename C1, typename ...Cs>
inline void EntityComponentSystem::CreateEntityHelper(const size_t id, Bitmask& bitmask, const C1 & first_component, const Cs & ...components)
{
	// check if bit is already set
	if (TestForComponents<C1>(bitmask))
	{
		// replace previous component
		GetComponentContainer<C1>().GetByID(id) = first_component;
		std::cout << " replacing component with bit position " << C1::bit_position_ << "\n";
	}
	else
	{
		// create new component
		// call create for component's respective container object 
		GetComponentContainer<C1>().Create(id, first_component);
		// set first component's bit in bitmask
		bitmask.set(C1::bit_position_);
	}

	// recursion without first component, calls termination version at the end
	CreateEntityHelper(id, bitmask, components...);
}

template<typename C1, typename C2, typename ...Cs>
inline bool EntityComponentSystem::TestForComponents(const Bitmask& bitmask)
{
	return bitmask.test(C1::bit_position_) ? TestForComponents<C2, Cs...>(bitmask) : false;
}

template<typename C1>
inline bool EntityComponentSystem::TestForComponents(const Bitmask& bitmask)
{
	return bitmask.test(C1::bit_position_);
}
