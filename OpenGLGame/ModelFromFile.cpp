#include "ModelFromFile.h"

#include "ModelLoader.h"

#include "Dependencies\glew\glew.h"
#include "Dependencies\glm\geometric.hpp"
#include "Dependencies\glm\mat3x3.hpp"
#include "Dependencies\glm\gtc\matrix_transform.hpp"



ModelFromFile::ModelFromFile(const std::string & file_path, Program * program)
{
	struct VertexFormat
	{
		glm::vec3 position_;
		glm::vec3 normal_;


		VertexFormat(const glm::vec3 &position, const glm::vec3 &normal)
		{
			position_ = position;
			normal_ = normal;
		}
		VertexFormat() {}
	};


	ModelLoader model_loader(file_path);
	number_vertices_ = model_loader.number_vertices_;
	number_faces_ = model_loader.number_faces_;

	GLuint vao_temp;
	GLuint vbo_temp;
	GLuint ibo_temp;

	glGenVertexArrays(1, &vao_temp);
	glBindVertexArray(vao_temp);

	glGenBuffers(1, &ibo_temp);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_temp);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, model_loader.index_buffer_.size() * sizeof(unsigned int), &model_loader.index_buffer_[0], GL_STATIC_DRAW);

	glGenBuffers(1, &vbo_temp);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_temp);
	glBufferData(GL_ARRAY_BUFFER, model_loader.vertex_buffer_.size() * sizeof(VertexFormat), &model_loader.vertex_buffer_[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(offsetof(VertexFormat, VertexFormat::position_)));

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(offsetof(VertexFormat, VertexFormat::normal_)));



	glBindVertexArray(0);

	vao_ = vao_temp;
	vbos_.push_back(vbo_temp);
	vbos_.push_back(ibo_temp);

	program_ = program;

	min_ = model_loader.min_vertex_;
	max_ = model_loader.max_vertex_;
	radius_ = model_loader.max_vertex_distance_;

}


ModelFromFile::~ModelFromFile()
{
}


void ModelFromFile::Draw(const glm::mat4 & projection_matrix, const glm::mat4 & view_matrix, const glm::mat4 & model_matrix)
{
	glUseProgram(*program_);

	glUniformMatrix4fv(glGetUniformLocation(*program_, "model_matrix"), 1, false, &model_matrix[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(*program_, "view_matrix"), 1, false, &view_matrix[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(*program_, "projection_matrix"), 1, false, &projection_matrix[0][0]);
	glm::mat3 normal_matrix = transpose(inverse(glm::mat3(/*view_matrix **/ model_matrix)));
	glUniformMatrix3fv(glGetUniformLocation(*program_, "normal_matrix"), 1, false, &normal_matrix[0][0]);

	glBindVertexArray(vao_);
	glDrawElements(GL_TRIANGLES, number_faces_ * 3, GL_UNSIGNED_INT, 0);
}


