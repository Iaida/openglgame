#include "Ray.h"

#include "IBoundingVolume.h"

#include "Dependencies\glm\geometric.hpp"

#include <cmath>



Ray::Ray()
{
}

Ray::Ray(const glm::vec3 & origin, const glm::vec3 & direction) : origin_(origin), direction_(direction)
{
}


Ray::~Ray()
{
}

std::pair<bool, float> Ray::Intersection(const IBoundingVolume & bounding_volume)
{
	return bounding_volume.RayIntersection(*this);
}


glm::vec3 Ray::PointAlongRay(const float distance)
{
	return origin_ + (direction_ * distance);
}
