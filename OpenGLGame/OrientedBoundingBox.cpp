#include "OrientedBoundingBox.h"

#include "Model.h"
#include "Ray.h"
#include "BoundingSphere.h"
#include "Plane.h"

#include "Dependencies\glm\gtc\matrix_transform.hpp"
#include "Dependencies\glm\mat3x3.hpp"

#include <cmath>
#include <algorithm>

OrientedBoundingBox::OrientedBoundingBox()
{
}

// initialize position so it won't be seen when initiated, only after first update
// initial_extent_: scaling from min(-1,-1,-1) max(1,1,1) cube to min max of OBB
// TODO: still initiated like aabb, not rotated
OrientedBoundingBox::OrientedBoundingBox(Model * model_ptr, Model * target_model_ptr) :
	model_ptr_(model_ptr), initial_extent_(target_model_ptr->max_), extent_(initial_extent_),
	position_(glm::vec3(FLT_MAX)), rotation_(glm::quat())
{
}

OrientedBoundingBox::~OrientedBoundingBox()
{
}

bool OrientedBoundingBox::Collision(const IBoundingVolume & bounding_volume) const
{
	return bounding_volume.CollisionOrientedBoundingBox(*this);
}

bool OrientedBoundingBox::CollisionBoundingSphere(const BoundingSphere & bounding_sphere) const
{
	// OBB-Sphere 

	// Source: 'Real-Time Collision Detection' by Christer Ericson

	// Find point p on OBB closest to sphere center, maybe return ???
	glm::vec3 p = ClosestPointOBB(bounding_sphere.position_);
	glm::vec3 sphere_center_to_p = p - bounding_sphere.position_;
	// Sphere and OBB intersect if the (squared) distance from sphere center to point p is less than the (squared) sphere radius
	// dot with same parameters is squared length
	return glm::dot(sphere_center_to_p, sphere_center_to_p) <= bounding_sphere.radius_ * bounding_sphere.radius_;
}

bool OrientedBoundingBox::CollisionOrientedBoundingBox(const OrientedBoundingBox & oriented_bounding_box) const
{
	// OBB-OBB
	// Source: 'Real-Time Collision Detection' by Christer Ericson
	float ra, rb;
	glm::mat3 R, AbsR;
	glm::mat3 axes_orientation = glm::toMat3(rotation_);
	glm::mat3 target_axes_orientation = glm::toMat3(oriented_bounding_box.rotation_);

	// Compute rotation matrix expressing b in a�s coordinate frame
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			R[i][j] = glm::dot(axes_orientation[i], target_axes_orientation[j]);
	
	// Compute translation vector t
	glm::vec3 t = oriented_bounding_box.position_ - position_;
	// Bring translation into a's coordinate frame
	// ERROR in y-component: 'glm::dot(t, axes_orientation[1])' instead of 'glm::dot(t, axes_orientation[2])'
	t = glm::vec3(glm::dot(t, axes_orientation[0]), glm::dot(t, axes_orientation[1]), glm::dot(t, axes_orientation[2]));

	// Compute common subexpressions. Add in an epsilon term to
	// counteract arithmetic errors when two edges are parallel and
	// their cross product is (near) null 
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			AbsR[i][j] = glm::abs(R[i][j]) + FLT_EPSILON;

	// Test axes L = A0, L = A1, L = A2
	for (int i = 0; i < 3; i++)
	{
		ra = extent_[i];
		rb = oriented_bounding_box.extent_[0] * AbsR[i][0] + oriented_bounding_box.extent_[1] * AbsR[i][1] + oriented_bounding_box.extent_[2] * AbsR[i][2];
		if (glm::abs(t[i]) > ra + rb)
			return false;
	}

	// Test axes L = B0, L = B1, L = B2
	for (int i = 0; i < 3; i++) 
	{
		ra = extent_[0] * AbsR[0][i] + extent_[1] * AbsR[1][i] + extent_[2] * AbsR[2][i];
		rb = oriented_bounding_box.extent_[i];
		if (glm::abs(t[0] * R[0][i] + t[1] * R[1][i] + t[2] * R[2][i]) > ra + rb) 
			return false;
	}

	// Test axis L = A0 x B0
	ra = extent_[1] * AbsR[2][0] + extent_[2] * AbsR[1][0];
	rb = oriented_bounding_box.extent_[1] * AbsR[0][2] + oriented_bounding_box.extent_[2] * AbsR[0][1];
	if (glm::abs(t[2] * R[1][0] - t[1] * R[2][0]) > ra + rb) 
		return 0;
	// Test axis L = A0 x B1
	ra = extent_[1] * AbsR[2][1] + extent_[2] * AbsR[1][1];
	rb = oriented_bounding_box.extent_[0] * AbsR[0][2] + oriented_bounding_box.extent_[2] * AbsR[0][0];
	if (glm::abs(t[2] * R[1][1] - t[1] * R[2][1]) > ra + rb) 
		return 0;
	// Test axis L = A0 x B2
	ra = extent_[1] * AbsR[2][2] + extent_[2] * AbsR[1][2];
	rb = oriented_bounding_box.extent_[0] * AbsR[0][1] + oriented_bounding_box.extent_[1] * AbsR[0][0];
	if (glm::abs(t[2] * R[1][2] - t[1] * R[2][2]) > ra + rb) 
		return 0;
	// Test axis L = A1 x B0
	ra = extent_[0] * AbsR[2][0] + extent_[2] * AbsR[0][0];
	rb = oriented_bounding_box.extent_[1] * AbsR[1][2] + oriented_bounding_box.extent_[2] * AbsR[1][1];
	if (glm::abs(t[0] * R[2][0] - t[2] * R[0][0]) > ra + rb) 
		return 0;
	// Test axis L = A1 x B1
	ra = extent_[0] * AbsR[2][1] + extent_[2] * AbsR[0][1];
	rb = oriented_bounding_box.extent_[0] * AbsR[1][2] + oriented_bounding_box.extent_[2] * AbsR[1][0];
	if (glm::abs(t[0] * R[2][1] - t[2] * R[0][1]) > ra + rb) 
		return 0;
	// Test axis L = A1 x B2
	ra = extent_[0] * AbsR[2][2] + extent_[2] * AbsR[0][2];
	rb = oriented_bounding_box.extent_[0] * AbsR[1][1] + oriented_bounding_box.extent_[1] * AbsR[1][0];
	if (glm::abs(t[0] * R[2][2] - t[2] * R[0][2]) > ra + rb) 
		return 0;
	// Test axis L = A2 x B0
	ra = extent_[0] * AbsR[1][0] + extent_[1] * AbsR[0][0];
	rb = oriented_bounding_box.extent_[1] * AbsR[2][2] + oriented_bounding_box.extent_[2] * AbsR[2][1];
	if (glm::abs(t[1] * R[0][0] - t[0] * R[1][0]) > ra + rb) 
		return 0;
	// Test axis L = A2 x B1
	ra = extent_[0] * AbsR[1][1] + extent_[1] * AbsR[0][1];
	rb = oriented_bounding_box.extent_[0] * AbsR[2][2] + oriented_bounding_box.extent_[2] * AbsR[2][0];
	if (glm::abs(t[1] * R[0][1] - t[0] * R[1][1]) > ra + rb) 
		return 0;
	// Test axis L = A2 x B2
	ra = extent_[0] * AbsR[1][2] + extent_[1] * AbsR[0][2];
	rb = oriented_bounding_box.extent_[0] * AbsR[2][1] + oriented_bounding_box.extent_[1] * AbsR[2][0];
	if (glm::abs(t[1] * R[0][2] - t[0] * R[1][2]) > ra + rb) 
		return 0;
	// Since no separating axis is found, the OBBs must be intersecting
	return true;
}

void OrientedBoundingBox::Draw(const glm::mat4 & projection_matrix, const glm::mat4 & view_matrix)
{
	model_ptr_->Draw(projection_matrix, view_matrix, ModelMatrix());
}

std::pair<bool, float> OrientedBoundingBox::RayIntersection(const Ray & ray) const
{
	// transform ray into OBB's local space
	glm::mat4 model_matrix = ModelMatrix();
	glm::mat4 inverse_model_matrix = glm::inverse(model_matrix);
	Ray local_ray(glm::vec3(inverse_model_matrix * glm::vec4(ray.origin_, 1.0f)), 
		glm::normalize(glm::vec3(inverse_model_matrix * glm::vec4(ray.direction_, 0.0f))));

	// AABB-Ray intersection test based on: http://psgraphics.blogspot.de/2016/02/new-simple-ray-box-test-from-andrew.html

	float t_min = 0.0f;
	float t_max = FLT_MAX;
	glm::vec3 inv_dir = glm::vec3(1.0f) / local_ray.direction_;

	for (unsigned i = 0; i < 3; i++)
	{
		float t_0 = (-1.0f - local_ray.origin_[i]) * inv_dir[i];
		float t_1 = (1.0f - local_ray.origin_[i]) * inv_dir[i];
		if (inv_dir[i] < 0.0f)
			std::swap(t_0, t_1);
		if (t_0 > t_min)
			t_min = t_0;
		if (t_1 < t_max)
			t_max = t_1;
		// changed from '<=' to '<' for the case of only one intersection point
		if (t_max < t_min)
			return std::make_pair(false, -1.0f);
	}

	// intersection point(s) transformed back to world space
	glm::vec3 min_intersection_world = glm::vec3(model_matrix * glm::vec4(local_ray.PointAlongRay(t_min), 1.0f));
	// measure distance anew because of non-uniform scaling in model matrix
	return std::make_pair(true, glm::length(min_intersection_world - ray.origin_));

}

bool OrientedBoundingBox::NegativeHalfSpace(const Plane & plane) const
{
	// Source: 'Real-Time Collision Detection' by Christer Ericson

	glm::mat3 axes_orientation = glm::toMat3(rotation_);
	// Compute the projection interval radius of OBB onto L(t) = obb center + t * plane normal
	float r = extent_[0] * glm::abs(glm::dot(plane.normal_, axes_orientation[0])) +
		extent_[1] * glm::abs(glm::dot(plane.normal_, axes_orientation[1])) +
		extent_[2] * glm::abs(glm::dot(plane.normal_, axes_orientation[2]));
	// Compute distance of box center from plane
	float s = plane.SignedPointDistance(position_);
	// OBB lies partially in negative halfspace of the plane
	// -> NOT fully in the positive halfspace -> !(r <= s)
	return r > s;
}

glm::vec3 OrientedBoundingBox::ClosestPointOBB(const glm::vec3 & point) const
{
	// Based on: 'Real-Time Collision Detection' by Christer Ericson 
	// normally works with local axes member variables instead of quaternion

	glm::vec3 pos_to_point = point - position_;
	// Start result at center of box; make steps from there
	glm::vec3 result = position_;
	glm::mat3 axes_orientation = glm::toMat3(rotation_);

	// For each OBB axis...
	for (unsigned i = 0; i < 3; i++) {
		// ...project d onto that axis to get the distance
		// along the axis of d from the box center
		float distance = glm::dot(pos_to_point, axes_orientation[i]);
		// If distance farther than the box extents, clamp to the box
		if (distance > extent_[i]) 
			distance = extent_[i];
		if (distance < -extent_[i])
			distance = -extent_[i];
		// Step that distance along the axis to get world coordinate
		result += distance * axes_orientation[i];
	}
	return result;
}

glm::mat4 OrientedBoundingBox::ModelMatrix() const
{
	return glm::translate(glm::mat4(1.0f), position_) * glm::toMat4(rotation_) * glm::scale(glm::mat4(1.0f), extent_);
}
