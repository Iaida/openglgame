#pragma once

#include "Dependencies\sdl\include\SDL.h"
#include <array>

enum CardinalDirection
{
	NONE,
	NORTH,
	NORTHEAST,
	EAST,
	SOUTHEAST,
	SOUTH,
	SOUTHWEST,
	WEST,
	NORTHWEST
};

class SDLInputEvents
{
public:
	int mouse_x_rel_;
	int mouse_y_rel_;
	float mouse_sensitivity_;
	

	SDLInputEvents();
	~SDLInputEvents();

	// process events, mouse, keyboard, controller
	void Process();

	// Keyboard

	bool KeyPressed(const SDL_Scancode key);
	bool KeyReleased(const SDL_Scancode key);
	bool KeyDown(const SDL_Scancode key);
	CardinalDirection WASDCardinalDirection();

	// Controller

	CardinalDirection LeftStickCardinalDirection();
	bool RightStickXInDeadzone();
	bool RightStickYInDeadzone();
	bool RightStickInDeadzone();
	bool LeftStickXInDeadzone();
	bool LeftStickYInDeadzone();
	bool LeftStickInDeadzone();
	bool LeftTriggerInDeadzone();
	bool RightTriggerInDeadzone();
	Sint16 LeftTriggerValue();
	Sint16 RightTriggerValue();

	bool ButtonPressed(const SDL_GameControllerButton button);
	bool ButtonReleased(const SDL_GameControllerButton button);
	bool ButtonDown(const SDL_GameControllerButton button);
	float RightStickXValue();
	float RightStickYValue();
	void SetVibration(const unsigned short left, const unsigned short right);
	
	
	// Mouse

	// use 'SDL_BUTTON_LEFT', 'SDL_BUTTON_MIDDLE' or 'SDL_BUTTON_RIGHT'
	bool MouseButtonPressed(const int sdl_button);
	// use 'SDL_BUTTON_LEFT', 'SDL_BUTTON_MIDDLE' or 'SDL_BUTTON_RIGHT'
	bool MouseButtonReleased(const int sdl_button);
	// use 'SDL_BUTTON_LEFT', 'SDL_BUTTON_MIDDLE' or 'SDL_BUTTON_RIGHT'
	bool MouseButtonDown(const int sdl_button);

private:
	const Uint8 * keyboard_state_current_;
	Uint8 * keyboard_state_previous_;
	int number_keyboard_keys_;
	Uint32 mouse_state_current_;
	Uint32 mouse_state_previous_;

	static const int MAX_NUM_CONTROLLERS = 4;
	SDL_GameController * pad_;
	SDL_Joystick * joy_;
	std::array<bool, SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_MAX> gamepad_buttons_current_;
	std::array<bool, SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_MAX> gamepad_buttons_previous_;
	
	void ProcessEvents();
	void ProcessKeyboard();
	void ProcessMouse();
	void ProcessController();
	void AddController(const int id);
	void RemoveController(const int id);
	bool LeftStickValueInDeadzone(const Sint16 value);
	bool RightStickValueInDeadzone(const Sint16 value);
	// returns values between -1 and +1
	float ShortToFloat(const Sint16 sdl_short);

};

