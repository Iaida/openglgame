#pragma once
#include <map>
#include <iostream>
#include <string>
#include <memory>

template<typename R>
class ResourceContainer
{
public:
	void Delete(const std::string & resource_name);
	R * Get(const std::string & resource_name);
	void Clear();
	template <class C, typename ...Ts>
	R * Add(const std::string & resource_name, Ts && ...constructor_parameters);

private:
	std::map<std::string, std::unique_ptr<R>> resources_;
};



template<typename R>
inline void ResourceContainer<R>::Delete(const std::string & resource_name)
{
	resources_.erase(resource_name);
}

template<typename R>
inline R * ResourceContainer<R>::Get(const std::string & resource_name)
{
	std::map<std::string, std::unique_ptr<R>>::iterator it = resources_.find(resource_name);
	return (it != resources_.end()) ? it->second.get() : nullptr;
}

template<typename R>
inline void ResourceContainer<R>::Clear()
{
	resources_.clear();
}

template<typename R>
template <class C, typename ...Ts>
inline R * ResourceContainer<R>::Add(const std::string & resource_name, Ts && ...constructor_parameters)
{
	R * get_result = Get(resource_name);
	if (get_result != nullptr)
	{
		return get_result;
	}
	else
	{
		std::unique_ptr<R> resource = std::make_unique<C>(std::forward<Ts>(constructor_parameters)...);
		return resources_.emplace(resource_name, std::move(resource)).first->second.get();
	}
}

