#include "CubeMaterial.h"


#include "Locator.h"
#include "Dependencies\glm\geometric.hpp"
#include "Dependencies\glm\gtx\transform.hpp"
#include "TextureContainer.h"
#include "Camera.h"



CubeMaterial::CubeMaterial(Program * program)
{
	struct VertexFormat
	{
		glm::vec3 position_;
		glm::vec3 normal_;
		glm::vec2 uv_;

		VertexFormat(const glm::vec3 &position, const glm::vec3 &normal, const glm::vec2 &uv)
		{
			position_ = position;
			normal_ = normal;
			uv_ = uv;
		}
		VertexFormat() {}
	};



	GLuint vao;
	GLuint vbo;
	GLuint ibo;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// http://in2gpu.com/wp-content/uploads/2015/07/c2_2_cube_index.jpg
	// corrected ccw for: right, back, upper faces
	std::vector<unsigned int> indices = {
		0,  1,  2,  0,  2,  3,   //front
		4,  6,  5,  4,  7,  6,   //right
		8,  10, 9,  8,  11, 10,  //back
		12, 13, 14, 12, 14, 15,  //left
		16, 18, 17, 16, 19, 18,  //upper
		20, 21, 22, 20, 22, 23 }; //bottom

								  // still contains duplicated positions, but different normals and texture coordinates
	std::vector<VertexFormat> vertices;

	// front
	vertices.push_back(VertexFormat(glm::vec3(-1.0, -1.0, 1.0),
		glm::vec3(0.0, 0.0, 1.0),
		glm::vec2(0, 0)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(0.0, 0.0, 1.0),
		glm::vec2(1, 0)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(0.0, 0.0, 1.0),
		glm::vec2(1, 1)));
	vertices.push_back(VertexFormat(glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(0.0, 0.0, 1.0),
		glm::vec2(0, 1)));

	// right
	vertices.push_back(VertexFormat(glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(1.0, 0.0, 0.0),
		glm::vec2(0, 1)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(1.0, 0.0, 0.0),
		glm::vec2(1, 1)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, -1.0, -1.0),
		glm::vec3(1.0, 0.0, 0.0),
		glm::vec2(1, 0)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(1.0, 0.0, 0.0),
		glm::vec2(0, 0)));

	// back
	vertices.push_back(VertexFormat(glm::vec3(-1.0, -1.0, -1.0),
		glm::vec3(0.0, 0.0, -1.0),
		glm::vec2(1, 0)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, -1.0, -1.0),
		glm::vec3(0.0, 0.0, -1.0),
		glm::vec2(0, 0)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(0.0, 0.0, -1.0),
		glm::vec2(0, 1)));
	vertices.push_back(VertexFormat(glm::vec3(-1.0, 1.0, -1.0),
		glm::vec3(0.0, 0.0, -1.0),
		glm::vec2(1, 1)));

	// left
	vertices.push_back(VertexFormat(glm::vec3(-1.0, -1.0, -1.0),
		glm::vec3(-1.0, 0.0, 0.0),
		glm::vec2(0, 0)));
	vertices.push_back(VertexFormat(glm::vec3(-1.0, -1.0, 1.0),
		glm::vec3(-1.0, 0.0, 0.0),
		glm::vec2(1, 0)));
	vertices.push_back(VertexFormat(glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(-1.0, 0.0, 0.0),
		glm::vec2(1, 1)));
	vertices.push_back(VertexFormat(glm::vec3(-1.0, 1.0, -1.0),
		glm::vec3(-1.0, 0.0, 0.0),
		glm::vec2(0, 1)));

	// upper
	vertices.push_back(VertexFormat(glm::vec3(1.0, 1.0, 1.0),
		glm::vec3(0.0, 1.0, 0.0),
		glm::vec2(1, 0)));
	vertices.push_back(VertexFormat(glm::vec3(-1.0, 1.0, 1.0),
		glm::vec3(0.0, 1.0, 0.0),
		glm::vec2(0, 0)));
	vertices.push_back(VertexFormat(glm::vec3(-1.0, 1.0, -1.0),
		glm::vec3(0.0, 1.0, 0.0),
		glm::vec2(0, 1)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, 1.0, -1.0),
		glm::vec3(0.0, 1.0, 0.0),
		glm::vec2(1, 1)));

	// bottom
	vertices.push_back(VertexFormat(glm::vec3(-1.0, -1.0, -1.0),
		glm::vec3(0.0, -1.0, 0.0),
		glm::vec2(0, 0)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, -1.0, -1.0),
		glm::vec3(0.0, -1.0, 0.0),
		glm::vec2(1, 0)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, -1.0, 1.0),
		glm::vec3(0.0, -1.0, 0.0),
		glm::vec2(1, 1)));
	vertices.push_back(VertexFormat(glm::vec3(-1.0, -1.0, 1.0),
		glm::vec3(0.0, -1.0, 0.0),
		glm::vec2(0, 1)));

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(VertexFormat), &vertices[0], GL_STATIC_DRAW);

	glGenBuffers(1, &ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(offsetof(VertexFormat, VertexFormat::position_)));

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(offsetof(VertexFormat, VertexFormat::normal_)));

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(offsetof(VertexFormat, VertexFormat::uv_)));

	glBindVertexArray(0);
	vao_ = vao;
	vbos_.push_back(vbo);
	vbos_.push_back(ibo);

	texture_diffuse_ = Locator<TextureContainer>::service()->Get("container_diffuse");
	texture_specular_ = Locator<TextureContainer>::service()->Get("container_specular");

	program_ = program;

	min_ = glm::vec3(-1.0, -1.0, -1.0);
	max_ = glm::vec3(1.0, 1.0, 1.0);
	
}

CubeMaterial::~CubeMaterial()
{
}


void CubeMaterial::Draw(const glm::mat4 & projection_matrix, const glm::mat4 & view_matrix, const glm::mat4 & model_matrix)
{
	glUseProgram(*program_);
	glBindVertexArray(vao_);

	// MATRICES FOR VERTEXSHADER
	glUniformMatrix4fv(glGetUniformLocation(*program_, "model_matrix"), 1, false, &model_matrix[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(*program_, "view_matrix"), 1, false, &view_matrix[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(*program_, "projection_matrix"), 1, false, &projection_matrix[0][0]);
	glm::mat3 normal_matrix = transpose(inverse(glm::mat3(/*view_matrix **/ model_matrix)));
	glUniformMatrix3fv(glGetUniformLocation(*program_, "normal_matrix"), 1, false, &normal_matrix[0][0]);

	// CAMERA POSITION FOR FRAGMENTSHADER
	glUniform3f(glGetUniformLocation(*program_, "camera_position"), Locator<Camera>::service()->position_.x, Locator<Camera>::service()->position_.y, Locator<Camera>::service()->position_.z);

	// MATERIAL FOR FRAGMENTSHADER
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, *texture_diffuse_);
	glUniform1i(glGetUniformLocation(*program_, "material.diffuse"), 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, *texture_specular_);
	glUniform1i(glGetUniformLocation(*program_, "material.specular"), 1);

	glUniform1f(glGetUniformLocation(*program_, "material.shininess"), 32.0f);

	// LIGHT FOR FRAGMENTSHADER
	glm::vec3 light_dir = glm::normalize(glm::vec3(-1.0f, -1.0f, -1.0f));
	glUniform3f(glGetUniformLocation(*program_, "light.direction"), light_dir.x, light_dir.y, light_dir.z);
	glUniform3f(glGetUniformLocation(*program_, "light.ambient"), 0.2f, 0.2f, 0.2f);
	glUniform3f(glGetUniformLocation(*program_, "light.diffuse"), 0.5f, 0.5f, 0.5f);
	glUniform3f(glGetUniformLocation(*program_, "light.specular"), 1.0f, 1.0f, 1.0f);

	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
}
