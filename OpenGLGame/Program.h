#pragma once

#include <string>

typedef unsigned int GLuint;
typedef unsigned int GLenum;

class Program
{
public:
	Program(const std::string & vertex_shader_file_path, const std::string & fragment_shader_file_path);
	Program();
	~Program();
	// implicit conversion
	operator GLuint() const { return program_; }
	GLuint program_;

private:
	std::string ReadShader(const std::string & file_path);
	GLuint CreateShader(const GLenum shader_type, const std::string & source, const std::string & shader_name);
};

