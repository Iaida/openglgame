#include "SDLInputEvents.h"

#include "Locator.h"
#include "LevelLoader.h"
#include "Options.h"
#include "Camera.h"
#include "EntityComponentSystem.h"
#include "GameInfo.h"


#include <Windows.h>
#include <Xinput.h>
#pragma comment(lib, "XInput.lib")

#include <iostream>

#include "Dependencies\glm\geometric.hpp"

SDLInputEvents::SDLInputEvents()
{
	if (SDL_SetRelativeMouseMode(SDL_FALSE) < 0)
		std::cout << "SDL_SetRelativeMouseMode failed \n";

	// initialize key state arrays with values
	SDL_PumpEvents();
	keyboard_state_current_ = SDL_GetKeyboardState(&number_keyboard_keys_);
	keyboard_state_previous_ = new Uint8[number_keyboard_keys_];
	for (int i = 0; i < number_keyboard_keys_; i++) 
	{ 
		keyboard_state_previous_[i] = '\0'; 
	}

	mouse_sensitivity_ = 0.10f;
}

SDLInputEvents::~SDLInputEvents()
{
}

void SDLInputEvents::Process()
{
	// using event filters -> force an event queue update
	SDL_PumpEvents();
	ProcessEvents();

	// functions process general input first, then call their respective state function
	ProcessKeyboard();	
	ProcessMouse();
	ProcessController();
	
}

void SDLInputEvents::ProcessEvents()
{
	SDL_Event event;
	int num_events = SDL_PeepEvents(&event, 1, SDL_GETEVENT, SDL_FIRSTEVENT, SDL_APP_TERMINATING);;
	while (num_events != 0)
	{
		if (event.type == SDL_QUIT || event.type == SDL_APP_TERMINATING)
			Locator<GameInfo>::service()->set_quitting(true);

		num_events = SDL_PeepEvents(&event, 1, SDL_GETEVENT, SDL_FIRSTEVENT, SDL_APP_TERMINATING);
	};

	
}

void SDLInputEvents::ProcessKeyboard()
{
	// update
	keyboard_state_current_ = SDL_GetKeyboardState(NULL);

	if (KeyDown(SDL_SCANCODE_LALT) && KeyPressed(SDL_SCANCODE_F4))
		Locator<GameInfo>::service()->set_quitting(true);

	Locator<GameInfo>::service()->game_state()->ProcessKeyboard();

	// save old values
	std::memcpy(keyboard_state_previous_, keyboard_state_current_, sizeof(Uint8)*number_keyboard_keys_);
}

void SDLInputEvents::ProcessMouse()
{
	// update
	mouse_state_current_ = SDL_GetRelativeMouseState(&mouse_x_rel_, &mouse_y_rel_);

	Locator<GameInfo>::service()->game_state()->ProcessMouse();

	// save old values
	mouse_state_previous_ = mouse_state_current_;
}

void SDLInputEvents::ProcessController()
{
	//SDL_GameControllerUpdate();

	SDL_Event controller_events[MAX_NUM_CONTROLLERS];
	SDL_PeepEvents(controller_events, MAX_NUM_CONTROLLERS, SDL_GETEVENT, SDL_CONTROLLERDEVICEADDED, SDL_CONTROLLERDEVICEREMOVED);
	for (int c = 0; c < MAX_NUM_CONTROLLERS; c++)
	{
		if (controller_events[c].type == SDL_CONTROLLERDEVICEADDED)
		{
			AddController(controller_events[c].cdevice.which);
		}
		else if (controller_events[c].type == SDL_CONTROLLERDEVICEREMOVED)
		{
			RemoveController(controller_events[c].cdevice.which);
		}
	}

	if (!SDL_GameControllerGetAttached(pad_) || !Locator<Options>::service()->gamepad())
		return;

	if (!LeftTriggerInDeadzone())
	{
		std::cout << "Controller Trigger Left down \n";
	}

	if (!RightTriggerInDeadzone())
	{
		std::cout << "Controller Trigger Right down \n";
	}


	// update buttons
	for (int b = 0; b != SDL_GameControllerButton::SDL_CONTROLLER_BUTTON_MAX; b++)
	{
		gamepad_buttons_current_[b] = (SDL_GameControllerGetButton(pad_, static_cast<SDL_GameControllerButton>(b)) != 0);
	}



	if (ButtonPressed(SDL_CONTROLLER_BUTTON_A))
		std::cout << "Controller Button A down \n";
	if (ButtonPressed(SDL_CONTROLLER_BUTTON_B))
		std::cout << "Controller Button B down \n";
	if (ButtonPressed(SDL_CONTROLLER_BUTTON_X))
		std::cout << "Controller Button X down \n";
	if (ButtonPressed(SDL_CONTROLLER_BUTTON_Y))
		std::cout << "Controller Button Y down \n";
	if (ButtonPressed(SDL_CONTROLLER_BUTTON_GUIDE))
		std::cout << "Controller Button Guide down \n";
	if (ButtonPressed(SDL_CONTROLLER_BUTTON_START))
		std::cout << "Controller Button Start down \n";
	if (ButtonPressed(SDL_CONTROLLER_BUTTON_BACK))
		std::cout << "Controller Button Back down \n";
	if (ButtonPressed(SDL_CONTROLLER_BUTTON_LEFTSHOULDER))
		std::cout << "Controller Button Left Shoulder down \n";
	if (ButtonPressed(SDL_CONTROLLER_BUTTON_RIGHTSHOULDER))
		std::cout << "Controller Button Right Shoulder down \n";
	if (ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_UP))
		std::cout << "Controller Button DPad Up down \n";
	if (ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_DOWN))
		std::cout << "Controller Button DPad Down down \n";
	if (ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_LEFT))
		std::cout << "Controller Button DPad Left down \n";
	if (ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_RIGHT))
		std::cout << "Controller Button DPad Right down \n";

	Locator<GameInfo>::service()->game_state()->ProcessController();

	// save old button states
	gamepad_buttons_previous_ = gamepad_buttons_current_;
}

bool SDLInputEvents::KeyPressed(const SDL_Scancode key)
{
	return (keyboard_state_current_[key] && (!keyboard_state_previous_[key]));
}

bool SDLInputEvents::KeyReleased(const SDL_Scancode key)
{
	return !keyboard_state_current_[key] && keyboard_state_previous_[key];
}

bool SDLInputEvents::KeyDown(const SDL_Scancode key)
{
	return keyboard_state_current_[key] != 0;
}

CardinalDirection SDLInputEvents::WASDCardinalDirection()
{
	bool w = false;
	bool a = false;
	bool s = false;
	bool d = false;

	if (KeyDown(SDL_SCANCODE_W))
		w = true;
	if (KeyDown(SDL_SCANCODE_A))
		a = true;
	if (KeyDown(SDL_SCANCODE_S))
		s = true;
	if (KeyDown(SDL_SCANCODE_D))
		d = true;

	if (!w && !a && !s && !d)
		return CardinalDirection::NONE;
	else if (w && !a && !d)
		return CardinalDirection::NORTH;
	else if (w && d)
		return CardinalDirection::NORTHEAST;
	else if (d && !w && !s)
		return CardinalDirection::EAST;
	else if (d && s)
		return CardinalDirection::SOUTHEAST;
	else if (s && !d && !a)
		return CardinalDirection::SOUTH;
	else if (s && a)
		return CardinalDirection::SOUTHWEST;
	else if (a && !s && !w)
		return CardinalDirection::WEST;
	else if (a && w)
		return CardinalDirection::NORTHWEST;
	else
		return CardinalDirection::NONE;
}


float SDLInputEvents::ShortToFloat(const Sint16 sdl_short)
{
	// returns values between -1 and +1
	return static_cast<float>(sdl_short) / static_cast<float>(SHRT_MAX);
}

bool SDLInputEvents::ButtonPressed(const SDL_GameControllerButton button)
{
	return gamepad_buttons_current_[button] && !gamepad_buttons_previous_[button];
}

bool SDLInputEvents::ButtonReleased(const SDL_GameControllerButton button)
{
	return !gamepad_buttons_current_[button] && gamepad_buttons_previous_[button];
}

bool SDLInputEvents::ButtonDown(const SDL_GameControllerButton button)
{
	return gamepad_buttons_current_[button];
}

float SDLInputEvents::RightStickXValue()
{
	return ShortToFloat(SDL_GameControllerGetAxis(Locator<SDLInputEvents>::service()->pad_, SDL_CONTROLLER_AXIS_RIGHTX));
}

float SDLInputEvents::RightStickYValue()
{
	return ShortToFloat(SDL_GameControllerGetAxis(Locator<SDLInputEvents>::service()->pad_, SDL_CONTROLLER_AXIS_RIGHTY));
}

void SDLInputEvents::SetVibration(const unsigned short left_low_frequency, const unsigned short right_high_frequency)
{
	XINPUT_VIBRATION vibration;
	ZeroMemory(&vibration, sizeof(XINPUT_VIBRATION));
	vibration.wLeftMotorSpeed = left_low_frequency; // use any value between 0-65535 here
	vibration.wRightMotorSpeed = right_high_frequency; // use any value between 0-65535 here
	XInputSetState(0, &vibration);
}

bool SDLInputEvents::MouseButtonPressed(const int sdl_button)
{
	return (mouse_state_current_ & SDL_BUTTON(sdl_button)) && !(mouse_state_previous_ & SDL_BUTTON(sdl_button));
}

bool SDLInputEvents::MouseButtonReleased(const int sdl_button)
{
	return !(mouse_state_current_ & SDL_BUTTON(sdl_button)) && (mouse_state_previous_ & SDL_BUTTON(sdl_button));
}

bool SDLInputEvents::MouseButtonDown(const int sdl_button)
{
	return (mouse_state_current_ & SDL_BUTTON(sdl_button)) != 0;
}



void SDLInputEvents::AddController(const int id)
{
	std::cout << "Adding Controller with ID " << id << " \n";
	if (SDL_IsGameController(id))
	{
		pad_ = SDL_GameControllerOpen(id);

		if (pad_)
		{
			joy_ = SDL_GameControllerGetJoystick(pad_);
			int instanceID = SDL_JoystickInstanceID(joy_);
			// mapping: joystick IDs to controllers ?
		}
	}
}

void SDLInputEvents::RemoveController(const int id)
{
	std::cout << "Removing Controller with ID " << id << " \n";
	SDL_GameControllerClose(pad_);
	pad_ = nullptr;
	joy_ = nullptr;
}

bool SDLInputEvents::LeftStickValueInDeadzone(const Sint16 value)
{
	return (value < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE && value > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
}

bool SDLInputEvents::RightStickValueInDeadzone(const Sint16 value)
{
	return (value < XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE && value > -XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
}

bool SDLInputEvents::RightStickXInDeadzone()
{
	return RightStickValueInDeadzone(SDL_GameControllerGetAxis(Locator<SDLInputEvents>::service()->pad_, SDL_CONTROLLER_AXIS_RIGHTX));
}

bool SDLInputEvents::RightStickYInDeadzone()
{
	return RightStickValueInDeadzone(SDL_GameControllerGetAxis(Locator<SDLInputEvents>::service()->pad_, SDL_CONTROLLER_AXIS_RIGHTY));
}

bool SDLInputEvents::RightStickInDeadzone()
{
	return RightStickXInDeadzone() && RightStickYInDeadzone();
}

bool SDLInputEvents::LeftStickXInDeadzone()
{
	return LeftStickValueInDeadzone(SDL_GameControllerGetAxis(Locator<SDLInputEvents>::service()->pad_, SDL_CONTROLLER_AXIS_LEFTX));
}

bool SDLInputEvents::LeftStickYInDeadzone()
{
	return LeftStickValueInDeadzone(SDL_GameControllerGetAxis(Locator<SDLInputEvents>::service()->pad_, SDL_CONTROLLER_AXIS_LEFTY));
}

bool SDLInputEvents::LeftStickInDeadzone()
{
	return LeftStickXInDeadzone() && LeftStickYInDeadzone();
}


bool SDLInputEvents::LeftTriggerInDeadzone()
{
	return SDL_GameControllerGetAxis(pad_, SDL_CONTROLLER_AXIS_TRIGGERLEFT) < XINPUT_GAMEPAD_TRIGGER_THRESHOLD;
}

bool SDLInputEvents::RightTriggerInDeadzone()
{
	return SDL_GameControllerGetAxis(pad_, SDL_CONTROLLER_AXIS_TRIGGERRIGHT) < XINPUT_GAMEPAD_TRIGGER_THRESHOLD;
}

Sint16 SDLInputEvents::LeftTriggerValue()
{
	Sint16 value = SDL_GameControllerGetAxis(pad_, SDL_CONTROLLER_AXIS_TRIGGERLEFT);
	return (value < XINPUT_GAMEPAD_TRIGGER_THRESHOLD) ? 0 : value;
}

Sint16 SDLInputEvents::RightTriggerValue()
{
	Sint16 value = SDL_GameControllerGetAxis(pad_, SDL_CONTROLLER_AXIS_TRIGGERRIGHT);
	return (value < XINPUT_GAMEPAD_TRIGGER_THRESHOLD) ? 0 : value;
}




CardinalDirection SDLInputEvents::LeftStickCardinalDirection()
{

	if (LeftStickInDeadzone())
	{
		return CardinalDirection::NONE;
	}

	float left_stick_x = ShortToFloat(SDL_GameControllerGetAxis(pad_, SDL_CONTROLLER_AXIS_LEFTX));
	float left_stick_y = -ShortToFloat(SDL_GameControllerGetAxis(pad_, SDL_CONTROLLER_AXIS_LEFTY));

	// normalize 2D vector
	float length = sqrt(left_stick_x*left_stick_x + left_stick_y*left_stick_y);
	float normalized_x = left_stick_x / length;
	float normalized_y = left_stick_y / length;

	// get one of 8 cardinal directions like wasd
	// reference vector north
	float north_x = 0.0f;
	float north_y = 1.0f;

	// angle between normalized and north vector, result only between 0 and pi, no sign
	float angle_rad = acos(normalized_x * north_x + normalized_y * north_y);

	// range between 0 and 8, corresponds to eights of pi
	unsigned angle_section = (unsigned)(angle_rad * 8.0f / (float)M_PI);

	// right half
	if (left_stick_x >= 0.0f)
	{
		switch (angle_section)
		{
		case 0:
			return CardinalDirection::NORTH;
		case 1:
			return CardinalDirection::NORTHEAST;
		case 2:
			return CardinalDirection::NORTHEAST;
		case 3:
			return CardinalDirection::EAST;
		case 4:
			return CardinalDirection::EAST;
		case 5:
			return CardinalDirection::SOUTHEAST;
		case 6:
			return CardinalDirection::SOUTHEAST;
		default: // case 7+8
			return CardinalDirection::SOUTH;
		}
	}
	// left half
	else
	{
		switch (angle_section)
		{
		case 0:
			return CardinalDirection::NORTH;
		case 1:
			return CardinalDirection::NORTHWEST;
		case 2:
			return CardinalDirection::NORTHWEST;
		case 3:
			return CardinalDirection::WEST;
		case 4:
			return CardinalDirection::WEST;
		case 5:
			return CardinalDirection::SOUTHWEST;
		case 6:
			return CardinalDirection::SOUTHWEST;
		default: // case 7+8
			return CardinalDirection::SOUTH;
		}
	}
}

