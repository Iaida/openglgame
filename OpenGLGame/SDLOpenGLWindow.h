#pragma once

//#include "Dependencies\sdl\include\SDL.h"
#include "Dependencies\glm\mat4x4.hpp"

#include <string>

struct SDL_Window;

class SDLOpenGLWindow
{
public:
	SDLOpenGLWindow();
	~SDLOpenGLWindow();
	
	void SetVSync(const bool v_sync);
	void SetFullscreen(const bool fullscreen);
	void UpdateDisplay();
	void SetResolution(const int width, const int height);
	void SetWindowTitle(const std::string & window_title);
	bool Focus() const;
	void ToggleBackFaceCulling();
	void ToggleWireFrameMode();
	glm::mat4 ProjectionMatrix();

private:
	
	SDL_Window * window_;

	void Init();
	void Error(const char * message);
};

