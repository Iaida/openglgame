#pragma once
#include "Plane.h"

#include "Dependencies\glm\mat4x4.hpp"

#include <array>

class Frustum
{
public:
	Frustum();
	Frustum(const glm::mat4 & view_projection);
	~Frustum();
	bool Contained(const IBoundingVolume & bounding_volume);
	

	std::array<Plane, 6> planes_;
};

