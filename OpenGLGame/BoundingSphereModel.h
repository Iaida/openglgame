#pragma once
#include "Model.h"

#include "Dependencies\glm\vec3.hpp"

class BoundingSphereModel :
	public Model
{
public:
	BoundingSphereModel(Program * program);
	~BoundingSphereModel();

	virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::mat4& model_matrix)	override final;

private:
	int number_vertices_;
	int number_faces_;

	glm::vec3 UnitCubeToUnitSphere(const glm::vec3 & vector);
	void Subdivide(std::vector<unsigned int> & index_buffer, std::vector<glm::vec3> & vertex_buffer);
	// returns index
	unsigned AddVertex(const glm::vec3 & vector, std::vector<glm::vec3> & vertex_pos_buffer);
};



