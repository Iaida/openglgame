#pragma once

#include <vector>

#include "Dependencies\glm\vec3.hpp"



// http://www.andrewnoske.com/wiki/Code_-_heatmaps_and_color_gradients

class ColorGradient
{
private:
	struct GradientPoint 
	{
		float val;  // Position of our color along the gradient (between 0 and 1).
		glm::vec3 col;
		GradientPoint(const float value, const glm::vec3 &color)
			: col(color), val(value) {}
	};
	std::vector<GradientPoint> gradient;  // An array of points in ascending value.

public:
	ColorGradient();
	void addGradientPoint(const float value, const glm::vec3 &color);
	void clearGradient();
	void createDefaultGradient();
	void createEarthGradient();
	glm::vec3 getColorAtValue(const float value);
};

