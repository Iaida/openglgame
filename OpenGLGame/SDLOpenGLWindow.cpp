#include "SDLOpenGLWindow.h"

#include "Locator.h"
#include "Camera.h"
#include "Options.h"

#include "Dependencies\sdl\include\SDL.h"
#include "Dependencies\glew\glew.h"
#include "Dependencies\sdl\include\SDL_opengl.h"
#include "Dependencies\glm\gtc\matrix_transform.hpp"

#include <gl\glu.h>
#include <string>
#include <iostream>
#include <Xinput.h>

SDLOpenGLWindow::SDLOpenGLWindow()
{
	window_ = NULL;
	Init();
}


SDLOpenGLWindow::~SDLOpenGLWindow()
{
}

void SDLOpenGLWindow::Error(const char * message)
{
	fprintf(stderr, "%s: %s\n", message, SDL_GetError());
	exit(2);
}

void SDLOpenGLWindow::Init()
{
	// Initialize SDL 

	// allow gamepad input even if application is in background
	SDL_SetHint(SDL_HINT_JOYSTICK_ALLOW_BACKGROUND_EVENTS, "1");

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		Error("Couldn't initialize SDL Video subsystem");
	if (SDL_Init(SDL_INIT_GAMECONTROLLER) < 0)
		Error("Couldn't initialize SDL Gamecontroller subsystem");

	atexit(SDL_Quit);
	SDL_GL_LoadLibrary(NULL); // Default OpenGL is fine.
	
	// Request an OpenGL 4.5 context (should be core)
	SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
	// Also request a depth buffer
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	
	// Create the window
	if (Locator<Options>::service()->fullscreen())
	{
		window_ = SDL_CreateWindow(
			Locator<Options>::service()->title().c_str(),
			SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
			0, 0, 
			SDL_WINDOW_FULLSCREEN | SDL_WINDOW_OPENGL
			);
	}
	else // WINDOWED
	{
		window_ = SDL_CreateWindow(
			Locator<Options>::service()->title().c_str(),
			SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			Locator<Options>::service()->width(), Locator<Options>::service()->height(), 
			SDL_WINDOW_OPENGL
			);
	}
	if (window_ == NULL) 
		Error("Couldn't set video mode");
	
	if (SDL_GL_CreateContext(window_) == NULL)
		Error("Failed to create OpenGL context");

	
	// Check OpenGL properties
	printf("OpenGL loaded\n");
	printf("Vendor:   %s\n", glGetString(GL_VENDOR));
	printf("Renderer: %s\n", glGetString(GL_RENDERER));
	printf("Version:  %s\n", glGetString(GL_VERSION));
	

	glViewport(0, 0, Locator<Options>::service()->width(), Locator<Options>::service()->height());

	// Use v-sync
	SDL_GL_SetSwapInterval(Locator<Options>::service()->vertical_sync());

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	
	// Wireframe Mode
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	// Fill Polygons
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// Face Culling
	glEnable(GL_CULL_FACE); // GL_BACK default face to be culled
	

	// Blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glewExperimental = GL_TRUE;
	// init glew
	if (GLEW_OK != glewInit())
		std::cout << glewGetErrorString(glewInit()) << "\n";
}

bool SDLOpenGLWindow::Focus() const
{
	return (SDL_GetWindowFlags(window_) & SDL_WINDOW_INPUT_FOCUS) != 0;
}

void SDLOpenGLWindow::ToggleBackFaceCulling()
{
	glIsEnabled(GL_CULL_FACE) ? glDisable(GL_CULL_FACE) : glEnable(GL_CULL_FACE);
}

void SDLOpenGLWindow::ToggleWireFrameMode()
{
	GLint mode;
	glGetIntegerv(GL_POLYGON_MODE, &mode);
	(mode == GL_FILL) ? glPolygonMode(GL_FRONT_AND_BACK, GL_LINE) :	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

glm::mat4 SDLOpenGLWindow::ProjectionMatrix()
{
	// glm:: perspective takes radians instead of degrees
	return glm::perspective(Locator<Camera>::service()->zoom_ * glm::radians(Locator<Options>::service()->fov_vertical_deg()),
		Locator<Options>::service()->aspect_ratio(),
		Locator<Options>::service()->near_distance(),
		Locator<Options>::service()->far_distance());
}

	

// https://wiki.libsdl.org/SDL_GL_SetSwapInterval
void SDLOpenGLWindow::SetVSync(const bool v_sync)
{
	if (v_sync)
	{
		// try to enable late swap tearing
		if (SDL_GL_SetSwapInterval(-1) == -1)
		{
			// late swap tearing not available, use normal vsync
			SDL_GL_SetSwapInterval(1);
		}
	}
	else
	{
		// immediate updates
		SDL_GL_SetSwapInterval(0);
	}
	
}

void SDLOpenGLWindow::SetFullscreen(const bool fullscreen)
{
	if (fullscreen)
	{
		SDL_SetWindowFullscreen(window_, SDL_WINDOW_FULLSCREEN);
		SDL_SetWindowGrab(window_, SDL_TRUE);
	}
	else // windowed
	{
		SDL_SetWindowFullscreen(window_, 0);
		SDL_SetWindowGrab(window_, SDL_FALSE);
	}	
}

void SDLOpenGLWindow::UpdateDisplay()
{
	SDL_GL_SwapWindow(window_);
}



void SDLOpenGLWindow::SetResolution(const int width, const int height)
{
	if (Locator<Options>::service()->fullscreen())
	{
		// to avoid weird errors: 
		// fullscreen -> windowed -> resize -> fullscreen
		SetFullscreen(false);

		SDL_SetWindowSize(window_, width, height);
		SDL_SetWindowPosition(window_, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
		glViewport(0, 0, width, height);
		SetFullscreen(true);
	}
	else // WINDOWED
	{
		SDL_SetWindowSize(window_, width, height);
		SDL_SetWindowPosition(window_, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
		glViewport(0, 0, width, height);
	}
	
}

void SDLOpenGLWindow::SetWindowTitle(const std::string & window_title)
{
	SDL_SetWindowTitle(window_, window_title.c_str());
}

