#pragma once
#include <vector>

// https://solarianprogrammer.com/2012/07/18/perlin-noise-cpp-11/

class SimplexNoise
{
public:
	
	
	// initialized with reference permutation vector
	SimplexNoise();
	SimplexNoise(unsigned long long seed);
	
	float noise(float x, float y, float z);
	float fBM(float x, float y, float z, unsigned octaves, float lacunarity, float gain);
	std::vector<int> PermutationVector();

	float sNMin;
	float sNMax;
	float fBMMin;
	float fBMMax;

private:
	// permutation vector
	// randomly shuffled numbers from 0 to 255 based on a seed
	std::vector<int> p;

	float fade(float t);
	float lerp(float t, float a, float b);
	float grad(int hash, float x, float y, float z);
};


