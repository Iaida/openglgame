#include "RNG.h"

#include "Dependencies\glm\geometric.hpp"

#include <time.h>

std::mt19937_64 RNG::engine_(time(NULL));

float RNG::GetFloat(const float min, const float max)
{
	return std::uniform_real_distribution<float>(min, max)(engine_);
}

glm::vec3 RNG::GetDirection()
{
	return glm::normalize(glm::vec3(GetFloat(-1.0f, 1.0f), GetFloat(-1.0f, 1.0f), GetFloat(-1.0f, 1.0f)));
}
