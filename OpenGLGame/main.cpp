#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "GlU32.lib")
#pragma comment(lib, "glew32.lib")
#ifdef _DEBUG
#pragma comment(lib, "SOILdebug.lib")
#pragma comment(lib, "SDL2debug.lib")
#pragma comment(lib, "SDL2maindebug.lib")
#else // RELEASE
#pragma comment(lib, "SOILrelease.lib")
#pragma comment(lib, "SDL2release.lib")
#pragma comment(lib, "SDL2mainrelease.lib")
#endif

#include "Camera.h"
#include "EntityComponentSystem.h"
#include "Frustum.h"
#include "GameInfo.h"
#include "LevelLoader.h"
#include "Locator.h"
#include "ModelContainer.h"
#include "Options.h"
#include "SDLInputEvents.h"
#include "SDLOpenGLWindow.h"
#include "ProgramContainer.h"
#include "Stats.h"
#include "TextureContainer.h"

#include <iostream>
#include <string>

int main(int argc, char* args[]) 
{	
	//Plane plane(glm::vec3(-1.0f, 0.0f, -1.0f), glm::vec3(-1.0f, 0.0f, 1.0f), glm::vec3(1.0f, 0.0f, -1.0f));
	
	std::ios::sync_with_stdio(false);

	Options options("Resources\\Config.cfg", "Resources\\Config default.cfg");
	Locator<Options>::provide(options);

	SDLOpenGLWindow sdl_opengl_window;
	Locator<SDLOpenGLWindow>::provide(sdl_opengl_window);

	SDLInputEvents sdl_input_events;
	Locator<SDLInputEvents>::provide(sdl_input_events);

	Camera camera;
	Locator<Camera>::provide(camera);
	
	ProgramContainer shader_container;
	Locator<ProgramContainer>::provide(shader_container);
		
	ModelContainer model_container;
	Locator<ModelContainer>::provide(model_container);

	TextureContainer texture_container;
	Locator<TextureContainer>::provide(texture_container);
		
	EntityComponentSystem entity_component_system;
	Locator<EntityComponentSystem>::provide(entity_component_system);

	LevelLoader level_loader;
	Locator<LevelLoader>::provide(level_loader);
	level_loader.LoadLevel1();

	GameInfo game_info;
	Locator<GameInfo>::provide(game_info);

	// TIME/FPS VARIABLES
	const float dt = 1.0f / 120.0f; // time step
	float previous_time = 0.0f;
	float accumulator = 0.0f;

	unsigned frame_counter = 0;
	float last_fps_display = 0.0f;
	
	
	while (!game_info.quitting())
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		// Background color
		//glClearColor(0.0, 0.0, 0.0, 1.0);
		glClearColor(1.0, 1.0, 1.0, 1.0);
		
		// TIME variables in seconds
		const float current_time = static_cast<float>(SDL_GetTicks()) / 1000.0f;
		const float frame_time = current_time - previous_time;
		previous_time = current_time;
		
		if(!game_info.TestGameState<Paused>())
			accumulator += frame_time;
		
		// FPS COUNTER
		last_fps_display += frame_time;
		// update title every few milliseconds
		if (last_fps_display > 0.25f)
		{	
			unsigned fps = static_cast<unsigned>(static_cast<float>(frame_counter) / last_fps_display);
			sdl_opengl_window.SetWindowTitle(options.title() + " - " + std::to_string(fps) + " FPS");
			last_fps_display = 0.0f;
			frame_counter = 0;

			// update debug info for ray picking in same interval
			if (camera.picking_ray_min_distance_id_ != 0)
				std::cout << "Ray picking id: " << camera.picking_ray_min_distance_id_ << ", distance: " << camera.picking_ray_min_distance_ << "\n";
		}
		
				
		// GAME LOGIC / PHYSICS
		while (accumulator >= dt && !game_info.TestGameState<Paused>())
		{
			accumulator -= dt;
			entity_component_system.UpdateGameLoop(dt);
			camera.Update(dt);
		}
		
		// EVENTS + INPUT
		sdl_input_events.Process();

		// render entities
		entity_component_system.UpdateFrame();

		// render to screen
		sdl_opengl_window.UpdateDisplay();

		frame_counter++;
	}

	options.SaveChanges();
	return 0;
}