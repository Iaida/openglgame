#include "GameInfo.h"
#include <iostream>



GameInfo::GameInfo()
{
	quitting_ = false;

	size_t game_state_index_counter = 0;
	LambdaForEach(
		[&](auto gs)
		{
			static_assert(std::is_base_of<IGameState, decltype(gs)>::value, "Type not derived from IGameState");
			gs.game_state_index_ = game_state_index_counter++;
			game_states_[decltype(gs)::game_state_index_] = std::make_unique<decltype(gs)>();
			std::cout << typeid(decltype(gs)).name() << " gets the game_state_index " << decltype(gs)::game_state_index_ << "\n";

		},
		GameStateList());
	
	set_game_state<Paused>();
}


GameInfo::~GameInfo()
{
}

IGameState * GameInfo::game_state() const
{
	return game_state_;
}

bool GameInfo::quitting() const
{
	return quitting_;
}

void GameInfo::set_quitting(const bool quitting)
{
	quitting_ = quitting;
}
