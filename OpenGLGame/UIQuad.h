#pragma once
#include "Model.h"
#include "Texture.h"
#include "Dependencies\glm\mat4x4.hpp"

class UIQuad : 
	public Model
{
public:
	UIQuad(Program * program);
	~UIQuad();
	
	Texture * texture_;

	
	virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::mat4& model_matrix) override final;
	
};

