#pragma once

class IGameState
{
public:
	virtual ~IGameState() {}
	virtual void ProcessKeyboard() = 0;
	virtual void ProcessController() = 0;
	virtual void ProcessMouse() = 0;
	virtual size_t GetGameStateIndex() = 0;
};

