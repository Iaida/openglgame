#include "Quad.h"

#include "Dependencies\glew\glew.h"
#include "Dependencies\glm\vec3.hpp"
#include "Dependencies\glm\vec4.hpp"


Quad::Quad(Program * program)
{
	struct VertexFormat
	{
		glm::vec3 position_;
		glm::vec4 color_;

		VertexFormat(const glm::vec3 &position, const glm::vec4 &color)
		{
			position_ = position;
			color_ = color;
		}
		VertexFormat() {}
	};

	GLuint vao;
	GLuint vbo;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// 0 2
	// 1 3
	std::vector<VertexFormat> vertices;
	vertices.push_back(VertexFormat(glm::vec3(-1.0f, 0.0f, -1.0f),//pos
		glm::vec4(0.3f, 0.3f, 0.3f, 1)));   //color
	vertices.push_back(VertexFormat(glm::vec3(-1.0f, 0.0f, 1.0f),//pos
		glm::vec4(0.3f, 0.3f, 0.3f, 1)));   //color
	vertices.push_back(VertexFormat(glm::vec3(1.0f, 0.0f, -1.0f),//pos
		glm::vec4(0.3f, 0.3f, 0.3f, 1)));   //color				
	vertices.push_back(VertexFormat(glm::vec3(1.0f, 0.0f, 1.0f),//pos
		glm::vec4(0.3f, 0.3f, 0.3f, 1)));   //color

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);            //here we have 4
	glBufferData(GL_ARRAY_BUFFER, sizeof(VertexFormat) * 4, &vertices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(offsetof(VertexFormat, VertexFormat::position_)));

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(offsetof(VertexFormat, VertexFormat::color_)));

	glBindVertexArray(0);
	vao_ = vao;
	vbos_.push_back(vbo);

	program_ = program;

	std::vector<glm::vec3> temp_positions;
	for (auto & vertex : vertices) { temp_positions.push_back(vertex.position_); }
	CalculateMinMax(temp_positions);
}


Quad::~Quad()
{
}



void Quad::Draw(const glm::mat4 & projection_matrix, const glm::mat4 & view_matrix, const glm::mat4 & model_matrix)
{
	glUseProgram(*program_);
	glUniformMatrix4fv(glGetUniformLocation(*program_, "model_matrix"), 1, false, &model_matrix[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(*program_, "view_matrix"), 1, false, &view_matrix[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(*program_, "projection_matrix"), 1, false, &projection_matrix[0][0]);

	glBindVertexArray(vao_);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}
