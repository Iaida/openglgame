#pragma once
#include "ColorGradient.h"

#include "Dependencies/glew/glew.h"

#include <string>

class Texture
{
public:
	Texture(const std::string & texture_file_path);
	Texture(ColorGradient & color_gradient);
	~Texture();
	Texture();

	// implicit conversion
	operator GLuint() const;

	GLuint texture_;

};

