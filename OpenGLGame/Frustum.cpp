#include "Frustum.h"

#include "IBoundingVolume.h"

#include "Dependencies\glm\matrix.hpp"
#include "Dependencies\glm\gtc\matrix_access.hpp"


Frustum::Frustum()
{
}

Frustum::Frustum(const glm::mat4 & view_projection)
{
	//// http://gamedev.stackexchange.com/questions/29999/how-do-i-create-a-bounding-frustum-from-a-view-projection-matrix

	//// start with 8 corners of (-1,-1,-1) to (1,1,1) cube for NDC
	//std::array<glm::vec3, 8> corners =
	//{
	//	glm::vec3(-1.0f, -1.0f, 1.0f), // 0
	//	glm::vec3(1.0f, -1.0f, 1.0f), // 1
	//	glm::vec3(1.0f, 1.0f, 1.0f), // 2
	//	glm::vec3(-1.0f, 1.0f, 1.0f), // 3

	//	glm::vec3(-1.0f, -1.0f, -1.0f), // 4
	//	glm::vec3(1.0f, -1.0f, -1.0f), // 5
	//	glm::vec3(1.0f, 1.0f, -1.0f), // 6
	//	glm::vec3(-1.0f, 1.0f, -1.0f) // 7
	//};

	//// multiply corners by inverse view_projection, divide by w component 
	//glm::mat4 inverse_view_projection = glm::inverse(view_projection);
	//for (glm::vec3 & corner : corners)
	//{
	//	glm::vec4 temp_result = inverse_view_projection * glm::vec4(corner, 1.0f);
	//	corner = glm::vec3(temp_result / temp_result.w);
	//}
	////// CCW from inside, CW from outisde, not working
	////// near
	////planes_[0] = Plane(corners[0], corners[1], corners[2]);
	////// far
	////planes_[1] = Plane(corners[4], corners[6], corners[5]);
	////// top
	////planes_[2] = Plane(corners[2], corners[7], corners[3]);
	////// bottom
	////planes_[3] = Plane(corners[1], corners[0], corners[4]);
	////// right
	////planes_[4] = Plane(corners[1], corners[5], corners[2]);
	////// left
	////planes_[5] = Plane(corners[0], corners[3], corners[4]);

	//// CW from outside, CCW from inside, working
	//// near
	//planes_[0] = Plane(corners[2], corners[1], corners[0]);
	//// far
	//planes_[1] = Plane(corners[5], corners[6], corners[4]);
	//// top
	//planes_[2] = Plane(corners[3], corners[7], corners[2]);
	//// bottom
	//planes_[3] = Plane(corners[4], corners[0], corners[1]);
	//// right
	//planes_[4] = Plane(corners[2], corners[5], corners[1]);
	//// left
	//planes_[5] = Plane(corners[4], corners[3], corners[0]);

	

	// Microsoft XNA Game Studio http://pastie.org/4012070
	// http://www.technologicalutopia.com/sourcecode/xnageometry/boundingfrustum.cs.htm
	
	// near
	planes_[0].normal_.x = -view_projection[0][2];
	planes_[0].normal_.y = -view_projection[1][2];
	planes_[0].normal_.z = -view_projection[2][2];
	planes_[0].distance_ = -view_projection[3][2];
	// far
	planes_[1].normal_.x = -view_projection[0][3] + view_projection[0][2];
	planes_[1].normal_.y = -view_projection[1][3] + view_projection[1][2];
	planes_[1].normal_.z = -view_projection[2][3] + view_projection[2][2];
	planes_[1].distance_ = -view_projection[3][3] + view_projection[3][2];
	// left
	planes_[2].normal_.x = -view_projection[0][3] - view_projection[0][0];
	planes_[2].normal_.y = -view_projection[1][3] - view_projection[1][0];
	planes_[2].normal_.z = -view_projection[2][3] - view_projection[2][0];
	planes_[2].distance_ = -view_projection[3][3] - view_projection[3][0];
	// right
	planes_[3].normal_.x = -view_projection[0][3] + view_projection[0][0];
	planes_[3].normal_.y = -view_projection[1][3] + view_projection[1][0];
	planes_[3].normal_.z = -view_projection[2][3] + view_projection[2][0];
	planes_[3].distance_ = -view_projection[3][3] + view_projection[3][0];
	// top 
	planes_[4].normal_.x = -view_projection[0][3] + view_projection[0][1];
	planes_[4].normal_.y = -view_projection[1][3] + view_projection[1][1];
	planes_[4].normal_.z = -view_projection[2][3] + view_projection[2][1];
	planes_[4].distance_ = -view_projection[3][3] + view_projection[3][1];
	// bottom
	planes_[5].normal_.x = -view_projection[0][3] - view_projection[0][1];
	planes_[5].normal_.y = -view_projection[1][3] - view_projection[1][1];
	planes_[5].normal_.z = -view_projection[2][3] - view_projection[2][1];
	planes_[5].distance_ = -view_projection[3][3] - view_projection[3][1];
	
	for (auto & plane : planes_)
	{
		float normal_length = glm::length(plane.normal_);
		plane.normal_ /= normal_length;
		plane.distance_ /= -normal_length; // negated for plane implementation
	}
}

Frustum::~Frustum()
{
}

bool Frustum::Contained(const IBoundingVolume & bounding_volume)
{
	// volume has to (partially) exist in the negative halfspace of each of the six planes
	for (auto & plane : planes_)
	{
		if (!bounding_volume.NegativeHalfSpace(plane))
			return false;
	}
	return true;
}
