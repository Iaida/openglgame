#pragma once
#include "Component.h"
#include "Dependencies\glm\vec3.hpp"
#include "Dependencies\glm\gtc\quaternion.hpp"
#include "Dependencies\glm\gtx\quaternion.hpp"

class Rotation : public Component<Rotation>
{
public:
	Rotation() : rotation_(glm::quat(glm::radians(glm::vec3(0.0f, 0.0f, 0.0f)))) {}
	Rotation(const float deg_x, const  float deg_y, const float deg_z) : rotation_(/*glm::normalize(*/glm::quat(glm::radians(glm::vec3(deg_x, deg_y, deg_z)))/*)*/) {}
	Rotation(const glm::vec3 & deg_v) : rotation_(/*glm::normalize(*/glm::quat(glm::radians(deg_v))/*)*/) {}
	Rotation(const glm::quat & rotation) : rotation_(/*glm::normalize(*/rotation/*)*/) {}

	operator glm::quat() { return rotation_; }
	Rotation& operator= (const glm::quat& q) { rotation_ = q;  return *this; }

	glm::quat rotation_;
	
};

