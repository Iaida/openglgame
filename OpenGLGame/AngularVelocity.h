#pragma once
#include "Component.h"
#include "Dependencies\glm\vec3.hpp"
#include "Dependencies\glm\gtc\quaternion.hpp"
#include "Dependencies\glm\gtx\quaternion.hpp"


class AngularVelocity : public Component<AngularVelocity>
{
public:
	AngularVelocity() : angular_velocity_(glm::radians(glm::vec3(0.0f, 0.0f, 0.0f))) {}
	AngularVelocity(const float deg_x, const  float deg_y, const float deg_z) : angular_velocity_(glm::radians(glm::vec3(deg_x, deg_y, deg_z))) {}
	AngularVelocity(const glm::vec3 & deg_v) : angular_velocity_(glm::radians(deg_v)) {}


	operator glm::vec3() { return angular_velocity_; }
	AngularVelocity& operator= (const glm::vec3& q) { angular_velocity_ = q;  return *this; }

	glm::vec3 angular_velocity_;
};
