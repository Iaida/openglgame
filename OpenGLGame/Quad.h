#pragma once
#include "Model.h"

#include "Dependencies\glm\mat4x4.hpp"

class Quad : public Model
{
public:
	Quad(Program * program);
	~Quad();

	virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::mat4& model_matrix)	override final;
	
};

