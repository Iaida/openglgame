#version 450 core

uniform sampler1D gradient;
uniform int permutation_vector[512];
uniform float diameter;

in vec3 position;

out vec4 out_color;


float grad(int hash, float x, float y, float z) 
{
	int h = hash & 15;
	// Convert lower 4 bits of hash into 12 gradient directions
	float u = h < 8 ? x : y,
		v = h < 4 ? y : h == 12 || h == 14 ? x : z;
	return ((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v);
}

float lerp(float t, float a, float b) 
{
	return a + t * (b - a);
}

float fade(float t)
{
	return t * t * t * (t * (t * 6 - 15) + 10);
}

// returns values from -1.0 to +1.0
float noise(float x, float y, float z) 
{
	// Find the unit cube that contains the point
	int X = int(floor(x)) & 255;
	int Y = int(floor(y)) & 255;
	int Z = int(floor(z)) & 255;

	// Find relative x, y, z of point in cube
	x -= floor(x);
	y -= floor(y);
	z -= floor(z);

	// Compute fade curves for each of x, y, z
	float u = fade(x);
	float v = fade(y);
	float w = fade(z);

	// Hash coordinates of the 8 cube corners
	int A = permutation_vector[X] + Y;
	int AA = permutation_vector[A] + Z;
	int AB = permutation_vector[A + 1] + Z;
	int B = permutation_vector[X + 1] + Y;
	int BA = permutation_vector[B] + Z;
	int BB = permutation_vector[B + 1] + Z;

	// Add blended results from 8 corners of cube
	float res = lerp(w, lerp(v, lerp(u, grad(permutation_vector[AA], x, y, z), 
										 grad(permutation_vector[BA], x - 1, y, z)), 
								 lerp(u, grad(permutation_vector[AB], x, y - 1, z), 
										 grad(permutation_vector[BB], x - 1, y - 1, z))), 
								 lerp(v, lerp(u, grad(permutation_vector[AA + 1], x, y, z - 1), 
										 grad(permutation_vector[BA + 1], x - 1, y, z - 1)), 
								 lerp(u, grad(permutation_vector[AB + 1], x, y - 1, z - 1), 
										 grad(permutation_vector[BB + 1], x - 1, y - 1, z - 1))));

	// values from -1.0 to +1.0
	return res;

	// values from 0.0 to +1.0
	//res = (res + 1.0) / 2.0;
}

float fBM(float x, float y, float z, uint octaves, float lacunarity, float gain)
{
	float sum = 0.0;
	
	float frequency = 1.0 / diameter;
	float amplitude = 1.0;

	for (uint i = 0; i < octaves; i++)
	{
		sum += noise(x * frequency, y * frequency, z * frequency) * amplitude;
		frequency *= lacunarity;
		amplitude *= gain;
	}

	// out of bounds
	if (sum > 1.0)
		sum = 1.0;
	if (sum < -1.0)
		sum = -1.0;

	return sum;
}

void main()
{
	float noise_value = fBM(position.x, position.y, position.z, 8, 2.0f, 0.75f);
	// map from -1 to +1 to 0 to +1
	noise_value = (noise_value + 1.0f) * 0.5f;

	out_color = texture(gradient, noise_value);
}