#version 450 core
 
struct Material 
{
	sampler2D diffuse;
    sampler2D specular;
    float     shininess;
};  

struct Light 
{
    vec3 direction;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

in vec3 fragment_position;
in vec3 normal;
in vec2 uv;

uniform vec3 camera_position;
uniform Material material;
uniform Light light;

out vec4 out_color;

void main()
{
	// Ambient
	vec3 ambient = light.ambient * vec3(texture(material.diffuse, uv));

	// Diffuse 
	vec3 towards_light = normalize(-light.direction);
	float diffuse_value = max(dot(normal, towards_light), 0.0f);
	vec3 diffuse = light.diffuse * diffuse_value * vec3(texture(material.diffuse, uv));

	// Specular
	vec3 towards_camera = normalize(camera_position - fragment_position);
	vec3 reflection_direction = reflect(-towards_light, normal);
	float specular_value = pow(max(dot(towards_camera, reflection_direction), 0.0f), material.shininess);
	vec3 specular = light.specular * specular_value * vec3(texture(material.specular, uv));

	out_color = vec4(ambient + diffuse + specular, 1.0f);
}