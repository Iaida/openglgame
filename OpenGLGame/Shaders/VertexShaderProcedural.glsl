#version 450 core
layout(location = 0) in vec3 in_position;

uniform mat4 projection_matrix, view_matrix, model_matrix;

out vec3 position;
 
void main()
{
	gl_Position = projection_matrix * view_matrix * model_matrix * vec4(in_position, 1.0);


	position = in_position;
}