#version 450 core
layout(location = 0) in vec2 in_position;
layout(location = 1) in vec2 in_uv;
  
uniform mat4 model_matrix;

out vec2 uv;
 
void main()
{
	gl_Position = model_matrix * vec4(in_position.x, in_position.y, 0.0, 1.0);
	// We swap the y-axis by substracing our coordinates from 1. This is done because most images have the top y-axis inversed with OpenGL's top y-axis.
	uv = vec2(in_uv.x, 1.0 - in_uv.y);
}