#version 450 core
layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_normal;
 
uniform mat4 projection_matrix, view_matrix, model_matrix;
uniform mat3 normal_matrix;
 
out vec4 color;
 
void main()
{
	vec3 normal_rotated = normalize(normal_matrix * in_normal);
	color = vec4(normal_rotated * 0.5f + vec3(0.5f, 0.5f, 0.5f), 1.0f);
	gl_Position = projection_matrix * view_matrix * model_matrix * vec4(in_position, 1.0);   
}