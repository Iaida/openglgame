#version 450 core
 
layout(location = 0) out vec4 out_color;

uniform sampler2D texture1;

in vec2 uv;

void main()
{
  vec4 texture_color = texture(texture1, uv);
  out_color = texture_color;
}