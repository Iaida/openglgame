#version 450 core
layout(location = 0) in vec3 in_position;
layout(location = 1) in vec3 in_normal;
layout(location = 2) in vec2 in_uv;
 
uniform mat4 projection_matrix, view_matrix, model_matrix;
uniform mat3 normal_matrix;

out vec3 normal;
out vec2 uv;
out vec3 fragment_position;
 
void main()
{
	gl_Position = projection_matrix * view_matrix * model_matrix * vec4(in_position, 1.0);

	// swap the y-axis because most images have the top y-axis inversed with OpenGL's top y-axis.
	uv = vec2(in_uv.x, 1.0 - in_uv.y);

	normal =  normalize(normal_matrix * in_normal);

	fragment_position = vec3(model_matrix * vec4(in_position, 1.0f));
}