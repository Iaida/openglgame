#version 450 core
layout(location = 0) in vec3 in_position;
 
uniform mat4 projection_matrix, view_matrix, model_matrix;
uniform vec4 bounding_volume_color;

out vec4 color;
 
void main()
{
	color = bounding_volume_color;
	gl_Position = projection_matrix * view_matrix * model_matrix * vec4(in_position, 1.0);   
}