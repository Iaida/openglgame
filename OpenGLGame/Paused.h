#pragma once
#include "GameState.h"
class Paused :
	public GameState<Paused>
{
public:
	Paused();
	~Paused();

	void ProcessKeyboard() override;
	void ProcessController() override;
	void ProcessMouse() override;
};

