#pragma once

#include "IGameState.h"
#include "Paused.h"
#include "Ingame.h"
#include "TypeList.h"

#include <memory>
#include <type_traits>
#include <array>

class GameInfo
{
public:
	using GameStateList = TypeList<Paused, Ingame>;

	GameInfo();
	~GameInfo();

	IGameState * game_state() const;
	bool quitting() const;
	void set_quitting(const bool quitting);
	template <typename T>
	bool TestGameState();
	template <typename T>
	void set_game_state();
	
private:
	bool quitting_;
	IGameState * game_state_;
	std::array<std::unique_ptr<IGameState>, GameStateList::size()> game_states_;
};

template<typename T>
inline bool GameInfo::TestGameState()
{
	static_assert(std::is_base_of<IGameState, T>::value, "Type not derived from IGameState");
	return game_state_->GetGameStateIndex() == T::game_state_index_;
}

template<typename T>
inline void GameInfo::set_game_state()
{
	game_state_ = game_states_[T::game_state_index_].get();
}


