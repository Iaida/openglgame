#pragma once

#include <string>

class Options
{
public:

	Options(const std::string & file_path, const std::string & default_file_path);

	// called on quit
	void SaveChanges();

	// Mutators
	// [General]
	void set_title(const std::string & title);
	void set_gamepad(const bool gamepad);
	// [Video]
	void set_width(const int width);
	void set_height(const int height);
	void set_resolution(const int width, const int height);
	void set_fov_horizontal_deg(const float fov_horizontal_deg);
	void set_fov_vertical_deg(const float fov_vertical_deg);
	void set_near_distance(const float near_distance);
	void set_far_distance(const float far_distance);
	void set_vertical_sync(const bool vertical_sync);
	void set_fullscreen(const bool fullscreen);
	// [Audio]
	void set_master_volume(const float master_volume);
	void set_effects_volume(const float effects_volume);
	void set_voice_volume(const float voice_volume);
	void set_ambience_volume(const float ambience_volume);
	void set_music_volume(const float music_volume);

	// Accessors
	// [General]
	std::string title() const;
	bool gamepad() const;
	// [Video]
	int width() const;
	int height() const;
	float aspect_ratio() const;
	float fov_horizontal_deg() const;
	float fov_vertical_deg() const;
	float near_distance() const;
	float far_distance() const;
	bool vertical_sync() const;
	bool fullscreen() const;
	// [Audio]
	float master_volume() const;
	float effects_volume() const;
	float voice_volume() const;
	float ambience_volume() const;
	float music_volume() const;
	

private:
	std::string file_path_;
	std::string default_file_path_;

	// [General]
	std::string title_;
	bool gamepad_;
	// [Video]
	int width_;
	int height_;
	//float aspect_ratio_;
	float fov_vertical_deg_;
	float near_distance_;
	float far_distance_;
	bool vertical_sync_;
	bool fullscreen_;
	// [Audio]
	float master_volume_;
	float effects_volume_;
	float voice_volume_;
	float ambience_volume_;
	float music_volume_;

	void Load();
	float DegToRad(const float deg) const;
	float RadToDeg(const float rad) const;
};

