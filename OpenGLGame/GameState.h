#pragma once
#include "IGameState.h"

template<typename C>
class GameState : public IGameState
{
public:
	virtual ~GameState() {}
	size_t GetGameStateIndex() override;
	static size_t game_state_index_;	
};

template <typename T>
size_t GameState<T>::game_state_index_(0);

template<typename C>
inline size_t GameState<C>::GetGameStateIndex()
{
	return game_state_index_;
}
