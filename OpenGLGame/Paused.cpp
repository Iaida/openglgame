#include "Paused.h"
#include "Locator.h"
#include "Options.h"
#include "SDLInputEvents.h"
#include "LevelLoader.h"
#include "GameInfo.h"
#include "SDLOpenGLWindow.h"
#include "Camera.h"
#include <iostream>


Paused::Paused()
{
}

Paused::~Paused()
{
}

void Paused::ProcessKeyboard()
{
	
	if (Locator<SDLInputEvents>::service()->KeyPressed(SDL_SCANCODE_C))
	{
		Locator<Options>::service()->set_gamepad(!Locator<Options>::service()->gamepad());
		std::cout << "Controller toggled, now "<< Locator<Options>::service()->gamepad() << "\n";
	}

	if (Locator<SDLInputEvents>::service()->KeyPressed(SDL_SCANCODE_B))
		Locator<SDLOpenGLWindow>::service()->ToggleBackFaceCulling();

	if (Locator<SDLInputEvents>::service()->KeyPressed(SDL_SCANCODE_F))
	{
		Locator<Camera>::service()->floating_ = !Locator<Camera>::service()->floating_;
		std::cout << "Camera floating now " << Locator<Camera>::service()->floating_ << "\n";
	}
		
	
	if (Locator<SDLInputEvents>::service()->KeyPressed(SDL_SCANCODE_F1))
		Locator<Options>::service()->set_fullscreen(!Locator<Options>::service()->fullscreen());

	if (Locator<SDLInputEvents>::service()->KeyPressed(SDL_SCANCODE_ESCAPE))
	{
		Locator<GameInfo>::service()->set_game_state<Ingame>();
		if (SDL_SetRelativeMouseMode(SDL_TRUE) < 0)
			std::cout << "SDL_SetRelativeMouseMode failed \n";
	}

	// NUMBERS

	if (Locator<SDLInputEvents>::service()->KeyPressed(SDL_SCANCODE_1))
		Locator<LevelLoader>::service()->LoadLevel1();
	if (Locator<SDLInputEvents>::service()->KeyPressed(SDL_SCANCODE_2))
		Locator<LevelLoader>::service()->LoadLevel2();
	if (Locator<SDLInputEvents>::service()->KeyPressed(SDL_SCANCODE_3))
		Locator<LevelLoader>::service()->LoadLevel3();
	if (Locator<SDLInputEvents>::service()->KeyPressed(SDL_SCANCODE_4))
		Locator<LevelLoader>::service()->LoadLevel4();
	if (Locator<SDLInputEvents>::service()->KeyPressed(SDL_SCANCODE_5))
		Locator<LevelLoader>::service()->LoadLevel5();
	if (Locator<SDLInputEvents>::service()->KeyPressed(SDL_SCANCODE_6))
		Locator<LevelLoader>::service()->LoadLevel6();
	if (Locator<SDLInputEvents>::service()->KeyPressed(SDL_SCANCODE_7))
		Locator<LevelLoader>::service()->LoadLevel7();
}

void Paused::ProcessController()
{
	if (Locator<SDLInputEvents>::service()->ButtonPressed(SDL_CONTROLLER_BUTTON_START))
	{
		Locator<GameInfo>::service()->set_game_state<Ingame>();
		if (SDL_SetRelativeMouseMode(SDL_TRUE) < 0)
			std::cout << "SDL_SetRelativeMouseMode failed \n";
	}

	if (Locator<SDLInputEvents>::service()->ButtonPressed(SDL_CONTROLLER_BUTTON_BACK))
		Locator<GameInfo>::service()->set_quitting(true);

	if (Locator<SDLInputEvents>::service()->ButtonPressed(SDL_CONTROLLER_BUTTON_LEFTSHOULDER))
		Locator<Options>::service()->set_resolution(1280, 720);
	
	if (Locator<SDLInputEvents>::service()->ButtonPressed(SDL_CONTROLLER_BUTTON_RIGHTSHOULDER))
		Locator<Options>::service()->set_resolution(1920, 1080);
	
	if (Locator<SDLInputEvents>::service()->ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_UP))
		Locator<Options>::service()->set_vertical_sync(false);
		
	if (Locator<SDLInputEvents>::service()->ButtonPressed(SDL_CONTROLLER_BUTTON_DPAD_DOWN))
		Locator<Options>::service()->set_vertical_sync(true);
		
	
	{
		unsigned short left = Locator<SDLInputEvents>::service()->LeftTriggerValue() << 8;
		unsigned short right = Locator<SDLInputEvents>::service()->RightTriggerValue() << 8;

		if (Locator<SDLInputEvents>::service()->ButtonPressed(SDL_CONTROLLER_BUTTON_A))
			Locator<SDLInputEvents>::service()->SetVibration(left, right);

		if (Locator<SDLInputEvents>::service()->ButtonPressed(SDL_CONTROLLER_BUTTON_B))
			Locator<SDLInputEvents>::service()->SetVibration(0, 0);
	}	
}

void Paused::ProcessMouse()
{
}