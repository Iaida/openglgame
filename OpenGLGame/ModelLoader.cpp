#include "ModelLoader.h"

#include "Dependencies/glm/geometric.hpp" // glm::cross, glm::normalize


#include <iostream>
#include <fstream>
#include <string>
#include <sstream>


ModelLoader::ModelLoader(const std::string & file_path)
{
	file_path_ = file_path;
	number_faces_ = 0;
	number_vertices_ = 0;
	Load();
	CalcAABB();
	CalcMaxVertexDistance();
}

ModelLoader::~ModelLoader()
{
}

void ModelLoader::CalcAABB()
{
	float minX = FLT_MAX;
	float minY = FLT_MAX;
	float minZ = FLT_MAX;

	float maxX = FLT_MIN;
	float maxY = FLT_MIN;
	float maxZ = FLT_MIN;

	for (VertexFormat& v : vertex_buffer_)
	{
		if (v.position_.x > maxX)
			maxX = v.position_.x;
		if (v.position_.x < minX)
			minX = v.position_.x;

		if (v.position_.y > maxY)
			maxY = v.position_.y;
		if (v.position_.y < minY)
			minY = v.position_.y;

		if (v.position_.z > maxZ)
			maxZ = v.position_.z;
		if (v.position_.z < minZ)
			minZ = v.position_.z;
	}

	min_vertex_ = glm::vec3(minX, minY, minZ);
	max_vertex_ = glm::vec3(maxX, maxY, maxZ);
	glm::vec3 center = (max_vertex_ + min_vertex_) / 2.0f;
	

	auto EpsilonEqual = [](const glm::vec3 & a, const glm::vec3 & b, const float epsilon = FLT_EPSILON)
	{
		return glm::abs(a - b).x < epsilon && glm::abs(a - b).y < epsilon && glm::abs(a - b).z < epsilon;
	};
	if (!EpsilonEqual(center, glm::vec3(0.0f, 0.0f, 0.0f)))
		std::cout << "Model not centered at origin \n";

}

void ModelLoader::CalcMaxVertexDistance()
{
	float max_vertex_distance = FLT_MIN;
	
	for (VertexFormat& v : vertex_buffer_)
	{
		// center should be at origin
		float radius = glm::length(v.position_);
		if (radius > max_vertex_distance)
			max_vertex_distance = radius;
	}

	max_vertex_distance_ = max_vertex_distance;
}


int ModelLoader::Load()
{
	std::cout << "Parsing model: " << file_path_.c_str() << " - Size: " << FileSizeInByte() / 1'000 << " KB ... ";

	// check if the file_path contains .ply
	if (file_path_.find(".ply") != std::string::npos)
	{
		LoadPLY();
		//LoadPLYTest();
		std::cout << "finished \n";
		return 0;
	}
	// check if the file_path contains .obj
	else if (file_path_.find(".obj") != std::string::npos)
	{
		LoadOBJ();
		std::cout << "finished \n";
		return 0;
	}
	else
	{
		std::cout << "File named " << file_path_.c_str() << "does not have a .ply or .obj extension. \n";
		return -1;
	}
}


// slow
int ModelLoader::LoadPLYTest()
{
	std::ifstream file_stream(file_path_);
	if (!file_stream)
	{
		std::cout << file_path_ << " could not be opened \n";
		return -1;
	}
	std::string line;

	// -READ HEADER-

	// Find number of vertexes
	while (number_vertices_ == 0)
	{
		std::getline(file_stream, line);
		std::string condition = "element vertex ";
		if (line.find(condition) != std::string::npos)
			number_vertices_ = stoi(line.substr(condition.length()));
	}

	// Sets the position of the next character to be extracted from the input stream
	file_stream.seekg(0, file_stream.beg);

	// Find number of faces
	while (number_faces_ == 0)
	{
		std::getline(file_stream, line);
		std::string condition = "element face ";
		if (line.find(condition) != std::string::npos)
			number_faces_ = stoi(line.substr(condition.length()));
	}

	// go to end_header
	while (true)
	{
		std::getline(file_stream, line);
		std::string condition = "end_header";
		if (line.find(condition) != std::string::npos)
		{
			break;
		}
	}
		
	// -READ VERTICES-


	// resize float vector to hold vertices, 3 floats per vertex
	vertex_buffer_.resize(number_vertices_);
	//vertex_buffer_.reserve(number_vertices_);

	for (int v = 0; v < number_vertices_; v++)
	{
		std::getline(file_stream, line);
		std::istringstream line_stream(line);
		
		line_stream >> vertex_buffer_[v].position_.x >> vertex_buffer_[v].position_.y >> vertex_buffer_[v].position_.z;

		/*glm::vec3 pos;
		for (int d = 0; d < 3; d++) { line_stream >> pos[d]; }
		vertex_buffer_.push_back(VertexFormat(pos));*/
	}
	

	return 0;
}

int ModelLoader::LoadPLY()
{
	FILE* file;
	// open file
	if (fopen_s(&file, file_path_.c_str(), "r") != 0)
	{
		std::cout << "\n File named " << file_path_.c_str() << " can't be opened \n";
		return -1;
	}

	// buffer to hold lines read
	char buffer[300];

	// -READ HEADER-

	// read next line or up to 299 characters from file into buffer
	fgets(buffer, 300, file);

	// Find number of vertexes
	// search for "element vertex ...\n"
	while (strncmp("element vertex", buffer, strlen("element vertex")) != 0)
	{
		// read next line or up to 299 characters from file into buffer
		fgets(buffer, 300, file);
	}
	// copy string after "element vertex" to buffer
	strcpy_s(buffer, buffer + strlen("element vertex"));
	// save number of vertices to variable
	sscanf_s(buffer, "%i", &number_vertices_);


	// Find number of vertexes
	// set the position indicator associated with the stream to the beginning of file
	fseek(file, 0, SEEK_SET);
	// search for "element face ...\n"
	while (strncmp("element face", buffer, strlen("element face")) != 0)
	{
		// read next line or up to 299 characters from file into buffer
		fgets(buffer, 300, file);
	}
	// copy string after "element face" to buffer
	strcpy_s(buffer, buffer + strlen("element face"));
	// save number of faces to variable
	sscanf_s(buffer, "%i", &number_faces_);


	// go to end_header
	while (strncmp("end_header", buffer, strlen("end_header")) != 0)
	{
		fgets(buffer, 300, file);
	}

	// -READ VERTICES-


	// resize float vector to hold vertices, 3 floats per vertex
	vertex_buffer_.resize(number_vertices_);

	for (int v = 0; v < number_vertices_; v++)
	{
		// read next line or up to 299 characters from file into buffer
		fgets(buffer, 300, file);
		// for each vertex, 1 value for each of the 3 dimensions
		// Read data from buffer and store in vertex buffer accordingly
		sscanf_s(buffer, "%f %f %f",
			&vertex_buffer_[v].position_.x,
			&vertex_buffer_[v].position_.y,
			&vertex_buffer_[v].position_.z);
	}


	// -READ FACES-

	// resize unsigned int vector to hold 3 indices per face
	index_buffer_.resize(3 * number_faces_);

	for (int f = 0; f < number_faces_; f++)
	{
		// read next line or up to 299 characters from file into buffer
		fgets(buffer, 300, file);

		// indicates 3 indices for 1 triangle
		if (buffer[0] == '3')
		{
			// replace 3 at beginning of line with ' '
			buffer[0] = ' ';
			// Read data from buffer and store the vertex indices in index buffer accordingly
			sscanf_s(buffer, "%u%u%u",
				&index_buffer_[3 * f],
				&index_buffer_[3 * f + 1],
				&index_buffer_[3 * f + 2]);

		}
	}

	// -CALCULATE VERTEX NORMALS-
	// http://stackoverflow.com/questions/16340931/calculating-vertex-normals-of-a-mesh

	// iterate over indices/triangles
	for (int i = 0; i < index_buffer_.size(); i += 3)
	{
		glm::vec3 triangle_normal = CalculateTriangleNormal(vertex_buffer_[index_buffer_[i]].position_,
			vertex_buffer_[index_buffer_[i + 1]].position_,
			vertex_buffer_[index_buffer_[i + 2]].position_);

		// add result to normals of contained vertices
		vertex_buffer_[index_buffer_[i]].normal_ += triangle_normal;
		vertex_buffer_[index_buffer_[i + 1]].normal_ += triangle_normal;
		vertex_buffer_[index_buffer_[i + 2]].normal_ += triangle_normal;
	}

	// normalize accumulated normals 
	for (VertexFormat& v : vertex_buffer_) { v.normal_ = glm::normalize(v.normal_); }
	
	return 0;
}




int ModelLoader::LoadOBJ()
{
	std::ifstream file_stream(file_path_);
	if (!file_stream)
	{
		std::cout << file_path_ << " could not be opened \n";
		return -1;
	}
	std::string line;

	// save current line till end of file is reached
	while (std::getline(file_stream, line))
	{
		// vertex lines
		if (line[0] == 'v' && line[1] == ' ')
		{
			std::istringstream line_stream(line.substr(2));
			glm::vec3 pos;
			for (int d = 0; d < 3; d++)	{ line_stream >> pos[d]; }
			vertex_buffer_.push_back(VertexFormat(pos, glm::vec3()));
		}

		// face lines
		else if (line[0] == 'f' && line[1] == ' ')
		{
			std::istringstream line_stream(line.substr(2));
			for (unsigned number; line_stream >> number;) 
			{	
				// OBJ Indices start with 1 instead of 0
				index_buffer_.push_back(number - 1); 
			}
		}
	}

	number_vertices_ = static_cast<int>(vertex_buffer_.size());
	number_faces_ = static_cast<int>(index_buffer_.size() / 3);


	// -CALCULATE VERTEX NORMALS-
	// http://stackoverflow.com/questions/16340931/calculating-vertex-normals-of-a-mesh

	// iterate over indices/triangles
	for (int i = 0; i < index_buffer_.size(); i += 3)
	{
		glm::vec3 triangle_normal = CalculateTriangleNormal(vertex_buffer_[index_buffer_[i]].position_,
			vertex_buffer_[index_buffer_[i + 1]].position_,
			vertex_buffer_[index_buffer_[i + 2]].position_);

		// add result to normals of contained vertices
		vertex_buffer_[index_buffer_[i]].normal_ += triangle_normal;
		vertex_buffer_[index_buffer_[i + 1]].normal_ += triangle_normal;
		vertex_buffer_[index_buffer_[i + 2]].normal_ += triangle_normal;
	}

	// normalize accumulated normals 
	for (VertexFormat& v : vertex_buffer_) { v.normal_ = glm::normalize(v.normal_); }
	
	return 0;
}

glm::vec3 ModelLoader::CalculateTriangleNormal(const glm::vec3 & a, const glm::vec3 & b, const glm::vec3 & c)
{
	glm::vec3 result = glm::normalize(glm::cross(c - a, b - a));
	// errors occur when 2 out of 3 vectors are the same, check for nan-values
	if (result.x != result.x || result.y != result.y || result.z != result.z)
		return glm::vec3(0.0f, 0.0f, 0.0f);

	return result;
}

long ModelLoader::FileSizeInByte()
{
	std::ifstream file(file_path_, std::ifstream::ate | std::ifstream::binary);
	if (!file)
	{
		std::cout << file_path_ << " could not be opened \n";
		return -1;
	}
	return static_cast<long>(file.tellg());
}
