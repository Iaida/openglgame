#define _USE_MATH_DEFINES
#include <cmath>

#include "Locator.h"
#include "Camera.h"
#include "EntityComponentSystem.h"

#include "Dependencies\glm\gtc\matrix_transform.hpp"
#include "Dependencies\glm\geometric.hpp"

#include <iostream>

Camera::Camera() :
	position_(glm::vec3(0.0f, 3.0f, -10.0f)), velocity_(glm::vec3(0.0f, 0.0f, 0.0f)), 
	y_rot_(0.0f), xz_rot_(0.0f), y_rot_vel_(0.0f), xz_rot_vel_(0.0f), 
	up_(glm::vec3(0.0f, 1.0f, 0.0f)), floating_(true),
	entity_id_(0), zoom_(1.0f), zoom_overlay_id_(0), 
	picking_ray_min_distance_(FLT_MAX), picking_ray_min_distance_id_(0),
	vel_speed_(5.0f), rot_speed_(100.0f),
	direction_(CalculateDirection()), right_(CalculateRight()), view_matrix_(CalculateViewMatrix())
{
}

void Camera::Update(const float dt)
{
	position_ += vel_speed_ * velocity_ * dt;

	y_rot_ += rot_speed_ * y_rot_vel_ * dt;
	// y_rot_ wrapped between 0 and 360
	if (y_rot_ > 360.0f)
	{
		y_rot_ = y_rot_ - 360.0f;
	}
	else if (y_rot_ < 0.0f)
	{
		y_rot_ = y_rot_ + 360.0f;
	}
	
	xz_rot_ += rot_speed_ * xz_rot_vel_ * dt;
	// xz_rot_ clamped between -89 and +89
	if (xz_rot_ > 89.0f)
	{
		xz_rot_ = 89.0f;
	}
	else if (xz_rot_ < -89.0f)
	{
		xz_rot_ = -89.0f;
	}

	direction_ = CalculateDirection();
	right_ = CalculateRight();
	view_matrix_ = CalculateViewMatrix();

	if (!floating_ && entity_id_ != 0)
		Locator<EntityComponentSystem>::service()->UpdateCameraEntity(entity_id_);
}

void Camera::Reset()
{
	*this = Camera();
}

Ray Camera::GetPickingRay()
{
	return Ray(position_, direction_);
}



glm::quat Camera::GetRotationQuaternion()
{	
	glm::quat y_quat = glm::angleAxis(glm::radians(y_rot_), up_);
	glm::quat xz_quat = glm::angleAxis(glm::radians(-xz_rot_), right_);
	return xz_quat * y_quat;
}

glm::vec3 Camera::CalculateDirection()
{
	float x_res = sin(glm::radians(y_rot_));
	float y_res = tan(glm::radians(xz_rot_));
	float z_res = cos(glm::radians(y_rot_));

	return glm::normalize(glm::vec3(x_res, y_res, z_res));
}

glm::vec3 Camera::CalculateRight()
{
	return glm::normalize(glm::cross(up_, direction_));
}

glm::mat4 Camera::CalculateViewMatrix()
{
	return glm::lookAt(position_, position_ + direction_, up_);
}


