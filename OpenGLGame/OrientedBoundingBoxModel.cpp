#include "OrientedBoundingBoxModel.h"

#include "Dependencies\glew\glew.h"


OrientedBoundingBoxModel::OrientedBoundingBoxModel(Program * program)
{
	struct VertexFormat
	{
		VertexFormat(const glm::vec3 &position) : position_(position) {}
		VertexFormat() {}
		~VertexFormat() {}
		glm::vec3 position_;
	};


	std::vector<unsigned int> index_buffer; // size = 3 * number_faces
	std::vector<VertexFormat> vertex_buffer; // size = number_vertices


	// min(-1,-1,-1) max(1,1,1) cube

	// http://in2gpu.com/wp-content/uploads/2015/07/c2_2_cube_index.jpg
	// corrected ccw for: right, back, upper faces
	index_buffer = {
		0,  1,  2,  0,  2,  3,   //front
		4,  6,  5,  4,  7,  6,   //right
		8,  10, 9,  8,  11, 10,  //back
		12, 13, 14, 12, 14, 15,  //left
		16, 18, 17, 16, 19, 18,  //upper
		20, 21, 22, 20, 22, 23 }; //bottom

	// front
	vertex_buffer.push_back(glm::vec3(-1.0, -1.0, 1.0));
	vertex_buffer.push_back(glm::vec3(1.0, -1.0, 1.0));
	vertex_buffer.push_back(glm::vec3(1.0, 1.0, 1.0));
	vertex_buffer.push_back(glm::vec3(-1.0, 1.0, 1.0));

	// right
	vertex_buffer.push_back(glm::vec3(1.0, 1.0, 1.0));
	vertex_buffer.push_back(glm::vec3(1.0, 1.0, -1.0));
	vertex_buffer.push_back(glm::vec3(1.0, -1.0, -1.0));
	vertex_buffer.push_back(glm::vec3(1.0, -1.0, 1.0));

	// back
	vertex_buffer.push_back(glm::vec3(-1.0, -1.0, -1.0));
	vertex_buffer.push_back(glm::vec3(1.0, -1.0, -1.0));
	vertex_buffer.push_back(glm::vec3(1.0, 1.0, -1.0));
	vertex_buffer.push_back(glm::vec3(-1.0, 1.0, -1.0));

	// left
	vertex_buffer.push_back(glm::vec3(-1.0, -1.0, -1.0));
	vertex_buffer.push_back(glm::vec3(-1.0, -1.0, 1.0));
	vertex_buffer.push_back(glm::vec3(-1.0, 1.0, 1.0));
	vertex_buffer.push_back(glm::vec3(-1.0, 1.0, -1.0));

	// upper
	vertex_buffer.push_back(glm::vec3(1.0, 1.0, 1.0));
	vertex_buffer.push_back(glm::vec3(-1.0, 1.0, 1.0));
	vertex_buffer.push_back(glm::vec3(-1.0, 1.0, -1.0));
	vertex_buffer.push_back(glm::vec3(1.0, 1.0, -1.0));

	// bottom
	vertex_buffer.push_back(glm::vec3(-1.0, -1.0, -1.0));
	vertex_buffer.push_back(glm::vec3(1.0, -1.0, -1.0));
	vertex_buffer.push_back(glm::vec3(1.0, -1.0, 1.0));
	vertex_buffer.push_back(glm::vec3(-1.0, -1.0, 1.0));

	number_faces_ = static_cast<int>(index_buffer.size()) / 3;
	number_vertices_ = static_cast<int>(vertex_buffer.size());

	GLuint vao_temp;
	GLuint vbo_temp;
	GLuint ibo_temp;

	glGenVertexArrays(1, &vao_temp);
	glBindVertexArray(vao_temp);

	glGenBuffers(1, &ibo_temp);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_temp);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, index_buffer.size() * sizeof(unsigned int), &index_buffer[0], GL_STATIC_DRAW);

	glGenBuffers(1, &vbo_temp);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_temp);
	glBufferData(GL_ARRAY_BUFFER, vertex_buffer.size() * sizeof(VertexFormat), &vertex_buffer[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(offsetof(VertexFormat, VertexFormat::position_)));

	glBindVertexArray(0);

	vao_ = vao_temp;
	vbos_.push_back(vbo_temp);
	vbos_.push_back(ibo_temp);

	program_ = program;

}


OrientedBoundingBoxModel::~OrientedBoundingBoxModel()
{
}

void OrientedBoundingBoxModel::Draw(const glm::mat4 & projection_matrix, const glm::mat4 & view_matrix, const glm::mat4 & model_matrix)
{
	

	glUseProgram(*program_);

	glUniformMatrix4fv(glGetUniformLocation(*program_, "model_matrix"), 1, false, &model_matrix[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(*program_, "view_matrix"), 1, false, &view_matrix[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(*program_, "projection_matrix"), 1, false, &projection_matrix[0][0]);

	glUniform4f(glGetUniformLocation(*program_, "bounding_volume_color"), 0.0f, 0.0f, 1.0f, 1.0f);

	glBindVertexArray(vao_);
	glDrawElements(GL_TRIANGLES, number_faces_ * 3, GL_UNSIGNED_INT, 0);

}


