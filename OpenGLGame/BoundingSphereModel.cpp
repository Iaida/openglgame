#include "BoundingSphereModel.h"

#include "Dependencies\glew\glew.h"
#include "Dependencies\glm\geometric.hpp"

#include <iostream>



BoundingSphereModel::BoundingSphereModel(Program* program)
{
	struct VertexFormat
	{
		VertexFormat(const glm::vec3 &position) : position_(position) {}
		VertexFormat() {}
		~VertexFormat() {}
		glm::vec3 position_;
	};

	std::vector<unsigned int> index_buffer; // size = 3 * number_faces
	std::vector<glm::vec3> vertex_pos_buffer;
	std::vector<VertexFormat> vertex_buffer; // size = number_vertices

	// min(-1,-1,-1) max(1,1,1) cube

	// http://in2gpu.com/wp-content/uploads/2015/07/c2_2_cube_index.jpg
	// corrected ccw for: right, back, upper faces
	index_buffer = {
		0,  1,  2,  0,  2,  3,   //front
		4,  6,  5,  4,  7,  6,   //right
		8,  10, 9,  8,  11, 10,  //back
		12, 13, 14, 12, 14, 15,  //left
		16, 18, 17, 16, 19, 18,  //upper
		20, 21, 22, 20, 22, 23 }; //bottom

	// front
	vertex_pos_buffer.push_back(glm::vec3(-1.0, -1.0, 1.0));
	vertex_pos_buffer.push_back(glm::vec3(1.0, -1.0, 1.0));
	vertex_pos_buffer.push_back(glm::vec3(1.0, 1.0, 1.0));
	vertex_pos_buffer.push_back(glm::vec3(-1.0, 1.0, 1.0));

	// right
	vertex_pos_buffer.push_back(glm::vec3(1.0, 1.0, 1.0));
	vertex_pos_buffer.push_back(glm::vec3(1.0, 1.0, -1.0));
	vertex_pos_buffer.push_back(glm::vec3(1.0, -1.0, -1.0));
	vertex_pos_buffer.push_back(glm::vec3(1.0, -1.0, 1.0));

	// back
	vertex_pos_buffer.push_back(glm::vec3(-1.0, -1.0, -1.0));
	vertex_pos_buffer.push_back(glm::vec3(1.0, -1.0, -1.0));
	vertex_pos_buffer.push_back(glm::vec3(1.0, 1.0, -1.0));
	vertex_pos_buffer.push_back(glm::vec3(-1.0, 1.0, -1.0));

	// left
	vertex_pos_buffer.push_back(glm::vec3(-1.0, -1.0, -1.0));
	vertex_pos_buffer.push_back(glm::vec3(-1.0, -1.0, 1.0));
	vertex_pos_buffer.push_back(glm::vec3(-1.0, 1.0, 1.0));
	vertex_pos_buffer.push_back(glm::vec3(-1.0, 1.0, -1.0));

	// upper
	vertex_pos_buffer.push_back(glm::vec3(1.0, 1.0, 1.0));
	vertex_pos_buffer.push_back(glm::vec3(-1.0, 1.0, 1.0));
	vertex_pos_buffer.push_back(glm::vec3(-1.0, 1.0, -1.0));
	vertex_pos_buffer.push_back(glm::vec3(1.0, 1.0, -1.0));

	// bottom
	vertex_pos_buffer.push_back(glm::vec3(-1.0, -1.0, -1.0));
	vertex_pos_buffer.push_back(glm::vec3(1.0, -1.0, -1.0));
	vertex_pos_buffer.push_back(glm::vec3(1.0, -1.0, 1.0));
	vertex_pos_buffer.push_back(glm::vec3(-1.0, -1.0, 1.0));

	// subdivide cube
	// replace 1 triangle with 4^number_subdivisions triangles
	unsigned number_subdivisions = 3;
	std::cout << "subdividing " << number_subdivisions << " times...";
	for (unsigned sd = 0; sd < number_subdivisions; sd++)
	{
		Subdivide(index_buffer, vertex_pos_buffer);
	}
	std::cout << "finished \n";

	// vertex_pos_buffer -> vertex_buffer
	vertex_buffer.resize(vertex_pos_buffer.size());
	for (size_t v = 0; v < vertex_buffer.size(); v++)
	{
		// transform min(-1,-1,-1) max(1,1,1) cube to unit sphere
		vertex_buffer[v].position_ = UnitCubeToUnitSphere(vertex_pos_buffer[v]);
	}

	number_faces_ = static_cast<int>(index_buffer.size()) / 3;
	number_vertices_ = static_cast<int>(vertex_buffer.size());
	std::cout << number_faces_ << " faces, " << number_vertices_ << " vertices \n";



	GLuint vao_temp;
	GLuint vbo_temp;
	GLuint ibo_temp;

	glGenVertexArrays(1, &vao_temp);
	glBindVertexArray(vao_temp);

	glGenBuffers(1, &ibo_temp);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo_temp);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, index_buffer.size() * sizeof(unsigned int), &index_buffer[0], GL_STATIC_DRAW);

	glGenBuffers(1, &vbo_temp);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_temp);
	glBufferData(GL_ARRAY_BUFFER, vertex_buffer.size() * sizeof(VertexFormat), &vertex_buffer[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(offsetof(VertexFormat, VertexFormat::position_)));

	glBindVertexArray(0);

	vao_ = vao_temp;
	vbos_.push_back(vbo_temp);
	vbos_.push_back(ibo_temp);
	
	program_ = program;
}

BoundingSphereModel::~BoundingSphereModel()
{
}

void BoundingSphereModel::Draw(const glm::mat4 & projection_matrix, const glm::mat4 & view_matrix, const glm::mat4 & model_matrix)
{
	glUseProgram(*program_);

	glUniformMatrix4fv(glGetUniformLocation(*program_, "model_matrix"), 1, false, &model_matrix[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(*program_, "view_matrix"), 1, false, &view_matrix[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(*program_, "projection_matrix"), 1, false, &projection_matrix[0][0]);

	glUniform4f(glGetUniformLocation(*program_, "bounding_volume_color"), 1.0f, 0.0f, 0.0f, 1.0f);

	glBindVertexArray(vao_);
	glDrawElements(GL_TRIANGLES, number_faces_ * 3, GL_UNSIGNED_INT, 0);
}

glm::vec3 BoundingSphereModel::UnitCubeToUnitSphere(const glm::vec3 & vector)
{
	float xx = vector.x * vector.x;
	float yy = vector.y * vector.y;
	float zz = vector.z * vector.z;

	return glm::vec3(
		vector.x * sqrt(1.0 - (yy / 2.0) - (zz / 2.0) + ((yy * zz) / 3.0)),
		vector.y * sqrt(1.0 - (zz / 2.0) - (xx / 2.0) + ((xx * zz) / 3.0)),
		vector.z * sqrt(1.0 - (xx / 2.0) - (yy / 2.0) + ((xx * yy) / 3.0)));
}

void BoundingSphereModel::Subdivide(std::vector<unsigned int>& index_buffer, std::vector<glm::vec3>& vertex_pos_buffer)
{
	// each triangle subdivided into 4
	std::vector<unsigned int> new_index_buffer(index_buffer.size() * 4);

	// iterate over triangles
	for (size_t i = 0; i < index_buffer.size(); i += 3)
	{
		unsigned a_index = index_buffer[i];
		unsigned b_index = index_buffer[i + 1];
		unsigned c_index = index_buffer[i + 2];

		glm::vec3 a_pos = vertex_pos_buffer[a_index];
		glm::vec3 b_pos = vertex_pos_buffer[b_index];
		glm::vec3 c_pos = vertex_pos_buffer[c_index];

		// calculate new vertices
		glm::vec3 ab_pos = (a_pos + b_pos) * 0.5f;
		glm::vec3 ac_pos = (a_pos + c_pos) * 0.5f;
		glm::vec3 bc_pos = (b_pos + c_pos) * 0.5f;

		// add new vertices
		unsigned ab_index = AddVertex(ab_pos, vertex_pos_buffer);
		unsigned ac_index = AddVertex(ac_pos, vertex_pos_buffer);
		unsigned bc_index = AddVertex(bc_pos, vertex_pos_buffer);

		// add 4 new triangles for each old triangle
		// triangle indices are in ccw order

		//  A
		//  |\
		// AB-AC
		//  |\ |\
		//  B-BC-C

		size_t triangle_counter = i / 3;

		new_index_buffer[triangle_counter * 12 + 0] = a_index;
		new_index_buffer[triangle_counter * 12 + 1] = ab_index;
		new_index_buffer[triangle_counter * 12 + 2] = ac_index;

		new_index_buffer[triangle_counter * 12 + 3] = ab_index;
		new_index_buffer[triangle_counter * 12 + 4] = b_index;
		new_index_buffer[triangle_counter * 12 + 5] = bc_index;

		new_index_buffer[triangle_counter * 12 + 6] = ab_index;
		new_index_buffer[triangle_counter * 12 + 7] = bc_index;
		new_index_buffer[triangle_counter * 12 + 8] = ac_index;

		new_index_buffer[triangle_counter * 12 + 9] = ac_index;
		new_index_buffer[triangle_counter * 12 + 10] = bc_index;
		new_index_buffer[triangle_counter * 12 + 11] = c_index;
	}

	// replace old indices with new indices, thereby deleting old triangles
	index_buffer = new_index_buffer;
}

unsigned BoundingSphereModel::AddVertex(const glm::vec3 & vector, std::vector<glm::vec3>& vertex_pos_buffer)
{
	// check if vertex position already exists in vertex_buffer
	for (int v = 0; v < vertex_pos_buffer.size(); v++)
	{
		if (vertex_pos_buffer[v] == vector)
			return v;
	}
	// vertex does not exist in vertex_buffer
	vertex_pos_buffer.push_back(vector);
	return static_cast<unsigned>(vertex_pos_buffer.size() - 1);
}
