#pragma once
#include "Model.h"


class ModelFromFile :
	public Model
{
public:
	ModelFromFile(const std::string & file_path, Program * program);
	~ModelFromFile();

	int number_vertices_;
	int number_faces_;
	
	virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::mat4& model_matrix)	override final;
};

