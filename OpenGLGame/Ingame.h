#pragma once
#include "GameState.h"
class Ingame :
	public GameState<Ingame>
{
public:
	Ingame();
	~Ingame();

	void ProcessKeyboard() override;
	void ProcessController() override;
	void ProcessMouse() override;

};

