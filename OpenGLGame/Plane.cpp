#include "Plane.h"
#include "Dependencies\glm\geometric.hpp"
#include "IBoundingVolume.h"

Plane::Plane()
{
}

Plane::Plane(const glm::vec3 & normal, const float distance) :
	normal_(normal), distance_(distance)
{
}

Plane::Plane(const glm::vec4 & normal_distance) :
	normal_(glm::vec3(normal_distance)), distance_(normal_distance.w)
{
}


Plane::Plane(const glm::vec3 & a, const glm::vec3 & b, const glm::vec3 & c) :
	normal_(glm::normalize(glm::cross(b - a, c - a))), distance_(glm::dot(normal_, a))
{
}

Plane::~Plane()
{
}

float Plane::SignedPointDistance(const glm::vec3 & point) const
{
	// normal_ is normalized, simplifies equation
	return glm::dot(point, normal_) - distance_; // minus d in book, plus d in other sources
}
