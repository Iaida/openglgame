#pragma once
#include "Dependencies\glm\vec3.hpp"
#include <vector>


// Mesh Files should be centered at origin, else bugs will occur

class ModelLoader
{
public:

	struct VertexFormat
	{
		glm::vec3 position_;
		glm::vec3 normal_;


		VertexFormat(const glm::vec3 &position, const glm::vec3 &normal)
		{
			position_ = position;
			normal_ = normal;
		}
		VertexFormat() {}
	};

	std::vector<unsigned int> index_buffer_; // size = 3 * number_faces
	std::vector<VertexFormat> vertex_buffer_; // size = number_vertices

	int number_vertices_;
	int number_faces_;
	std::string file_path_;

	glm::vec3 min_vertex_;
	glm::vec3 max_vertex_;
	float max_vertex_distance_;

	ModelLoader(const std::string & file_path);
	~ModelLoader();

	void CalcAABB();
	void CalcMaxVertexDistance();
private:

	int Load();
	int LoadPLYTest();
	int LoadPLY();
	int LoadOBJ();
	glm::vec3 CalculateTriangleNormal(const glm::vec3 & a, const glm::vec3 & b, const glm::vec3 & c);
	long FileSizeInByte();

	
};

