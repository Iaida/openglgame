#pragma once

#include "Ray.h"

#include "Dependencies\glm\vec3.hpp"
#include "Dependencies\glm\mat4x4.hpp"
#include "Dependencies\glm\gtc\quaternion.hpp"
#include "Dependencies\glm\gtx\quaternion.hpp"


class Camera
{
public:
	Camera();

	glm::vec3 position_;
	glm::vec3 velocity_;
	float y_rot_; // in degrees, [0,360], azimuth
	float xz_rot_; // in degrees, [-89, +89], zenith
	float y_rot_vel_;
	float xz_rot_vel_;
	glm::vec3 direction_;
	glm::vec3 up_;
	glm::vec3 right_;
	glm::mat4 view_matrix_;
	
	bool floating_;

	size_t entity_id_;
	
	float zoom_;
	size_t zoom_overlay_id_;

	float picking_ray_min_distance_;
	size_t picking_ray_min_distance_id_;


	void Update(const float dt);
	void Reset();
	Ray GetPickingRay();
	glm::quat GetRotationQuaternion();

	
private:
	float vel_speed_;
	float rot_speed_;
	glm::vec3 CalculateDirection();
	glm::vec3 CalculateRight();
	glm::mat4 CalculateViewMatrix();
};

