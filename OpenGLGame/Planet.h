#pragma once
#include "Model.h"
#include "SimplexNoise.h"
#include "Texture.h"
class Planet :
	public Model
{
public:
	Planet(Program * program);
	~Planet();

	virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::mat4& model_matrix)	override final;


private:
	int number_vertices_;
	int number_faces_;
	Texture * gradient_;
	SimplexNoise simplex_noise_;

	glm::vec3 UnitCubeToUnitSphere(const glm::vec3 & vector);
	// each triangle is split into 4
	void Subdivide(std::vector<unsigned int> & index_buffer, std::vector<glm::vec3> & vertex_pos_buffer);
	// returns index
	unsigned AddVertex(const glm::vec3 & vector, std::vector<glm::vec3> & vertex_pos_buffer);
	
};

