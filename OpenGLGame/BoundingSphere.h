#pragma once
#include "IBoundingVolume.h"
#include "Component.h"

#include "Dependencies\glm\vec3.hpp"

class Model;

class BoundingSphere : 
	public IBoundingVolume, public Component<BoundingSphere>
{
public:
	BoundingSphere();
	BoundingSphere(Model * model_ptr, Model * target_model_ptr);
	~BoundingSphere();

	virtual bool Collision(const IBoundingVolume & bounding_volume) const override;
	virtual bool CollisionBoundingSphere(const BoundingSphere & bounding_sphere) const override;
	virtual bool CollisionOrientedBoundingBox(const OrientedBoundingBox & oriented_bounding_box) const override;
	virtual void Draw(const glm::mat4 & projection_matrix, const glm::mat4 & view_matrix) override;
	virtual std::pair<bool, float> RayIntersection(const Ray & ray) const override;
	virtual bool NegativeHalfSpace(const Plane & plane) const override;

	glm::vec3 position_;
	float initial_radius_;
	float radius_;
	

private:
	Model * model_ptr_;
};

