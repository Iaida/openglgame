#pragma once

template <typename T>
class Locator
{
public:
	static T* service();
	static void provide(T & service);
	
private:
	static T* service_;
};

template <typename T>
T* Locator<T>::service_ = nullptr;

template<typename T>
inline T * Locator<T>::service()
{
	return service_;
}

template<typename T>
inline void Locator<T>::provide(T & service)
{
	service_ = &service;
}
