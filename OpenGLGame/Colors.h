#pragma once

#include "Dependencies\glm\vec3.hpp"


class Colors
{
public:
	static glm::vec3 lerp(float t, const glm::vec3 &col1, const glm::vec3 &col2);

	static glm::vec3 white();
	static glm::vec3 black();
	static glm::vec3 grey();
	static glm::vec3 red();
	static glm::vec3 green();
	static glm::vec3 blue();
	static glm::vec3 navy();
	static glm::vec3 steelBlue();
	static glm::vec3 darkGreen();
	static glm::vec3 forestGreen();
	static glm::vec3 wheat();
	static glm::vec3 cyan();
	static glm::vec3 yellow();
	static glm::vec3 brown();
};

