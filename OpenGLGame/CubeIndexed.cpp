#include "CubeIndexed.h"

#include "Dependencies\glew\glew.h"
#include "Dependencies\glm\vec3.hpp"
#include "Dependencies\glm\vec4.hpp"
#include <cmath>





CubeIndexed::CubeIndexed(Program * program)
{
	struct VertexFormat
	{
		glm::vec3 position_;
		glm::vec4 color_;

		VertexFormat(const glm::vec3 &position, const glm::vec4 &color)
		{
			position_ = position;
			color_ = color;
		}
		VertexFormat() {}
	};


	GLuint vao;
	GLuint vbo;
	GLuint ibo;
	GLuint model_matrices_temp;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// http://in2gpu.com/wp-content/uploads/2015/07/c2_2_cube_index.jpg
	// corrected ccw for: right, back, upper faces
	std::vector<unsigned int> indices = {
		0,  1,  2,  0,  2,  3,   //front
		4,  6,  5,  4,  7,  6,   //right
		8,  10, 9,  8,  11, 10,  //back
		12, 13, 14, 12, 14, 15,  //left
		16, 18, 17, 16, 19, 18,  //upper
		20, 21, 22, 20, 22, 23 }; //bottom

								  // still contains duplicates ???
	std::vector<VertexFormat> vertices;

	//front
	vertices.push_back(VertexFormat(glm::vec3(-1.0, -1.0, 1.0),
		glm::vec4(0, 0, 1, 1)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, -1.0, 1.0),
		glm::vec4(1, 0, 1, 1)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, 1.0, 1.0),
		glm::vec4(1, 1, 1, 1)));
	vertices.push_back(VertexFormat(glm::vec3(-1.0, 1.0, 1.0),
		glm::vec4(0, 1, 1, 1)));

	//right
	vertices.push_back(VertexFormat(glm::vec3(1.0, 1.0, 1.0),
		glm::vec4(1, 1, 1, 1)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, 1.0, -1.0),
		glm::vec4(1, 1, 0, 1)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, -1.0, -1.0),
		glm::vec4(1, 0, 0, 1)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, -1.0, 1.0),
		glm::vec4(1, 0, 1, 1)));

	//back
	vertices.push_back(VertexFormat(glm::vec3(-1.0, -1.0, -1.0),
		glm::vec4(0, 0, 0, 1)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, -1.0, -1.0),
		glm::vec4(1, 0, 0, 1)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, 1.0, -1.0),
		glm::vec4(1, 1, 0, 1)));
	vertices.push_back(VertexFormat(glm::vec3(-1.0, 1.0, -1.0),
		glm::vec4(0, 1, 0, 1)));

	//left
	vertices.push_back(VertexFormat(glm::vec3(-1.0, -1.0, -1.0),
		glm::vec4(0, 0, 0, 1)));
	vertices.push_back(VertexFormat(glm::vec3(-1.0, -1.0, 1.0),
		glm::vec4(0, 0, 1, 1)));
	vertices.push_back(VertexFormat(glm::vec3(-1.0, 1.0, 1.0),
		glm::vec4(0, 1, 1, 1)));
	vertices.push_back(VertexFormat(glm::vec3(-1.0, 1.0, -1.0),
		glm::vec4(0, 1, 0, 1)));

	//upper
	vertices.push_back(VertexFormat(glm::vec3(1.0, 1.0, 1.0),
		glm::vec4(1, 1, 1, 1)));
	vertices.push_back(VertexFormat(glm::vec3(-1.0, 1.0, 1.0),
		glm::vec4(0, 1, 1, 1)));
	vertices.push_back(VertexFormat(glm::vec3(-1.0, 1.0, -1.0),
		glm::vec4(0, 1, 0, 1)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, 1.0, -1.0),
		glm::vec4(1, 1, 0, 1)));

	//bottom
	vertices.push_back(VertexFormat(glm::vec3(-1.0, -1.0, -1.0),
		glm::vec4(0, 0, 0, 1)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, -1.0, -1.0),
		glm::vec4(1, 0, 0, 1)));
	vertices.push_back(VertexFormat(glm::vec3(1.0, -1.0, 1.0),
		glm::vec4(1, 0, 1, 1)));
	vertices.push_back(VertexFormat(glm::vec3(-1.0, -1.0, 1.0),
		glm::vec4(0, 0, 1, 1)));

	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(VertexFormat), &vertices[0], GL_STATIC_DRAW);

	glGenBuffers(1, &ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(offsetof(VertexFormat, VertexFormat::position_)));

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(offsetof(VertexFormat, VertexFormat::color_)));

	// instanced rendering
	glGenBuffers(1, &model_matrices_temp);
	glBindBuffer(GL_ARRAY_BUFFER, model_matrices_temp);
	GLuint mat_loc = 2;
	// mat4 -> 4 times vec3
	for (unsigned int i = 0; i < 4; i++)
	{
		glEnableVertexAttribArray(mat_loc + i);
		glVertexAttribPointer(mat_loc + i, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4),
			(const GLvoid*)(sizeof(GLfloat) * i * 4));
		glVertexAttribDivisor(mat_loc + i, 1);
	}


	glBindVertexArray(0);
	vao_ = vao;
	vbos_.push_back(vbo);
	vbos_.push_back(ibo);


	model_matrices_object_ = model_matrices_temp;

	unsigned number_instances_ca = 65000;
	unsigned num_steps_per_dim = static_cast<unsigned>(cbrt(number_instances_ca));
	number_instances_ = num_steps_per_dim * num_steps_per_dim * num_steps_per_dim;
	step_distance_ = 200.0f / num_steps_per_dim;

	model_matrices_.resize(number_instances_);

	program_ = program;
	
	min_ = glm::vec3(-1.0, -1.0, -1.0);
	max_ = glm::vec3(1.0, 1.0, 1.0);
}

CubeIndexed::~CubeIndexed()
{
}

//
//void CubeIndexed::Create()
//{
//	GLuint vao;
//	GLuint vbo;
//	GLuint ibo;
//
//	glGenVertexArrays(1, &vao);
//	glBindVertexArray(vao);
//
//	// http://in2gpu.com/wp-content/uploads/2015/07/c2_2_cube_index.jpg
//	// corrected ccw for: right, back, upper faces
//	std::vector<unsigned int> indices = {
//		0,  1,  2,  0,  2,  3,   //front
//		4,  6,  5,  4,  7,  6,   //right
//		8,  10, 9,  8,  11, 10,  //back
//		12, 13, 14, 12, 14, 15,  //left
//		16, 18, 17, 16, 19, 18,  //upper
//		20, 21, 22, 20, 22, 23 }; //bottom
//
//								  // still contains duplicates ???
//	std::vector<VertexFormat> vertices;
//
//	//front
//	vertices.push_back(VertexFormat(glm::vec3(-1.0, -1.0, 1.0),
//		glm::vec4(0, 0, 1, 1)));
//	vertices.push_back(VertexFormat(glm::vec3(1.0, -1.0, 1.0),
//		glm::vec4(1, 0, 1, 1)));
//	vertices.push_back(VertexFormat(glm::vec3(1.0, 1.0, 1.0),
//		glm::vec4(1, 1, 1, 1)));
//	vertices.push_back(VertexFormat(glm::vec3(-1.0, 1.0, 1.0),
//		glm::vec4(0, 1, 1, 1)));
//
//	//right
//	vertices.push_back(VertexFormat(glm::vec3(1.0, 1.0, 1.0),
//		glm::vec4(1, 1, 1, 1)));
//	vertices.push_back(VertexFormat(glm::vec3(1.0, 1.0, -1.0),
//		glm::vec4(1, 1, 0, 1)));
//	vertices.push_back(VertexFormat(glm::vec3(1.0, -1.0, -1.0),
//		glm::vec4(1, 0, 0, 1)));
//	vertices.push_back(VertexFormat(glm::vec3(1.0, -1.0, 1.0),
//		glm::vec4(1, 0, 1, 1)));
//
//	//back
//	vertices.push_back(VertexFormat(glm::vec3(-1.0, -1.0, -1.0),
//		glm::vec4(0, 0, 0, 1)));
//	vertices.push_back(VertexFormat(glm::vec3(1.0, -1.0, -1.0),
//		glm::vec4(1, 0, 0, 1)));
//	vertices.push_back(VertexFormat(glm::vec3(1.0, 1.0, -1.0),
//		glm::vec4(1, 1, 0, 1)));
//	vertices.push_back(VertexFormat(glm::vec3(-1.0, 1.0, -1.0),
//		glm::vec4(0, 1, 0, 1)));
//
//	//left
//	vertices.push_back(VertexFormat(glm::vec3(-1.0, -1.0, -1.0),
//		glm::vec4(0, 0, 0, 1)));
//	vertices.push_back(VertexFormat(glm::vec3(-1.0, -1.0, 1.0),
//		glm::vec4(0, 0, 1, 1)));
//	vertices.push_back(VertexFormat(glm::vec3(-1.0, 1.0, 1.0),
//		glm::vec4(0, 1, 1, 1)));
//	vertices.push_back(VertexFormat(glm::vec3(-1.0, 1.0, -1.0),
//		glm::vec4(0, 1, 0, 1)));
//
//	//upper
//	vertices.push_back(VertexFormat(glm::vec3(1.0, 1.0, 1.0),
//		glm::vec4(1, 1, 1, 1)));
//	vertices.push_back(VertexFormat(glm::vec3(-1.0, 1.0, 1.0),
//		glm::vec4(0, 1, 1, 1)));
//	vertices.push_back(VertexFormat(glm::vec3(-1.0, 1.0, -1.0),
//		glm::vec4(0, 1, 0, 1)));
//	vertices.push_back(VertexFormat(glm::vec3(1.0, 1.0, -1.0),
//		glm::vec4(1, 1, 0, 1)));
//
//	//bottom
//	vertices.push_back(VertexFormat(glm::vec3(-1.0, -1.0, -1.0),
//		glm::vec4(0, 0, 0, 1)));
//	vertices.push_back(VertexFormat(glm::vec3(1.0, -1.0, -1.0),
//		glm::vec4(1, 0, 0, 1)));
//	vertices.push_back(VertexFormat(glm::vec3(1.0, -1.0, 1.0),
//		glm::vec4(1, 0, 1, 1)));
//	vertices.push_back(VertexFormat(glm::vec3(-1.0, -1.0, 1.0),
//		glm::vec4(0, 0, 1, 1)));
//
//	glGenBuffers(1, &vbo);
//	glBindBuffer(GL_ARRAY_BUFFER, vbo);
//	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(VertexFormat), &vertices[0], GL_STATIC_DRAW);
//
//	glGenBuffers(1, &ibo);
//	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
//	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
//
//	glEnableVertexAttribArray(0);
//	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(offsetof(VertexFormat, VertexFormat::position)));
//
//	glEnableVertexAttribArray(1);
//	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(VertexFormat), (void*)(offsetof(VertexFormat, VertexFormat::color)));
//
//	glBindVertexArray(0);
//	vao_ = vao;
//	vbos_.push_back(vbo);
//	vbos_.push_back(ibo);
//
//}
//
//
//void CubeIndexed::Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::mat4& model_matrix)
//{
//	glUseProgram(program_);
//	glBindVertexArray(vao_);
//
//	glUniformMatrix4fv(glGetUniformLocation(program_, "model_matrix"), 1, false, &model_matrix[0][0]);
//	glUniformMatrix4fv(glGetUniformLocation(program_, "view_matrix"), 1, false, &view_matrix[0][0]);
//	glUniformMatrix4fv(glGetUniformLocation(program_, "projection_matrix"), 1, false, &projection_matrix[0][0]);
//
//
//	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
//}





void CubeIndexed::Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::mat4& model_matrix)
{
	unsigned i = 0;
	for (float x = -100.0f; x < 100.0f; x += step_distance_)
	{
		for (float y = -100.0f; y < 100.0f; y += step_distance_)
		{
			for (float z = -100.0f; z < 100.0f; z += step_distance_)
			{
				// use reference model matrix and translate it to form a grid
				model_matrices_[i] = model_matrix;
				model_matrices_[i][3] = glm::vec4(x, y, z, model_matrix[3][3]);
				i++;
			}
		}
	}

	glBindBuffer(GL_ARRAY_BUFFER, model_matrices_object_);
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::mat4) * number_instances_, &model_matrices_[0], GL_DYNAMIC_DRAW);

	glUseProgram(*program_);
	glBindVertexArray(vao_);


	glUniformMatrix4fv(glGetUniformLocation(*program_, "view_matrix"), 1, false, &view_matrix[0][0]);
	glUniformMatrix4fv(glGetUniformLocation(*program_, "projection_matrix"), 1, false, &projection_matrix[0][0]);


	glDrawElementsInstanced(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0, number_instances_);

	glBindVertexArray(0);
}