#include "LevelLoader.h"

#include "BoundingSphere.h"
#include "BoundingSphereModel.h"
#include "Camera.h"
#include "CubeIndexed.h"
#include "CubeMaterial.h"
#include "CubeTextured.h"
#include "EntityComponentSystem.h"
#include "Locator.h"
#include "ModelContainer.h"
#include "ModelFromFile.h"
#include "OrientedBoundingBox.h"
#include "OrientedBoundingBoxModel.h"
#include "Planet.h"
#include "ProgramContainer.h"
#include "Quad.h"
#include "RNG.h"
#include "TextureContainer.h"
#include "UIQuad.h"

#include <iostream>

LevelLoader::LevelLoader()
{
	Locator<ProgramContainer>::service()->Add<Program>("bounding_volume_program", "Shaders\\VertexShaderBoundingVolume.glsl", "Shaders\\FragmentShader.glsl");
	Locator<ModelContainer>::service()->Add<BoundingSphereModel>("bounding_sphere", Locator<ProgramContainer>::service()->Get("bounding_volume_program"));
	Locator<ModelContainer>::service()->Add<OrientedBoundingBoxModel>("oriented_bounding_box", Locator<ProgramContainer>::service()->Get("bounding_volume_program"));

	
	Locator<ProgramContainer>::service()->Add<Program>("texture_program", "Shaders\\VertexShaderTexture.glsl", "Shaders\\FragmentShaderTexture.glsl");
	Locator<TextureContainer>::service()->Add<Texture>("Crate", "Resources\\Crate.bmp");
	Locator<ModelContainer>::service()->Add<CubeTextured>("cube_textured", Locator<ProgramContainer>::service()->Get("texture_program"));
}

LevelLoader::~LevelLoader()
{
}

void LevelLoader::Clear()
{
	// entities -> models -> textures -> shaders
	Locator<EntityComponentSystem>::service()->ClearEntities();
	/*Locator<ModelContainer>::service()->Clear();
	Locator<TextureContainer>::service()->Clear();
	Locator<ProgramContainer>::service()->Clear();*/

	Locator<Camera>::service()->Reset();
}

void LevelLoader::LoadLevel1()
{
	std::cout << "Loading Level 1: instanced rendering cubes \n";

	Clear();

	// camera position/direction ?

	// shaders -> textures -> models -> entities

	Locator<ProgramContainer>::service()->Add<Program>("color_instanced_program", "Shaders\\VertexShaderColorInstanced.glsl", "Shaders\\FragmentShader.glsl");

	Locator<ModelContainer>::service()->Add<CubeIndexed>("cube_indexed", Locator<ProgramContainer>::service()->Get("color_instanced_program"));

	
	Locator<EntityComponentSystem>::service()->CreateEntity(
		Position(0.0f, 1.0f, 0.0f),
		Rotation(0.0f, 0.0f, 0.0f),
		Rendering(Locator<ModelContainer>::service()->Get("cube_indexed")),
		Velocity(0.0f, 100.0f, 0.0f), // no impact ???
		AngularVelocity(0.0f, 20.0f, 0.0f));
}

void LevelLoader::LoadLevel2()
{
	std::cout << "Loading Level 2: Dragon ply with normal colors \n";

	Clear();

	// camera position/direction ?

	// shaders -> models -> entities
	
	
	Locator<ProgramContainer>::service()->Add<Program>("normal_program", "Shaders\\VertexShaderNormal.glsl", "Shaders\\FragmentShader.glsl");
	Locator<ModelContainer>::service()->Add<ModelFromFile>("dragon", "Resources\\dragon.ply", Locator<ProgramContainer>::service()->Get("normal_program"));

	size_t dragon_id = Locator<EntityComponentSystem>::service()->CreateEntity(
		Position(0.0f, 5.0f, 0.0f),
		Scaling(40.0f),
		Rotation(0.0f, 0.0f, 0.0f),
		Rendering(Locator<ModelContainer>::service()->Get("dragon")),
		//Velocity(0.0f, 0.1f, 0.0f),
		AngularVelocity(0.0f, 20.0f, 0.0f),
		OrientedBoundingBox(Locator<ModelContainer>::service()->Get("oriented_bounding_box"), Locator<ModelContainer>::service()->Get("dragon")));

	Locator<ProgramContainer>::service()->Add<Program>("color_program", "Shaders\\VertexShaderColor.glsl", "Shaders\\FragmentShader.glsl");
	Locator<ModelContainer>::service()->Add<Quad>("quad", Locator<ProgramContainer>::service()->Get("color_program"));

	Locator<EntityComponentSystem>::service()->CreateEntity(
		Position(0.0f, -0.1f, 0.0f),
		Scaling(1000.0f),
		Rendering(Locator<ModelContainer>::service()->Get("quad")));

	Locator<ProgramContainer>::service()->Add<Program>("ui_program", "Shaders\\VertexShaderUI.glsl", "Shaders\\FragmentShaderTexture.glsl");
	Locator<TextureContainer>::service()->Add<Texture>("Overlay", "Resources\\Overlay.png");
	Locator<ModelContainer>::service()->Add<UIQuad>("ui_quad", Locator<ProgramContainer>::service()->Get("ui_program"));
	Locator<Camera>::service()->zoom_overlay_id_ = Locator<EntityComponentSystem>::service()->CreateEntity(
		Position(0.0f, 0.0f, 0.0f),
		Scaling(1.0f),
		Rotation(0.0f, 0.0f, 0.0f),
		RenderingUI(Locator<ModelContainer>::service()->Get("ui_quad")));
}


void LevelLoader::LoadLevel3()
{
	std::cout << "Loading Level 3: Teapot obj with normal colors \n";

	Clear();

	// camera position/direction ?

	// shaders -> textures -> models -> entities
	
	Locator<ProgramContainer>::service()->Add<Program>("normal_program", "Shaders\\VertexShaderNormal.glsl", "Shaders\\FragmentShader.glsl");
	Locator<ModelContainer>::service()->Add<ModelFromFile>("teapot", "Resources\\teapot.obj", Locator<ProgramContainer>::service()->Get("normal_program"));
	
	size_t teapot_id = Locator<EntityComponentSystem>::service()->CreateEntity(
		Position(0.0f, 3.0f, 0.0f),
		//Scaling(0.3f, 0.4f, 0.5f),
		Scaling(1.5f),
		Rotation(0.0f, 0.0f, 0.0f),
		//Velocity(0.0f, 0.5f, 0.0f),
		Rendering(Locator<ModelContainer>::service()->Get("teapot")),
		AngularVelocity(0.0f, 20.0f, 0.0f),
		//OrientedBoundingBox(Locator<ModelContainer>::service()->Get("oriented_bounding_box"), Locator<ModelContainer>::service()->Get("teapot")),
		BoundingSphere(Locator<ModelContainer>::service()->Get("bounding_sphere"), Locator<ModelContainer>::service()->Get("teapot")));


	Locator<ProgramContainer>::service()->Add<Program>("color_program", "Shaders\\VertexShaderColor.glsl", "Shaders\\FragmentShader.glsl");
	Locator<ModelContainer>::service()->Add<Quad>("quad", Locator<ProgramContainer>::service()->Get("color_program"));
	Locator<EntityComponentSystem>::service()->CreateEntity(
		Position(0.0f, -0.1f, 0.0f),
		Scaling(1000.0f),
		Rendering(Locator<ModelContainer>::service()->Get("quad")));

	Locator<ProgramContainer>::service()->Add<Program>("ui_program", "Shaders\\VertexShaderUI.glsl", "Shaders\\FragmentShaderTexture.glsl");
	Locator<TextureContainer>::service()->Add<Texture>("Overlay", "Resources\\Overlay.png");
	Locator<ModelContainer>::service()->Add<UIQuad>("ui_quad", Locator<ProgramContainer>::service()->Get("ui_program"));
	Locator<Camera>::service()->zoom_overlay_id_ = Locator<EntityComponentSystem>::service()->CreateEntity(
		Position(0.0f, 0.0f, 0.0f),
		Scaling(1.0f),
		Rotation(0.0f, 0.0f, 0.0f),
		RenderingUI(Locator<ModelContainer>::service()->Get("ui_quad")));
}


void LevelLoader::LoadLevel4()
{
	std::cout << "Loading Level 4: Textured Box with Phong Shading \n";

	Clear();

	// camera position/direction ?

	// shaders -> textures -> models -> entities
	Locator<ProgramContainer>::service()->Add<Program>("color_program", "Shaders\\VertexShaderColor.glsl", "Shaders\\FragmentShader.glsl");
	Locator<ModelContainer>::service()->Add<Quad>("quad", Locator<ProgramContainer>::service()->Get("color_program"));
	Locator<EntityComponentSystem>::service()->CreateEntity(
		Position(0.0f, -0.1f, 0.0f),
		Scaling(1000.0f),
		Rendering(Locator<ModelContainer>::service()->Get("quad")));

	Locator<ProgramContainer>::service()->Add<Program>("material_program", "Shaders\\VertexShaderMaterial.glsl", "Shaders\\FragmentShaderMaterial.glsl");
	Locator<TextureContainer>::service()->Add<Texture>("container_diffuse", "Resources\\container_diffuse.png");
	Locator<TextureContainer>::service()->Add<Texture>("container_specular", "Resources\\container_specular.png");
	Locator<ModelContainer>::service()->Add<CubeMaterial>("cube_material", Locator<ProgramContainer>::service()->Get("material_program"));
	for (float i = 0; i < 100; i++)
	{
		Locator<EntityComponentSystem>::service()->CreateEntity(
			Position(0.0f, (float)i * 2.0f, 0.0f),
			Rendering(Locator<ModelContainer>::service()->Get("cube_material")),
			Rotation(0.0f, 0.0f, 0.0f),
			AngularVelocity(0.0f, 20.0f, 0.0f),
			Velocity(RNG::GetDirection()));
	}
}


void LevelLoader::LoadLevel5()
{
	std::cout << "Loading Level 5: Planet \n";

	Clear();

	// camera position/direction ?

	// shaders -> textures -> models -> entities
	Locator<ProgramContainer>::service()->Add<Program>("procedural_program", "Shaders\\VertexShaderProcedural.glsl", "Shaders\\FragmentShaderProcedural.glsl");
	
	// earth_gradient and planet needs to be reloaded for new noise
	Locator<TextureContainer>::service()->Delete("earth_gradient");
	ColorGradient cg;
	cg.createEarthGradient();
	Locator<TextureContainer>::service()->Add<Texture>("earth_gradient", cg);
	Locator<ModelContainer>::service()->Delete("planet");
	Locator<ModelContainer>::service()->Add<Planet>("planet", Locator<ProgramContainer>::service()->Get("procedural_program"));

	Locator<EntityComponentSystem>::service()->CreateEntity(
		Position(0.0f, 5.0f, 0.0f),
		Scaling(2.0f),
		Rotation(0.0f, 0.0f, 0.0f),
		Rendering(Locator<ModelContainer>::service()->Get("planet")),
		Velocity(0.0f, 0.0f, 0.0f),
		AngularVelocity(0.0f, 20.0f, 0.0f));
}


void LevelLoader::LoadLevel6()
{
	std::cout << "Loading Level 6: Shooting Range \n";

	Clear();
	
	// camera position/direction ?

	// shaders -> textures -> models -> entities
	
	Locator<ProgramContainer>::service()->Add<Program>("texture_program", "Shaders\\VertexShaderTexture.glsl", "Shaders\\FragmentShaderTexture.glsl");
	Locator<TextureContainer>::service()->Add<Texture>("Crate", "Resources\\Crate.bmp");
	Locator<ModelContainer>::service()->Add<CubeTextured>("cube_textured", Locator<ProgramContainer>::service()->Get("texture_program"));
	for (size_t i = 0; i < 100; i++)
	{
		Locator<EntityComponentSystem>::service()->CreateEntity(
			Position(RNG::GetFloat(-70.0f, 70.0f), RNG::GetFloat(1.0f, 20.0f), RNG::GetFloat(-70.0f, 70.0f)),
			Scaling(1.0f),
			Rotation(RNG::GetFloat(0.0f, 360.0f), RNG::GetFloat(0.0f, 360.0f), RNG::GetFloat(0.0f, 360.0f)),
			Rendering(Locator<ModelContainer>::service()->Get("cube_textured")),
			Velocity(RNG::GetDirection()),
			BoundingSphere(Locator<ModelContainer>::service()->Get("bounding_sphere"), Locator<ModelContainer>::service()->Get("cube_textured"))/*,
			OrientedBoundingBox(Locator<ModelContainer>::service()->Get("oriented_bounding_box"), Locator<ModelContainer>::service()->Get("cube_textured"))*/);
	}
	Locator<Camera>::service()->entity_id_ = Locator<EntityComponentSystem>::service()->CreateEntity(
		Position(0.0f, 3.0f, 0.0f),
		Scaling(1.5f),
		Rotation(0.0f, 0.0f, 0.0f),
		Rendering(Locator<ModelContainer>::service()->Get("cube_textured")));

	

	Locator<ProgramContainer>::service()->Add<Program>("color_program", "Shaders\\VertexShaderColor.glsl", "Shaders\\FragmentShader.glsl");
	Locator<ModelContainer>::service()->Add<Quad>("quad", Locator<ProgramContainer>::service()->Get("color_program"));
	Locator<EntityComponentSystem>::service()->CreateEntity(
		Position(0.0f, -0.1f, 0.0f),
		Scaling(1000.0f),
		Rendering(Locator<ModelContainer>::service()->Get("quad")));

	Locator<ProgramContainer>::service()->Add<Program>("ui_program", "Shaders\\VertexShaderUI.glsl", "Shaders\\FragmentShaderTexture.glsl");
	Locator<TextureContainer>::service()->Add<Texture>("Overlay", "Resources\\Overlay.png");
	Locator<ModelContainer>::service()->Add<UIQuad>("ui_quad", Locator<ProgramContainer>::service()->Get("ui_program"));
	Locator<Camera>::service()->zoom_overlay_id_ = Locator<EntityComponentSystem>::service()->CreateEntity(
		Position(0.0f, 0.0f, 0.0f),
		Scaling(1.0f),
		Rotation(0.0f, 0.0f, 0.0f),
		RenderingUI(Locator<ModelContainer>::service()->Get("ui_quad")));

	
}

void LevelLoader::LoadLevel7()
{
	std::cout << "Loading Level 7: Test \n";

	Clear();
}