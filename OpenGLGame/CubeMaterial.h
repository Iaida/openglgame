#pragma once
#include "Model.h"
#include "Texture.h"

class CubeMaterial : 
	public Model
{
public:
	CubeMaterial(Program * program);
	~CubeMaterial();

	Texture * texture_diffuse_;
	Texture * texture_specular_;

	virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::mat4& model_matrix)	override final;
};

