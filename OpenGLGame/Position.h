#pragma once
#include "Component.h"
#include "Dependencies\glm\vec3.hpp"


class Position : public Component<Position>
{
public:
	Position() : position_(glm::vec3()) {}
	Position(const float x, const float y, const float z) : position_(glm::vec3(x, y, z)) {}
	Position(const glm::vec3 & v) : position_(v) {}
	operator glm::vec3() { return position_; }
	Position& operator= (const glm::vec3& v) { position_ = v; return *this; }

	glm::vec3 position_;
};

