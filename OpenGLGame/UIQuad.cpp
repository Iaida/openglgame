
#include "UIQuad.h"

#include "Locator.h"
#include "TextureContainer.h"

#include "Dependencies\glm\vec4.hpp"
#include "Dependencies\glm\vec2.hpp"


UIQuad::UIQuad(Program * program)
{

	struct UIVertexFormat
	{

		glm::vec2 position_; // [-1,+1]
		glm::vec2 uv_; // [0,1]

		UIVertexFormat(const glm::vec2 &position, const glm::vec2 &uv)
		{
			position_ = position;
			uv_ = uv;
		}

		UIVertexFormat()
		{
			position_ = glm::vec2(0.0f, 0.0f);
			uv_ = glm::vec2(0.0f, 0.0f);
		}

	};

	GLuint vao;
	GLuint vbo;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// 3 4
	// 1 2
	std::vector<UIVertexFormat> vertices;

	// default over whole screen										  
	vertices.push_back(UIVertexFormat(glm::vec2(-1.0f, -1.0f),
		glm::vec2(0.0f, 0.0f)));   //color
	vertices.push_back(UIVertexFormat(glm::vec2(1.0f, -1.0f),
		glm::vec2(1.0f, 0.0f)));
	vertices.push_back(UIVertexFormat(glm::vec2(-1.0f, 1.0f),
		glm::vec2(0.0f, 1.0f)));
	vertices.push_back(UIVertexFormat(glm::vec2(1.0f, 1.0f),
		glm::vec2(1.0f, 1.0f)));


	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(UIVertexFormat) * 4, &vertices[0], GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(UIVertexFormat), (void*)(offsetof(UIVertexFormat, UIVertexFormat::position_)));

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(UIVertexFormat), (void*)(offsetof(UIVertexFormat, UIVertexFormat::uv_)));

	glBindVertexArray(0);
	vao_ = vao;
	vbos_.push_back(vbo);

	texture_ = Locator<TextureContainer>::service()->Get("Overlay");

	program_ = program;
}


UIQuad::~UIQuad()
{
}


void UIQuad::Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::mat4& model_matrix)
{
	glUseProgram(*program_);
	glBindVertexArray(vao_);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, *texture_);
	glUniform1i(glGetUniformLocation(*program_, "texture1"), 0);

	glUniformMatrix4fv(glGetUniformLocation(*program_, "model_matrix"), 1, false, &model_matrix[0][0]);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

