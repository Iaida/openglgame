#pragma once
#include "Model.h"

#include "Texture.h"

#include "Dependencies\glm\vec2.hpp"

class CubeTextured :
	public Model
{
public:
	

	CubeTextured(Program * program);
	~CubeTextured();

	Texture * texture_;
	

	
	virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::mat4& model_matrix)	override final;
	
};

