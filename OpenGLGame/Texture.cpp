#include "Texture.h"

#include "Dependencies\soil\SOIL.h"

#include <iostream>
#include <vector>


// http://learnopengl.com/#!Getting-started/Textures

Texture::Texture(const std::string & texture_file_path)
{
	glGenTextures(1, &texture_);

	// All upcoming GL_TEXTURE_2D operations now have effect on our texture object
	glBindTexture(GL_TEXTURE_2D, texture_);

	// Set our texture parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	// Set texture filtering, GL_LINEAR_MIPMAP_LINEAR only usable for MIN_FILTER, not MAX_FILTER
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Load, create texture and generate mipmaps
	int width, height;
	unsigned char* image;

	if (texture_file_path.find(".png") != std::string::npos)
	{
		image = SOIL_load_image(texture_file_path.c_str(), &width, &height, 0, SOIL_LOAD_RGBA);
		if (image == NULL)
			std::cout << "SOIL_load_image failed loading image " << texture_file_path.c_str() << "\n";
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	}
	else
	{
		image = SOIL_load_image(texture_file_path.c_str(), &width, &height, 0, SOIL_LOAD_RGB);
		if (image == NULL)
			std::cout << "SOIL_load_image failed loading image " << texture_file_path.c_str() << "\n";
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
	}


	glGenerateMipmap(GL_TEXTURE_2D);
	SOIL_free_image_data(image);
	// Unbind texture when done, so we won't accidentily mess up our texture.
	glBindTexture(GL_TEXTURE_2D, 0);

}

Texture::Texture(ColorGradient & color_gradient)
{
	glGenTextures(1, &texture_);

	// All upcoming GL_TEXTURE_1D operations now have effect on our texture object
	glBindTexture(GL_TEXTURE_1D, texture_);
	// Set our texture parameters
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	// Set texture filtering, GL_LINEAR_MIPMAP_LINEAR only usable for MIN_FILTER, not MAX_FILTER
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// create texture of size 100 from color_gradient
	std::vector<glm::vec3> new_gradient(100);
	for (int c = 0; c < 100; c++)
	{
		new_gradient[c] = color_gradient.getColorAtValue(static_cast<float>(c) / 100.0f);
	}

	glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, 100, 0, GL_RGB, GL_FLOAT, &new_gradient[0]);
	glGenerateMipmap(GL_TEXTURE_1D);
	// Unbind texture when done, so we won't accidentily mess up our texture.
	glBindTexture(GL_TEXTURE_1D, 0);
}

Texture::~Texture()
{
	glDeleteTextures(1, &texture_);
}

Texture::Texture()
{
}

Texture::operator GLuint() const
{
	return texture_;
}
