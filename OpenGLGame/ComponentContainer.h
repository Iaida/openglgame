#pragma once
#include "IComponentContainer.h"

#include <unordered_map>
#include <iostream>
#include <algorithm>
#include <utility>

// C is a Component class
template<typename C>
class ComponentContainer : public IComponentContainer
{
public:
	ComponentContainer() {};
	~ComponentContainer() {};

	std::unordered_map<size_t, size_t> id_index_table_;

	void Create(const size_t id, const C & component);
	void Remove(const size_t id);
	C & GetByID(const size_t id);
	C & GetByIndex(const size_t index);
	void Clear();
	size_t Size();

private:
	std::vector<C> components_;
};

template<typename C>
inline void ComponentContainer<C>::Create(const size_t id, const C & component)
{
	id_index_table_.emplace(id, components_.size());
	components_.push_back(component);
}

template<typename C>
inline void ComponentContainer<C>::Remove(const size_t id)
{
	// -COMPONENTS-
	// replace component at id index with last component in vector
	GetByID(id) = std::move(components_.back());
	// delete last component which reduces size of vector by 1
	components_.pop_back();

	// -ID-INDEX TABLE-
	size_t last_index = components_.size(); // no '-1' because of previous pop_back() 
	// find ID-index-pair referencing last_index and change its index to passed ID's index
	std::find_if(id_index_table_.begin(), id_index_table_.end(), 
		[&](const auto& id_index) { return id_index.second == last_index; })->second = id_index_table_[id];
	// delete ID-index-pair referencing argument id
	id_index_table_.erase(id);
}

template<typename C>
inline C & ComponentContainer<C>::GetByID(const size_t id)
{
	return components_[id_index_table_[id]];
}

template<typename C>
inline C & ComponentContainer<C>::GetByIndex(const size_t index)
{
	return components_[index];
}

template<typename C>
inline void ComponentContainer<C>::Clear()
{
	components_.clear();
	id_index_table_.clear();
}

template<typename C>
inline size_t ComponentContainer<C>::Size()
{
	return components_.size();
}

