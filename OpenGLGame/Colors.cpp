#include "Colors.h"

#include <algorithm>

glm::vec3 Colors::lerp(float t, const glm::vec3 & col1, const glm::vec3 & col2)
{
	return col1 + (col2 - col1) * t;
}


glm::vec3 Colors::white()
{
	return glm::vec3{ 1.0, 1.0, 1.0 };
}

glm::vec3 Colors::black()
{
	return glm::vec3{ 0.0, 0.0, 0.0 };
}

glm::vec3 Colors::grey()
{
	return glm::vec3{ 0.5, 0.5, 0.5 };
}

glm::vec3 Colors::red()
{
	return glm::vec3{ 1.0, 0.0, 0.0 };
}

glm::vec3 Colors::green()
{
	return glm::vec3{ 0.0, 1.0, 0.0 };
}

glm::vec3 Colors::blue()
{
	return glm::vec3{ 0.0, 0.0, 1.0 };
}

glm::vec3 Colors::navy()
{
	return glm::vec3{ 0.0, 0.0, 0.5 };
}

glm::vec3 Colors::steelBlue()
{
	return glm::vec3{ 70.0f / 255.0f, 130.0f / 255.0f, 180.0f / 255.0f };
}

glm::vec3 Colors::darkGreen()
{
	return glm::vec3{ 0.0f / 255.0f, 100.0f / 255.0f, 0.0f / 255.0f };
}

glm::vec3 Colors::forestGreen()
{
	return glm::vec3{ 34.0f / 255.0f, 139.0f / 255.0f, 34.0f / 255.0f };
}

glm::vec3 Colors::wheat()
{
	return glm::vec3{ 245.0f / 255.0f, 222.0f / 255.0f, 179.0f / 255.0f };
}

glm::vec3 Colors::cyan()
{
	return glm::vec3{ 0.0, 1.0, 1.0 };
}

glm::vec3 Colors::yellow()
{
	return glm::vec3{ 1.0, 1.0, 0.0 };
}

glm::vec3 Colors::brown()
{
	return glm::vec3{ 165.0f / 255.0f, 42.0f / 255.0f, 42.0f / 255.0f };
}