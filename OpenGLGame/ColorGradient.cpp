#include "ColorGradient.h"

#include "Colors.h"
#include <algorithm>

ColorGradient::ColorGradient()
{
	createDefaultGradient();

}

// inserts a new point into its correct position:
void ColorGradient::addGradientPoint(const float value, const glm::vec3 &color)
{
	for (size_t i = 0; i<gradient.size(); i++)  
	{
		if (value < gradient[i].val) 
		{
			gradient.insert(gradient.begin() + i, GradientPoint(value, color));
			return;
		}
	}
	gradient.push_back(GradientPoint(value, color));
}

void ColorGradient::clearGradient() 
{ 
	gradient.clear(); 
}

void ColorGradient::createDefaultGradient()
{
	gradient.clear();
	gradient.push_back(GradientPoint(0.0, Colors::black()));
	gradient.push_back(GradientPoint(1.0, Colors::white()));


}

void ColorGradient::createEarthGradient()
{
	gradient.clear();
	gradient.push_back(GradientPoint(0.00f, Colors::navy()));
	gradient.push_back(GradientPoint(0.49f, Colors::steelBlue()));
	gradient.push_back(GradientPoint(0.50f, Colors::wheat()));
	gradient.push_back(GradientPoint(0.51f, Colors::forestGreen()));
	/*gradient.push_back(GradientPoint(1.0f, Colors::brown()));*/
	gradient.push_back(GradientPoint(0.70f, Colors::wheat()));
	gradient.push_back(GradientPoint(0.80f, Colors::grey()));
	gradient.push_back(GradientPoint(0.90f, Colors::white()));
}



glm::vec3 ColorGradient::getColorAtValue(const float value)
{
	if (gradient.size() == 0)
		return Colors::black();

	for (size_t i = 0; i<gradient.size(); i++)
	{
		GradientPoint &curr = gradient[i];
		if (value < curr.val)
		{
			GradientPoint &prev = gradient[std::max<size_t>(0, i - 1)];
			float valueDiff = (prev.val - curr.val);
			float fractBetween = (valueDiff == 0.0f) ? 0.0f : (value - curr.val) / valueDiff;
			return Colors::lerp(1.0f - fractBetween, prev.col, curr.col);
		}
	}
	return gradient.back().col;
}