#pragma once
#include "Component.h"
#include "Dependencies\glm\vec3.hpp"

class Velocity : public Component<Velocity>
{
public:
	Velocity() : velocity_(glm::vec3(1.0f)) {}
	Velocity(const float x, const float y, const float z) : velocity_(glm::vec3(x, y, z)) {}
	Velocity(const glm::vec3 & v) : velocity_(v) {}

	operator glm::vec3() { return velocity_; }
	Velocity& operator= (const glm::vec3& v) { velocity_ = v; return *this; }

	glm::vec3 velocity_;
};

