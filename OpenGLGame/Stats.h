#pragma once

#include <vector>

struct attributes
{
	std::string name;
	unsigned strength;
	unsigned agility;
	unsigned intelligence;
};

class Stats
{
private:
	std::vector<std::vector<std::string>> CsvToStringVector(std::string file_path);
	std::vector<attributes> StringVectorToAttributeVector(std::vector<std::vector<std::string>> &string_vector);

public:
	std::vector<attributes> attributes_vector_;

	Stats(std::string file_path);


};

