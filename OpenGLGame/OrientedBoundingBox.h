#pragma once
#include "IBoundingVolume.h"
#include "Component.h"

#include "Dependencies\glm\vec3.hpp"
#include "Dependencies\glm\gtc\quaternion.hpp"
#include "Dependencies\glm\gtx\quaternion.hpp"
#include "Dependencies\glm\mat4x4.hpp"

class Model;

class OrientedBoundingBox :
	public IBoundingVolume, public Component<OrientedBoundingBox>
{
public:
	OrientedBoundingBox();
	OrientedBoundingBox(Model * model_ptr, Model * target_model_ptr);
	~OrientedBoundingBox();

	virtual bool Collision(const IBoundingVolume & bounding_volume) const override;
	virtual bool CollisionBoundingSphere(const BoundingSphere & bounding_sphere) const override;
	virtual bool CollisionOrientedBoundingBox(const OrientedBoundingBox & oriented_bounding_box) const override;
	virtual void Draw(const glm::mat4 & projection_matrix, const glm::mat4 & view_matrix) override;
	virtual std::pair<bool, float> RayIntersection(const Ray & ray) const override;
	virtual bool NegativeHalfSpace(const Plane & plane) const override;


	glm::vec3 position_;
	glm::vec3 initial_extent_;
	glm::vec3 extent_;
	glm::quat rotation_;
	
	

private:
	Model * model_ptr_;

	glm::vec3 ClosestPointOBB(const glm::vec3 & point) const;
	glm::mat4 ModelMatrix() const;
};

