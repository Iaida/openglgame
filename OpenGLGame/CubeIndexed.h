#pragma once
#include "Model.h"

class CubeIndexed :
	public Model
{
public:
	CubeIndexed(Program * program);
	~CubeIndexed();

	
	virtual void Draw(const glm::mat4& projection_matrix, const glm::mat4& view_matrix, const glm::mat4& model_matrix)	override final;

private:
	GLuint model_matrices_object_;
	std::vector<glm::mat4> model_matrices_;
	unsigned number_instances_;
	float step_distance_;
};

