#pragma once
#include <random>
#include "Dependencies\glm\vec3.hpp"

class RNG
{
public:
	static float GetFloat(const float min, const float max);
	static glm::vec3 GetDirection();
	
private:
	static std::mt19937_64 engine_;
};

